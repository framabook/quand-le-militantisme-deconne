<!------------ CHAP 6 Que faire à la place de la déconnance&nbsp;? ----------->




## Que faire à la place de la déconnance&nbsp;?

On a déjà pu apercevoir dans les points précédents qu’on se mettait à
avoir des pratiques déconnantes non pas parce qu’on est persuadé que ce
sont de bonnes pratiques, mais davantage malgré nous, parce que nos
propres besoins sont sapés (par exemple faute d’avoir son autonomie
comblée, on tente de contrôler l’autre, ce qui nous donne une
satisfaction ponctuelle de notre besoin de compétence), parce que c’est
le modèle de fonctionnement majoritaire dans nos environnements sociaux,
parce qu’on manque d’informations, qu’on est surmené, etc.

### Viser les besoins fondamentaux et vivre sa motivation intrinsèque

La solution est donc pour casser ce cercle vicieux est de commencer à
*nourrir les besoins fondamentaux* à travers nos activités (tous les
conseils dans les cadres jaunes des schémas précédents), tant
les nôtres que ceux des autres en même temps. S’ensuivront des
motivations de plus haute qualité pour l’activité, et celles-ci vont
aussi nourrir en retour nos besoins en un cercle cette fois-ci vertueux.
Et quand on aime profondément une activité, on cherche à en décupler le
plaisir, donc on tente de la partager, les personnes aimant partager
des émotions plaisantes écoutent et sont à leur tour entraînées dans une
motivation à cette activité. Le truc serait juste de *vivre et de communiquer pleinement sa motivation intrinsèque* pour telle activité.

Cette transmission de la joie et du vécu positif pour une activité qu’on
a avec une motivation intrinsèque (comme peuvent l’être tous les loisirs
actifs, les disciplines qui ont pu passionner des personnes dans le
monde) peut se faire dès qu’on lève toute crainte quant au jugement de
celles et ceux qui pourraient observer ce vécu joyeux, crainte qui peut
potentiellement s’effacer lorsqu’on est concentré dans l’activité
elle-même. Généralement les gens perçoivent la passion, ressentent les
émotions positives sincères lorsqu’elles sont explicitement vécues, de
façon authentique (par exemple, on ne se forcerait pas à transmettre la
joie de faire ceci, on serait juste effectivement joyeux de faire
cela)[^64]. La plus grande difficulté de ce partage de motivation
intrinsèque réside dans la crainte des atteintes à la proximité
sociale&nbsp;: être authentique, c’est être à nu, donc s’exposer
potentiellement au jugement d’autrui, à son mépris, à son indifférence,
à sa future ostracisation. On peut donc avoir des réticences à s’exposer
sincèrement, tout particulièrement lorsqu’on a déjà vécu des situations
de forte indifférence ou de mépris alors qu’on était pleinement
authentique et bienveillant. Cela demande alors un même type de courage
qu’un saut du plongeoir, on ne peut que se jeter à l’eau, s’immerger
(ici dans le sujet, en vivant totalement avec lui), et nager jusqu’à
l’atteinte du but. La crainte du regard d’autrui, son jugement potentiel
est mis de côté, on se concentre sur son rapport authentique à sa
passion. Vivre pleinement sa motivation intrinsèque et l’exposer n’est
pas tant un effort, un travail, mais plutôt une immersion de l’attention
qui est telle que les craintes liées au sapage de la proximité sociale
sont pour le moment comme hors sujet.

### Viser les motivations extrinsèques intégrées

Cependant, on sait aussi tous que la vie n’est pas forcément composée
d’activité attractive. Changer la litière du chat, descendre les
poubelles, suivre le Code de la route… Je ne connais personne qui
ait de motivation intrinsèque pour ces activités, et c’est tout à fait
normal parce que celles-ci peuvent avoir intrinsèquement des stimuli
aversifs (l’odeur de la litière, des poubelles), demander des actions
ennuyeuses qui n’apportent rien (attendre au stop n’est en rien une
expérience qui nous apprend quelque chose), etc. De même, sans
motivation intrinsèque, certaines activités militantes peuvent être tout
aussi répulsives en premier lieu.

Toutefois on peut avoir des *motivations extrinsèques intégrées* pour
ces actions répulsives, et celles-ci sont puissantes, durables sur le
long terme et rendant l’acte moins pénible ou coûteux en efforts. On
change alors la litière pour maintenir un foyer plus agréable pour ses
habitants (y compris pour le chat qui vous remerciera en cessant de vous
faire découvrir au petit matin de petites surprises puantes sur le sol du
salon), on suit le Code de la route parce qu’on ne veut pas causer
d’accident, on trie ses poubelles correctement pour faciliter le travail
des éboueurs et de tous ceux qui travaillent sur le traitement des
déchets.

Et comme c’est particulièrement intégré en nous, ça ne nous coûte rien
de le faire, on ne rechigne pas, on n’a plus besoin d’y penser, on n’a
pas de crainte d’être jugé, on ne sent pas de menaces, on n’agit pas par
injonction.

L’autre avantage de cette motivation à régulation intégrée, c’est la
résistance aux tentatives de manipulation/d’influence néfaste&nbsp;: par
exemple, une personne à motivation intégrée pour le tri triera non
seulement tout le temps sans que personne n’ait à lui ordonner quoique
ce soit, sans qu’il y ait une seule pression, mais plus encore elle ne
sera pas influencée par les arguments tentant de la convaincre que c’est
pathétique de trier, et elle continuera son comportement. On a donc là
une motivation très puissante, potentiellement préventive face aux
menaces et aux tentatives d’influence.

Mais comment transmettre ça à un autre, sans être injonctif, saoulant,
culpabilisant&nbsp;? 

<div style="background: #F5F5F5">
Une expérience de la théorie de l’autodétermination est
assez éloquente à ce sujet&nbsp;: 

&#8505; &#8674;  Koestner et coll. (1984). Au travers d’une expérience sur la
peinture avec des enfants, il a été testé différentes façons de
présenter une règle consistant à respecter la propreté du matériel. Pour
soutenir l’autonomie malgré une imposition de règles, il a été vu qu’il
fallait présenter les choses ainsi&nbsp;:

- *Minimiser l’usage d’un langage contrôlant* («&nbsp;tu dois&nbsp;» «&nbsp;il faut&nbsp;»...),
- *Reconnaître le sentiment* des enfants à ne pas vouloir être soigneux avec les outils,
- Fournir aux enfants une *justification* de cette limite/règle (c’est-à-dire expliquer pourquoi on a voulu que les outils restent propres).

En présentant ainsi les limites de façon non contrôlante, la motivation
intrinsèque des enfants pour la peinture était préservée et beaucoup
plus haute que dans un cadre contrôlant (c’est-à-dire avec juste l’ordre
de ne pas salir les outils, sans justification ni reconnaissance du
sentiment de l’enfant).
</div>

Si on transpose cela à l’acte militant, vous avez plus de chances de
réussir à transmettre un changement d’habitude, une nouvelle pratique
qui supplante une ancienne, une alternative, en n’étant pas contrôlant
dans son langage&nbsp;: on supprime l’impératif, «&nbsp;il faut&nbsp;» «&nbsp;tu dois&nbsp;». À
la place, on peut mettre «&nbsp;on peut&nbsp;», «&nbsp;il est possible de&nbsp;»&nbsp;; je trouve
que le conditionnel est aussi très doux pour montrer des possibilités.
Et un discours non injonctif qui connote l’ouverture à des possibilités
est un discours qui permettra d’éviter des comportements réactants.

&#10083; *Reconnaître les émotions* d’autrui, c’est soit se mettre en empathie
cognitive avec l’autre (par exemple, imaginer ce que peut ressentir un
militant antivax), soit essayer de comprendre ses émotions en l’écoutant
activement, sans jugement.

Le thread suivant explique formidablement bien comment on peut communiquer avec
«&nbsp;l’adversaire&nbsp;» à sa cause d’une façon qui respecte son autonomie, ses
besoins (ici c’est la personne antivax, mais ça pourrait concerner un
autre sujet, la méthode d’écoute des émotions et besoins serait tout
aussi pertinente).

> **[Thread] Comment parler à une personne Antivax&nbsp;?**
> - (le) Deuxième Humain
> - @DeuxiemeHumain
> 
> 
> &#10093; J’ai vu plein de gens parler de leurs proches qui veulent pas se faire vacciner / ont peur des vaccins / pensent qu’il faut pas faire confiance à la médecine,
(4:24 PM · 21 mai 2021 · Twitter)
> 
> &#10093;  et qui aimeraient bien les faire changer d’avis ou les pousser à se faire vacciner (pour rester en vie), donc voici quelques astuces pour y arriver&nbsp;:
> 
> &#10093; Précision&nbsp;: tout ce dont je vais parler ici concerne les proches / personnes qu’on connait plutôt bien.
> 
> &#10093; Malheureusement  faire changer d’avis un·e inconnu·e sur twitter, surtout un sujet aussi chargé émotionnellement, ce n’est souvent pas un objectif réaliste. Mais si tu as de la patience et du temps, tu peux toujours essayer&nbsp;:)
> 
> &#10093; Le plus important c’est de ne pas prendre les personnes antivax pour des idiotes. C’est pas parce qu’on est antivax qu’on est plut bête qu’un·e autre.
> 
> &#10093; Et même si c’était le cas&nbsp;: se faire prendre de haut ça n’a jamais fait évoluer personne. Et ça n’a jamais n’a définitivement jamais fait évoluer personne de façon saine.
> 
> &#10093; Ne monopolisez pas la parole&nbsp;: c’est important d’avoir une vraie discussion où vous écoutez sincèrement la personne en face, sinon elle ne va pas avoir envie de vous écouter en retour et vous risquez de parler dans le vide.
> 
> &#10093; Il faut essayer d’avoir un véritable échange, ne placez pas uniquement les sources avec les faits ou statistiques sur les vaccins qui montrent que c’est mieux de se faire vacciner comme si vous étiez en train de jouer aux échecs.
> 
> &#10093; Écoutez. Écoutez. Écoutez. Personne ne «&nbsp;naît&nbsp;» antivax. Il y a toujours une raison derrière.
> 
> &#10093; Ça peut être une histoire personnelle, une peur des «&nbsp;élites&nbsp;», une peur ou une incompréhension de la science derrière les vaccins, une perte de confiance envers la médecine, des positions politiques ou religieuses…
> 
> &#10093; Et si la personne en face ne rentre pas dans les détails, posez des questions. Intéressez-vous sincèrement à la personne face à vous et aux raisons qui ont poussé à être contre les vaccins.
> 
> &#10093; Ça vous aidera à mieux l’aider à comprendre ce sujet et aussi à mieux la comprendre de manière générale dans la vie (et c’est toujours cool d’être plus proche de ses proches).
> 
> &#10093; Tant que vous y êtes&nbsp;: parlez de vous aussi. Pourquoi est-ce que vous êtes d’accord-vax&nbsp;? (Je viens d’inventer ce mot, j’en suis très fier)
> 
> &#10093; Est-ce que vous avez eu des doutes à certains moments&nbsp;? Comment avez-vous fait pour vous renseigner&nbsp;? Qu’est-ce qui vous a fait vous décider&nbsp;? Pourquoi vous faites-vous vacciner&nbsp;?
> 
> &#10093; Je passe beaucoup de temps dessus, parce que c’est très important d’avoir une vraie discussion et de ne pas venir avec son Powerpoint, balancer plein de chiffres ou de noms qui font sérieux puis repartir direct.
> 
> &#10093; Et la deuxième étape, après avoir pris le temps d’écouter et de parler posément avec la personne antivax, c’est d’apporter des réponses ou des solutions à ses problèmes.
> 
> &#10093; La personne que vous souhaitez convaincre ne fait pas confiance aux positions du gouvernement parce que, honnêtement, c’est des positions qui changent toutes les 2 semaines c’est chelou&nbsp;?
> 
> &#10093; Parlez-lui des recommandations de l’OMS-qui ont d’ailleurs plusieurs fois été gentiment ignorées par le gouvernement.
> 
> &#10093; Vous êtes face à quelqu’un qui a peur des effets secondaires potentiels&nbsp;? Regardez ensemble quels sont les effets secondaires potentiels des vaccins et les effets primaires du Covid (Spoiler&nbsp;: le Covid a l’air franchement plus violent).
> 
> &#10093; Et vous pouvez aussi regarder la liste d’effets secondaires de médicaments courants ou qu’elle prends, pour lui montrer que ça n’est pas si différent et qu’il s’agit de cas rares. Ils existent, mais sont rares.
> 
> &#10093; Quelqu’un ne veut pas se faire vacciner parce que «&nbsp;c’est chiant je sais pas comment faire avec internet et tout&nbsp;»&nbsp;? Vous pouvez l’aider à prendre rendez-vous, le faire pour elui voire même l’accompagner si vous êtes disponible&nbsp;!
> 
> &#10093; Rien que proposer de prendre des rendez-vous au même moment ça peut motiver certaines personnes qui n’étaient pas sûres&nbsp;: avec l’effet de groupe on se dit «&nbsp;allez, tant qu’on y est&nbsp;!&nbsp;» et c’est toujours plus rassurant d’y aller à plusieurs, surtout avec des proches.
> 
> &#10093; Storytime&nbsp;: Quelqu’un dans ma famille (anonymysé·e pour des raisons d’anonymat) vient d’un pays où il y a littéralement eu des tests de vaccins et médocs faits sur la population «&nbsp;pour voir si ça fonctionne bien avant de les envoyer dans les pays riches&nbsp;».
> 
> &#10093; Allez savoir pourquoi, cette personne n’a pas super méga confiance en la vaccination contre le Covid du coup. Et bah on va se faire vacciner avant elui, comme ça iel pourra voir si on va bien et aller se faire vacciner en ayant confiance.
> 
> &#10093; Le plus important c’est d’écouter les besoins ou peurs des personnes et les aider à les surmonter -et ça, quels que soient ces problèmes et ces peurs, même si elles nous paraissent ridicules&nbsp;: un peu de compassion punaise&nbsp;!
> 
> &#10093; Félicitations, vous êtes arrivé·es à la fin de ce thread&nbsp;! Pour fêter ça vous pouvez le RT où l’envoyer à des gens que ça pourrait aider. Et pour me soutenir vous pouvez aller sur https://utip.io/vivreavec , ça nous soutient Matthieu et moi&nbsp;!
> 
> --- [Source Twitter](https://twitter.com/DeuxiemeHumain/status/1395747396630589443).

&#9874;  Concernant la *justification rationnelle* à apporter sur «&nbsp;pourquoi&nbsp;»
selon le militant il faudrait changer de comportement (ne plus employer
tel mot, tel logiciel&nbsp;; porter le masque, se faire vacciner, ne pas
croire ceci, etc.), des méta-analyses révèlent celles les plus
convaincantes&nbsp;:

<div style="background: #F5F5F5">
&#8505; &#8674;  Steingut, Patall et Trimble (2017) ont fait une méta-analyse de 23
expériences portant sur le soutien à l’autonomie qui fournissait une
explication ou une justification rationnelle sur la tâche à faire. Ils
ont découvert que cette explication augmente la valeur perçue de la
tâche, mais peut parfois générer un effet négatif sur le sentiment
d’être compétent. En effet, toutes les explications n’ont pas la même
valeur autodéterminante, et peuvent être classées en 3 types&nbsp;:

- contrôlantes&nbsp;: le comportement est dit important pour des raisons
externes, tel que «&nbsp;cela vous rapportera de l’argent, une promotion&nbsp;»,
ou concernant l’apparence physique ou canalisant le sentiment de
culpabilité&nbsp;;
- autonomes&nbsp;: le comportement est dit important pour soi, ses valeurs
personnelles, son développement personnel «&nbsp;cela améliorera votre
mémoire/votre indépendance/votre esprit critique...&nbsp;»&nbsp;;
- prosociales&nbsp;: le comportement est dit important pour autrui, «&nbsp;cela
va apporter du confort et du bien-être à vos proches/aux personnes
présentes&nbsp;».
</div>

*C’est lorsque l’explication ou la justification est prosociale que le comportement est ensuite le plus efficace, avec une meilleure motivation autonome, un meilleur engagement.*

Autrement dit, l’humain étant un animal social, il est davantage motivé
de suivre un comportement qui va clairement montrer que ça aide un autre
humain&nbsp;; ça le motive plus que les récompenses, l’argent, l’évitement de
la culpabilité, ou la croissance de ces capacités ou compétences
personnelles. À mon avis, cette justification prosociale, pour être
transmise efficacement, pourrait être formulée au plus concret et
proximal possible&nbsp;: dire que le tri des déchets va sauver l’humanité ne
sera pas une justification qui motivera le locuteur à changer son
comportement, par contre dire que ça facilite le travail de l’éboueur
qu’on peut croiser de temps en temps dans sa rue sera bien plus
efficace. Parce que la réussite «&nbsp;sauver le monde&nbsp;» est à la fois un
défi trop important, quand bien même il serait réussi, il n’y aurait pas
de feedback de réussite direct («&nbsp;ah vous êtes le type qui avait eu une
pratique écologique parfaite et depuis nous n’avons plus de pollution,
merci beaucoup&nbsp;!&nbsp;»)&nbsp;; alors que voir les éboueurs de bonne humeur dans
la rue parce qu’il n’y a pas de problème avec les poubelles et les
déchets tels que les gens en ont pris soin, est un feedback directement
visible, appréciable, concret.

&#9784; *Soutenir l’autonomie* (en n’étant pas contrôlant, en donnant des
explications rationnelles et prosociales) est stratégiquement le plus
approprié si on souhaite transmettre à la personne l’adoption d’un
comportement à long terme, qui peut potentiellement «&nbsp;déborder&nbsp;» (Spill
Over effect), c’est-à-dire entraîner un comportement analogue (par
exemple on apprend un comportement écologique de tri, la personne va
faire déborder ce comportement par elle-même en commençant à faire
attention à sa production de déchets).

<div style="background: #F5F5F5">
&#8505; &#8674;  Dolan et Galizzi (2015) ont constaté que cet effet de débordement est
au plus fort lorsque les interventions visent la motivation
intrinsèque&nbsp;; et inversement, les interventions basées sur
l’augmentation de la culpabilité ont les effets les plus négatifs.
</div>

La motivation intrinsèque + la motivation intégrée sont le carburant des
résistants et génèrent, selon les situations, un courage, une créativité
rebelle et une puissance exceptionnelle, qu’eux-mêmes ne comprennent pas
quand elles adviennent[^65]. En cela, il me semble que ce sont les
motivations qu’on pourrait davantage tenter de nourrir lorsqu’on est
militant ou engagé, puisqu’une seule personne avec une telle motivation
peut transformer toute une situation concernant des centaines d’autres.

### Un militantisme autodéterminateur plutôt que contrôlant

La théorie de l’autodétermination donne des outils vraiment très
accessibles, testables, qui ont déjà démontré une forte efficacité. Mais
avant de trop nous emballer, il y a malheureusement à se rappeler que
même la militance la plus efficace ne pourra réparer immédiatement tout
le mal que des décennies d’environnements sociaux déconnants ont pu
générer, ni même réussir à combler les besoins d’individus qui sont
encore aux prises d’environnements sociaux sapants. Un oncle peut
arriver à rendre joyeux et libre son neveu, mais si l’enfant est battu
par ses parents dès qu’il les retrouve, tout le travail de nourrissement
des besoins par l’oncle est réduit en miettes. Parfois la meilleure aide
à apporter à autrui est de l’aider à fuir des environnements sociaux
destructifs, que ce soit la famille maltraitante, le travail où il y a
harcèlement, ce village où il n’y a que surveillance, mépris et
solitude, etc. Cet exemple peut apparaître éloigné des situations de
militance, mais pas tant que ça&nbsp;: lorsqu’on discute, qu’on tente de
comprendre l’adversaire ou le spectateur voulant rester dans sa routine
et ne rien changer, ceux-ci nous décrivent rapidement des environnements
sociaux dans lesquels ils sont sous emprise, parfois de manière très
complexe, et dont on peut difficilement les aider à s’extirper pour 
de meilleurs environnements sociaux (par conséquent, le
changement de comportement qu’on propose peut apparaître à la personne
comme un effort trop grand, ou ridicule par rapport à la souffrance
vécue).

Cependant, pour reprendre cette métaphore familiale, cet oncle qui aura
rendu heureux cet enfant maltraité, quand bien même il n’a pas réussi
pour le moment à trouver une solution pour libérer cet enfant, lui a
tout de même offert un modèle d’environnement social sain, lui aura
montré que les choses peuvent fonctionner d’une bien meilleure façon.
Cet acte n’est absolument pas anodin, au contraire, il permet d’aider
l’enfant à ne pas intérioriser le modèle maltraitant comme étant la
bonne chose à reproduire (puisque le modèle concurrent est producteur de
bonheur), et ça c’est extrêmement important pour le futur, pour son
développement.

Voilà pourquoi ça vaut le coup d’essayer de nourrir les besoins
fondamentaux des personnes, surtout dans un travail engagé/militant,
quand bien même on n’arrive pas dans l’immédiat à résoudre les grands
problèmes, ni à changer aucun comportement ou à convaincre. Il ne s’agit
pas de placer un arbre de force, mais de distiller quelques graines ci
et là. Si on a nourri un peu les besoins de la personne par notre
écoute, c’est déjà beaucoup, parce que c’est montrer concrètement qu’un
environnement social peut être nourrissant. Pour donner un exemple
concret, des discussions sympas peuvent amener un adversaire à
abandonner une idéologie qui le ravageait et se transformer&nbsp;: un incel[^66] raconte comment le fait d’avoir des discussions banales avec
des féministes et autres personnes non-incel lui a apporté quelque chose de libérateur. Le fait de rencontrer d’autres environnements sociaux ne fonctionnant pas de la même manière peut constituer une expérience
paradigmatique qui les transforment&nbsp;:

> «&nbsp;Quand j’étais un incel je ne sortais jamais. Je n’avais jamais mis un pied dans un bar, un club, je ne connaissais rien de ce style de vie.  Du coup, c’était facile de croire tout ce qui se racontait en ligne sur les bars, les clubs, les femmes, parce que je n’avais aucun élément de comparaison issu de la réalité qui m’aurait permis de séparer le vrai du faux. 
> 
> La première fois que j’ai été dans un bar, j’ai vu un mec faisant bien 10 centimètres de moins que moi et le double de mon poids, installé dans le carré VIP avec plein de femmes sexy gravitant de son côté. Voir ça, ça a anéanti ma vision du monde. Parce que si on en croit la communauté des incels, ce que faisait ce mec, là, c’était littéralement impossible.
> 
> En gros, j’ai remplacé ce que j’ai appris des incels par des connaissances tirées d’expériences réelles.&nbsp;» (tiré de [reddit.com](https://www.reddit.com/r/IncelTears/comments/8fd7in/a_story_from_a\_former_incel_how_i\_got_myself_out/), traduit par [Madmoizelle.com](https://www.madmoizelle.com/incel-temoignage-920619))[^66b].



Voilà ci-dessous tout ce que la théorie de l’autodétermination conseille
pour nourrir les besoins et tout ce qui pourrait aider la personne à
s’autodéterminer&nbsp;; il y a aussi tout ce qu’il y a à ne pas faire car
cela sape l’autodétermination, envoie les individus vers des motivations
de basses qualités. Cependant, si vous êtes autoritaire et si vous visez
le contrôle des individus afin d’en faire des pions, que vous avez
d’énormes moyens afin de développer ce mode de contrôle (par exemple
installer une surveillance massive de tout instant, embaucher de
nombreux militants injonctiveurs tels que des chefs, sous-chefs,
surveillants, contremaître, *black hat trolls*[^67], etc.), évidemment
il serait incohérent de suivre les conseils autodéterminateurs puisque
cela irait contre vos buts. À noter que ce ne sont pas des conseils
juste lancés comme ça, tout a été testé par des expériences et études
répliquées.

Table:  *Recommandations de la SDT pour viser l’autodétermination (= motivation intrinsèque + motivation intégrée + besoins fondamentaux comblés + orientation autonome)*

| Ce qui aide à l’autodétermination et au bien-être des individus dans les environnements sociaux. (Environnements autodéterminants) | Ce qui empêche l’autodétermination, contribue au mal-être, et pousse les individus à être pion dans les environnements sociaux (Environnements contrôlants) |
|:-----------------------------------------|:------------------------------------------------|
| &bull; Viser le bien-être<br> &bull; Viser le comblement des besoins<br> &bull; Chercher à ce que les individus soient autodéterminé, puissent  s’émanciper grâce à nos apports  ou  être  libres  dans  la  structure  (viser  la  préservation,  le  developpement,  le  maintien  de  la  motivation  intrinsèque,  la  régulation  identifiée/intégrée,  l’orientation autonome, l’amotivation pour les activités/comporte-ments sapant les besoins des autres/de soi) <br> &bull; Formuler, transmettre, encourager et nourrir les buts et aspirations intrinsèques, montrer les possibilités de la situation  | &bull; Viser le mal-être<br> &bull; Viser la frustration des besoins pour mieux déterminer son   comportement/ses idées...<br> &bull; Chercher  à  déterminer  totalement  les  individus,  a  avoir  un  contrôle  total  sur  eux  (orientation  contrôlée/impersonnelle,  pas  de  motivation  intrinsèque,  introjection,  régulation  externe,  amotivation  pour  les  activi-tés/comportements  nourrissant  ses  ou  les  besoins  des  autres) <br> &bull; Formuler, transmettre, encourager et nourrir les buts et aspirations extrinsèques, éliminer/nier buts intrinsèques, montrer les impossibilités et les contrôles de la situation    |
| **Concevoir un environnement favorisant l’autonomie** | **Concevoir un environnement contrôlant** |
|   &bull; transmission autonome de limites (pas de langage contrôlant&nbsp;; reconnaissance des sentiments négatifs&nbsp;; justification rationnelle et prosociale de la limite)<br> &bull; proposition et soutien de vrais choix, pas simplement des options interchangeables<br> &bull; fournir des explications claires et rationnelles<br> &bull; permettre à la personne de changer la structure, le cadre, les habitudes si cela est un bienfait pour tous<br> &bull; ne pas condamner les prises d’initiatives<br> &bull; modèle horizontal, autogouverné, en appuyant sur le pouvoir constructif de chacun | &bull; punitions  <br> &bull; transmission contrôlée des limites (langage contrôlant, déni des émotions, absence de justification) <br> &bull; récompenses (conditionné à la performance, conditionnelles) <br> &bull; mise en compétition menaçant l’ego <br> &bull; surveillance <br> &bull; notes / évaluations menaçant l’ego <br> &bull; objectifs imposés/temps limité induisant une pression <br> &bull; appuyer sur la comparaison sociale <br> &bull; évaluation menaçant l’ego <br> &bull; modèle de pouvoir hiérarchique, en insistant fortement sur son pouvoir dominant |
| **Concevoir un environnement favorisant la proximité sociale** | **Concevoir un environnement niant le besoin de proximité sociale ou uniquement de façon conditionnelle** |
| &bull; faire confiance<br > &bull; se préoccuper sincèrement des soucis ou problèmes de l’autre<br > &bull; dispenser de l’attention et du soin<br > &bull; exprimer son affection, sa compréhension<br > &bull; partager du temps ensemble<br > &bull; savoir s’effacer lorsque la personne n’a pas besoin de nous<br > &bull; écouter | &bull; ne jamais faire confiance<br> &bull; être condescendant, exprimer du dédain envers les personnes<br> &bull; terrifier les personnes <br> &bull; montrer de l’indifférence pour les autres<br> &bull; instrumentaliser les relations<br> &bull; empêcher les liens entre les personnes de se faire<br> &bull; comparaison sociale<br> &bull; appuyer sur les mécanismes d’inflation de l’ego (l’orgueil, la fierté d’avoir dépassé les autres) |
| **Concevoir un environnement favorisant la compétence** | **Concevoir un environnement défavorisant la compétence ou n’orientant que la compétence via la performance** |
| &bull; être clair sur les procédures, la structure, les attentes<br> &bull; laisser à disposition des défis/tâches optimales, adaptables à chacun<br> &bull; donner des trucs et astuces pour progresser<br> &bull; permettre l’autoévaluation<br> &bull; si besoin, proposer des récompenses «&nbsp;surprises&nbsp;» et congruentes (sans condition)<br> &bull; donner des feed-back informatif, positif ou négatif, mais sans implication de l’ego. | &bull; ne pas communiquer d’attentes claires, ni donner de structures ou procédures concernant les choses à faire<br> &bull; donner des taches et défis inadaptés aux compétences des personnes voire impossible. <br> &bull; Évaluer selon la performance<br> &bull; donner des feedback menaçant l’ego de la personne (humiliation, comparaison sociale)<br> &bull; donner des feedback flous sans informations<br> &bull; traduire les réussites et échecs en terme interne allégeant.<br> &bull; feed-back positif pour quelque chose de trop facile<br> &bull; valoriser les signes extérieurs superficiels de réussite |



<!------------ notes CHAP 6 ----------->


[^64]: Les recherches sur l’autodétermination démontrent que
    généralement les gens détectent et jugent très positivement les
    personnes à motivation intrinséque, passionnées, et souhaitant
    empuissanter autrui. Leur propre motivation intrinsèque augmente
    aussi via l’exposition à ces profils (que ce soit la ou le
    conjoint·e, les professeur·es, les coachs, les superviseurs, etc.).
    Cependant, s’ils sont contrôlants, sapent l’autonomie, cela ne
    marche pas du tout et fait l’effet inverse. Cf Deci, Schwartz,
    Sheinman et Ryan (1981)&nbsp;; Ryan et Grolnick (1986)&nbsp;; Deci et Ryan
    (2017).

[^65]: Dans *Un si fragile vernis d’humanité, banalité du mal, banalité du bien*, Michel Terestchenko rapporte comment un routier, voyant une situation où un groupe de Juifs allait se faire
    expulser ou incarcérer en camp, s’est d’un coup fait passer pour un
    diplomate auprès des nazis et a pu interdire aux autorités de nuire
    à ce groupe. Il a fait ça sans l’avoir prémédité. On trouve quantité
    d’actes non prémédités d’altruisme hautement stratégique et très
    efficace également dans «&nbsp;Altruistic personnality&nbsp;» des Oliner (dont
    on a traduit [des morceaux ici](https://www.hacking-social.com/2019/03/25/pa1-la-personnalite-altruiste/)&nbsp;; globalement, cela semble dû à un altruisme à motivation intégrée, ou
    à une amotivation autodéterminée à faire du mal qui a été forgée
    dans le passé, notamment grâce au fait que les désobéissants aient
    eu au moins un proche ou un ami nourrissant leurs besoins
    fondamentaux et présentant concrètement des actes altruistes.

[^66]: Idéologie anti-femme / anti-couples qui considère (entre autres)
    que seuls certains hommes exceptionnellement beaux ou riches
    attireront les femmes, donc qu’ils seront célibataires à jamais. Il
    y a aussi chez eux un rejet des femmes non-blanches et/ou
    non-blondes, un rejet des femmes ne suivant pas un modèle
    traditionnel (par exemple, si elles travaillent, si elles ont fait
    des études), un rejet du fait qu’elles puissent être des personnes
    (l’incel considére que s’il rend service à une femme, elle doit
    coucher avec lui&nbsp;; il y a une infériorisation de la femme et une
    objectivation). Ils disent haïr les femmes tout en disant crever
    d’envie d’être en couple avec elles. Les incels ont commis des
    tueries de masse à l’encontre des couples et des femmes, cf. le
    listing [sur Wikipédia](https://fr.wikipedia.org/wiki/Incel) (il est
    malheureusement régulièrement mis à jour).

[^66b]: Ici aussi&nbsp;:  Jack Peterson, *Why i’m leaving incel* ([Youtube](https://www.youtube.com/watch?v=ng29REVIyTQ))&nbsp;; 
    Nina Pareja, «&nbsp;[un *incel* repenti regrette le manque d’ironie du mouvement masculiniste](http://www.slate.fr/story/163424/peterson-incel-repenti-regrette-manque-ironie-mouvement-masculiniste)&nbsp;», 
	*Slate.fr*, 2018&nbsp;; et un [article du Guardian](https://www.theguardian.com/world/2018/jun/19/incels-why-jack-peterson-left-elliot-rodger).

[^67]: Ou «&nbsp;farfadet de la dialectique à chapeau noir&nbsp;». Ceci n’est pas
    un terme de l’Académie française pour troll, mais une proposition
    d’un internaute qui a répertorié [d’autres propositions ici](https://twitter.com/Nosferalis/status/1260468733274882063).


