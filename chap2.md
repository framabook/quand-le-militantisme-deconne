<!------------ CHAP 2 le militantisme deconnant un sabotage ----------->

## Le militantisme déconnant, un sabotage&nbsp;?&nbsp;!&nbsp;?

Si nous parlons ici de sabotage, c’est parce que nous allons dans un
premier temps prendre exemple sur l’activité
malveillante d’infiltration dans des mouvements
militants afin de les rendre inefficaces voire de
les détruire, une activité pratiquée par les adversaires au
mouvement&nbsp;: il peut s’agir par exemple d’adversaires
idéologiques qui ne sont pas des professionnels, comme les
fascistes peuvent créer de faux profils
«&nbsp;SJW&nbsp;»[^31] (qu’ils estiment ennemis) et jouer à
la militance déconnante pour les décrédibiliser et détruire globalement
l’image de la cause. L’histoire évoquée précédemment où il a été
exigé d’ajouter un «&nbsp;TW&nbsp;»[^32] sur une recette contenant
du Kiri ressemble fortement à une pratique de ce genre, jouée par
quelqu’un qui chercherait à ridiculiser les végans, à les faire
passer pour des «&nbsp;fragiles&nbsp;» (mais ce n’est qu’une hypothèse,
rien ne permet d’affirmer qu’il y aurait derrière un véritable
saboteur anti-vegan, je me permets de prendre cette illustration
justement parce qu’un saboteur hostile ne s’y serait pas mieux
pris).

Ici, on va surtout aborder le jeu de l’infiltration selon
les conseils des renseignements, à savoir l’OSS (ex-CIA, qui a
déclassifié des documents datant de la Seconde Guerre
Mondiale).

Attention, si nous nous appuyons sur les techniques de sabotage social
de l’OSS pour illustrer le militantisme déconnant, ce n’est aucunement
pour affirmer que tout saboteur qui nuit au mouvement est
systématiquement un adversaire infiltré. Ce que nous voulons au
contraire montrer, c’est qu’un militant déconnant s’évertue sans le
vouloir à saboter de l’intérieur son propre mouvement, à savoir&nbsp;:
susciter de la méfiance ou de la répulsion chez le potentiel allié ou
spectateur, offrir à l’adversaire une relative tranquillité permettant à
ce dernier de nuire de plus belle, de gripper le travail d’information
efficace, empêcher de construire et de régler les problèmes de fond qui
sapent la militance.



### Les leçons de déconnage par l’OSS


Dans les années 40, l’OSS a rédigé un guide à
l’usage des citoyens de pays occupés durant la Seconde Guerre
Mondiale afin de les aider à entraver l’activité des
Nazis, notamment parce que les citoyens étaient forcés de
travailler pour eux. Tout était bon à prendre pour générer et
propager l’improductivité, l’inefficacité dans presque tous les
corps de métiers, ce qui était bénéfique pour les citoyens,
résistants et alliés puisque la production était pompée par
l’occupant et servait à la destructivité. Ainsi, ces leçons font
l’inventaire de ce qui pourra perturber, entraver les environnements
sociaux.


<figure>
    <img src="Pictures/simplesabotage.png"
         alt="Simple Sabotage Field Manual by United States. Office of Strategic Services">
    <figcaption><em>Simple Sabotage Field Manual by United States. Office of Strategic Services</em>.  Vous pouvez retrouver le guide au complet sous tous les formats <a href="http://www.gutenberg.org/ebooks/26184?msg=welcome_stranger">ici</a>&nbsp;; un mot de la CIA à son sujet sur <a href="https://www.cia.gov/news-information/featured-story-archive/2012-featured-story-archive/simple-sabotage.html">cia.gov</a>&nbsp;;  on a traduit une partie du guide <a href="https://www.hacking-social.com/2016/05/09/être-stupide-ou-lart-du-sabotage-social-selon-les-lecons-de-la-cia/">ici</a>.</figcaption>
</figure>



Par exemple, l’OSS conseille aux saboteurs ceci&nbsp;:

> «&nbsp;(2) faites des «&nbsp;discours&nbsp;». Parlez aussi fréquemment que possible et très longuement. Illustrez vos «&nbsp;points&nbsp;» par de longues anecdotes et expériences personnelles. N’hésitez pas à faire quelques commentaires patriotiques appropriés.
> 
> (6) Reportez-vous aux questions résolues de la dernière réunion et tentez de rouvrir le débat à leur sujet.
> 
> (8) Inquiétez-vous au sujet de la légalité et de la légitimité de toute décision&nbsp;: posez la question de savoir si telle action envisagée relève ou non de la compétence du groupe, inquiétez-vous publiquement du fait que cela pourrait être une action qui entre en conflit avec la politique des supérieurs.&nbsp;»

On pourrait retrouver ces comportements dans la militance&nbsp;:
par exemple, lors de réunions ou de communication portant sur la
création d’un site web, les saboteurs vont discourir
sans fin sur un point hors-sujet ou peu pertinent alors que l’ordre du
jour était clair et précis&nbsp;; ils vont s’énerver
sur l’usage inapproprié d’un smiley&nbsp;; ils vont tenir un discours
narcissisant en vantant leurs victoires/qualités
ou compétences&nbsp;; ils vont débattre sans fin sur la pureté
éthique de l’hébergeur&nbsp;; etc. Sur les réseaux sociaux/les
tchats, cela peut se faire en détournant une
conversation portant sur un sujet X très clair en parlant
de tout autre chose, par exemple en lançant un débat
enflammé sur tel mot employé (et en ignorant totalement le sujet X).

Résultat&nbsp;: l’action n’avance pas, donc la cause non plus. Tout le
monde craint la future réunion ou communication parce que ça va être
très saoulant et inutile, certains n’osent plus soulever des points
pertinents tant la parole est occupée et le débat dominé par ces
individus. Le sentiment d’impuissance s’installe.

> «&nbsp;(4) Posez des questions non pertinentes aussi fréquemment que possible.
> 
> a. donnez des explications longues et incompréhensibles quand vous êtes interrogé&nbsp;»

Par exemple, lors d’un live sur internet, cela peut
se manifester par le fait de soulever des questions qui ne sont pas
du tout dans le sujet. Imaginons une discussion portant sur la
place de la philosophie dans la vulgarisation, une remarque sapante
consistera à demander pourquoi l’interlocuteur boit un coca,
emblème d’une horrible multinationale alors qu’il
faudrait tous être parfaitement anticapitalistes/écolos&nbsp;; en
lui posant des questions sans lien avec le propos, s’il connaît
l’UPR (question qui sera réitérée autant de fois que possible)&nbsp;;
le suspecter de mensonge et d’incompétence parce qu’il ne cite pas de
tête l’intégralité des chiffres de telle étude et ceux des 15
méta-analyses qui ont suivi&nbsp;; en lui demandant avec un ton
accusateur pourquoi il ne parle pas de l’attentat
qui a eu lieu il y a 7 mois alors que c’était
horrible et que c’est ça le vrai ennemi, le principal sujet,
etc.

On pourrait aussi taxer ces méthodes de trolling, mais le
but du trolling consiste juste à instaurer une
forme de chaos et pour le troll, le but de tester n’importe quel
comportement&nbsp;; or un militant déconnant peut sincèrement croire que sa
technique autosabotante est bonne pour son mouvement&nbsp;:


<figure>
    <img src="Pictures/upr.png"
         alt="Campagne UPR">
    <figcaption>J’ai été étonnée que la technique extrêmement saoulante de 2017 de l’UPR était en fait assez formalisée, semblait être considérée par ces militants comme une «&nbsp;bonne stratégie&nbsp;» (<a href="https://twitter.com/UPR_Ressources/status/844165147165908992">Source</a>).</figcaption>
</figure>



Résultat&nbsp;: ça saoule les intervenants et ceux qui sont intéressés
par le sujet, ça coupe ou empêche de voir les questions pertinentes
(donc ça entrave une communication, un débat constructif qui
aurait pu avancer), ça dégrade l’image de la cause des
questionneurs auprès des spectateurs parce que c’est associé à une
espèce de prosélytisme intempestif ou
à des interruptions non pertinentes.

> «&nbsp;(5) Soyez tatillon sur les formulations précises des communications, des procès-verbaux, des bilans.
> 
> d. soyez aussi irritable et querelleur que possible sans pour autant vous mettre dans l’ennui.&nbsp;»

Ici on arrive à un grand classique des réseaux sociaux, qui a débuté il
me semble par le mouvement des grammar nazis (qui sont à ma grande
surprise de «&nbsp;vrais&nbsp;» militants qui croient sincèrement en leur
croisade[^33], même si manifestement personne n’est jamais tombé
amoureux de la langue française en se faisant juger comme un criminel
sous prétexte qu’il avait mal gèrè l’inclinaison d’un accent) qui va
commenter uniquement pour souligner les fautes d’orthographe, de
grammaire, ou pour critiquer l’usage des anglicismes, la prononciation
des mots, l’accent, l’espace en trop, la TYpogrAPHie..................
ETC............ Et ça donne ce genre de chose&nbsp;:

> «&nbsp;Un jour, j’ai discuté avec un mec qui avait signalé une faute de ponctuation dans le statut Facebook d’une amie qui remerciait les gens pour leurs condoléances à la suite du décès de sa mère... Il n’avait même pas vraiment lu le statut en question. Et j’ai compris que moi non plus, je ne lisais pas vraiment les statuts. Je me contentais de chercher les fautes.&nbsp;» ([20minutes.fr](https://www.20minutes.fr/culture/1925415-20160919-orthographe-grammar-nazis-repentis-racontent-pourquoi-embeteront-plus-fautes))


Ce type de chasse et de procès à la «&nbsp;faute&nbsp;» peut aussi se faire à
l’encontre de termes qui sont accusés d’être malveillants&nbsp;: par exemple,
une personne vient se confier, à bout, dans l’espoir de trouver du
réconfort et de l’aide (même en privé) auprès de ses alliés, et ne
reçoit en retour qu’une violente correction «&nbsp;*ce mot que tu as employé
est psychophobe/insultant envers les travailleurs du sexe/homophobe/,
etc*&nbsp;», sans que son propos ait même été entendu&nbsp;:

> «&nbsp;Récemment, elle [une militante] a vu avec amertume une jeune mère, victime de violences, qui sollicitait de l’aide sur un groupe Facebook de parentalité féministe, se faire corriger, car elle n’employait pas les termes jugés inclusifs pour les personnes trans ou non binaires. «&nbsp;Elle avait besoin de manger, pas qu’on lui dise comment s’exprimer.&nbsp;» ([neonmag.fr](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html))

On voit que cette tatillonerie s’oppose directement au travail militant,
dans le sens où s’attaquer au terme mal employé a pour conséquence de ne
pas aider cette femme bien que ce soit pourtant un objectif central du
mouvement. Le travail est donc directement saboté avant même de
commencer.

La formulation précise devient un devoir tellement suprême que le
militant déconnant semble supprimer toute empathie pour autrui, nie son
émotion, ses besoins et encore plus si c’est un allié.

Résultat&nbsp;: les personnes n’osent pas parler sans over-justifier leur
propos, n’osent pas exprimer des émotions ou aborder des sujets qui les
touchent de peur d’utiliser de mauvais termes malgré eux, voire n’osent
même pas rejoindre les mouvements de peur de ne pas avoir les bons mots,
les bons codes&nbsp;:

> «&nbsp;Elle est étudiante et souhaite s’engager pour la première fois dans l’association dont je suis membre. Au téléphone, elle tourne autour du pot, hésitante, comme tourmentée. Et finit par admettre qu’elle a très peur de mal s’exprimer. De ne pas employer les mots justes. De ne pas savoir. Sa crainte a étouffé jusqu’ici ses envies d’engagement. Tandis que je tente de la rassurer, je lis dans son angoisse la confirmation d’un phénomène que j’observe depuis que j’ai l’œil sur les mouvements de défense de la justice sociale&nbsp;: une forme d’intransigeance affichée, propre à inhiber ou décourager certaines bonnes volontés. Une course à la pureté militante qui fait des ravages. ([neonmag.fr](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html)).

L’autre conséquence de tout cela est d’en venir à percevoir le mouvement
comme un ennemi, puisqu’à force les seules interactions que l’on peut
avoir avec lui en tant que tiers peuvent n’être qu’attaque et
réprobation, et par un phénomène d’escalade et de réactance[^34], le
tiers ciblé va progressivement se positionner dans le clan adverse.

Globalement, l’action est freinée, l’adversité n’est pas combattue,
voire est protégée par ces comportements (elle peut continuer sa
destructivité en toute tranquillité). Plutôt que de convaincre, la cause
est de plus en plus décrédibilisée et associée à une nuisance, les
alliés et spectateurs sont usés de tant d’inefficacité ou de subir tant
de reproches constamment. Il y a donc sabotage de la cause. Du moins ce
sont des pratiques conseillées par les renseignements si on veut rendre
inefficace, infécond un environnement social qu’on estime adversaire et
ne pas être repéré comme saboteur. Si on est un militant sincère dans
ses engagements, je doute que d’obtenir ces résultats soit satisfaisant.

Je n’ai parlé que de l’OSS pour montrer à quel point le militantisme
déconnant ressemble à un sabotage (donc, pourquoi le poursuivre,
pourquoi persister à croire que c’est un «&nbsp;bon&nbsp;» jeu&nbsp;?), mais on aurait
pu continuer le parallèle avec le travail d’infiltration ou de
contre-propagande, avec des exemples plus modernes comme ceux du
renseignement/contre-renseignement (Cointelpro[^35], JTRIG[^36]) ou
même les stratégies des fascistes[^37].


<figure>
    <img src="Pictures/trig.png"
         alt="GCHQ / JTRIG">
    <figcaption>Toutes les disciplines dans lesquelles le renseignement (ici anglais, GCHQ / JTRIG) pioche pour mieux duper, détourner, manipuler, etc. sur le Net. Il s’agit d’un <em>leak</em> de Snowden. Quand des membres du gouvernement dénigrent les sciences humaines et leur utilité, c’est de la pure hypocrisie (ou de l’ignorance&nbsp;?? mais j’en doute) puisque le renseignement (comme la communication politique) pioche largement dedans pour ses stratégies. Plus d’infos sur <a href="https://theintercept.com/2014/02/24/jtrig-manipulation/">The Intercept</a>.</figcaption>
</figure>



<!------------ notes CHAP 2 ----------->



[^31]: SWJ = *Social Justice Warrior*, guerrier pour une justice
    sociale. Le terme a pris avec le temps une connotation négative,
    plus d’infos [sur Wikipédia](https://fr.wikipedia.org/wiki/Social_justice_warrior).

[^32]: TW = *Trigger Warning* (avertissement) il s’agit d’une mention
    utilisée souvent sur les réseaux sociaux afin de prévenir d’un
    contenu pouvant choquer ou réveiller des traumatismes chez les
    personnes

[^33]: Benjamin Chapon, «&nbsp;[Orthographe: Des Grammar Nazis repentis racontent pourquoi ils ne vous embêteront plus avec vos fautes](https://www.20minutes.fr/culture/1925415-20160919-orthographe-grammar-nazis-repentis-racontent-pourquoi-embeteront-plus-fautes)&nbsp;», *20 Minutes*, 19/09/2016.

[^34]: Lorsqu’on interdit quelque chose à quelqu’un, qu’on le rend
    moins accessible ou qu’on lui retire une possibilité d’action qu’il
    avait auparavant, l’individu aura tendance à la vouloir plus, quand
    bien même il n’en avait cure avant. C’est la réactance, une réaction
    irréfléchie devant l’interdit, qui parfois fait choisir à l’individu
    des choses qui lui nuisent, lui sont inutiles, ou nuisent à ses
    proches/la société. Par exemple, vouloir polluer à cause d’interdits
    écologiques, refuser des vaccins à cause d’obligations à se faire
    vacciner de la part des autorités, se mettre à aimer un contenu nazi
    parce que celui-ci a été censuré, etc. Nous avons fait [une vidéo à
    ce sujet](https://skeptikon.fr/videos/watch/674aa752-4bd0-40d7-9298-66c845e4872a).

[^35]: Plus d’infos sur [Korben.info](https://korben.info/techniques-secretes-controler-forums-opinion-publique.html) et sur *[Le Monde Diplomatique](https://www.monde-diplomatique.fr/1995/08/COMBESQUE/6545)*.

[^36]: Une présentation générale sur [Wikipédia](https://fr.wikipedia.org/wiki/Joint_Threat_Research_Intelligence_Group). Tous les documents révélés par Snowden à ce sujet sur [search.edwardsnowden.com](https://search.edwardsnowden.com/search?utf8=%E2 %9C%93&q=JTRIG&document_topic_facet=Information+Operations) (qui détaille certaines stratégies et montre l’efficacité des
    opérations de contre-propagande/infiltration pour saper l’image d’un mouvement ou carrément le détruire)

[^37]: Midi Libre, «&nbsp;[Un militant repenti balance les secrets de l’ultra-droite](https://www.midilibre.fr/2012/10/08/un-militant-repenti-balance-les-secrets-de-l-ultra-droite,574771.php)&nbsp;», 08/10/2012.


