<!-------------- INTRO Quand le militantisme déconne ---------------->

# Quand le militantisme déconne&nbsp;: injonctions, pureté militante, attaques...



Si vous cherchez un article à charge contre le militantisme en
général, je me dois de vous prévenir d’emblée, ce ne sera pas le
cas ici&nbsp;: j’ai été militante à trois reprises, dans des
milieux radicalement différents (en syndicat, parmi des hackers
*grey hat*[^1], parmi des youtubeurs) et ces
trois expériences ont été mémorables à bien des titres. J’ai appris
énormément auprès des autres militant·es, dans
l’action, même dans les moments les plus pénibles,
comme ces moments de confrontation avec
«&nbsp;l’adversaire&nbsp;» (celui qui représentait/défendait le
maintien des problèmes structurels pour lesquels
on luttait). J’ai eu des opportunités de faire des choses que je
ne pensais jamais pouvoir faire dans ma vie. Cela m’a
prouvé que même si l’on est officiellement «&nbsp;sans
pouvoir&nbsp;», en fait si, on peut choper un pouvoir d’agir, pour
transformer les choses, et ensemble ça peut marcher, avoir des
effets conséquents. Jamais je ne regretterais d’avoir participé
à tout ça, connu de telles expériences, même avec
tous les aspects négatifs qu’elles ont pu avoir,
que ce soit à travers les pressions, les déceptions, les
difficultés, la violence et la paranoïa, les faux pas&nbsp;: parce
qu’on était là, ensemble, et on sortait de l’impuissance, on créait
quelque chose qui transformait un peu les choses, qui comptait, qui
était juste.

J’ai aussi une énorme sympathie pour les milieux militants que je
n’ai pas expérimentés. Par exemple, je n’ai jamais
milité directement pour le libre, mais j’ai toujours eu plaisir
à découvrir les nouveautés libres, à les tester, à les
adopter parfois. Et ça vaut pour tout un tas de mouvements très
variés.

Pourquoi cette sympathie et gratitude générale pour les
militants&nbsp;? Très sincèrement parce que leurs actions
répondent parfois à mes besoins (psychologiques ou
non) et ont une résonance particulièrement
empuissantante que j’aime ressentir, que ce soit
grâce à l’astuce d’une chimiste écolo pour fabriquer
ses produits cools, les crises de rire
devant le Pap 40, l’appréciation esthétique qu’offre le travail
souvent engagé de Banksy, le réseau social libre qui me
laisse plus de caractères que Twitter, la
sororité féministe qui m’a permis d’éviter mes foutus biais
d’internalité, l’accès à l’info rendu possible grâce
par des _grey hat_ qui littéralement me permet de travailler
pleinement au quotidien (Merci Aaron Swartz,
Alexandra Elbakyan, Edward Snowden, et tous les
inconnu·es qui bidouillent dans l’ombre à libérer
l’info), etc. Et si tout cela répond à mes besoins,
résonne, m’augmente, aide à me développer, m’offre des solutions ou
m’ouvre l’esprit à d’autres solutions, cela doit avoir cet effet
bénéfique chez d’autres. Et c’est effectivement le cas, quand on
étudie la militance sous une perspective historique, à
travers les décennies.

<figure>
    <img src="Pictures/pap40.png"
         alt="Le pap’40 en action">
    <figcaption>Le pap’40 en action (<a href="https://www.dailymotion.com/video/x1l4bqm">sur dailymotion</a>)&nbsp;; sa <a href="https://www.youtube.com/user/EgliseTSC">chaîne Youtube</a></figcaption>
</figure>


Là, le bénéfice est à un autre niveau que je qualifierais de
totalement épique&nbsp;: les changements sociaux proviennent toujours
d’abord de collectifs qui militent pour faire avancer
les choses.

On découvre que cette puissance de l’action militante
résonne à travers les siècles, on peut la sentir lorsqu’on
étudie l’histoire des Afro-américains et la conquête de
leurs droits, l’histoire des résistances sans armes durant la
Seconde Guerre Mondiale, l’histoire du
féminisme, l’histoire LGBT, l’histoire des
hackers et bien d’autres mouvements&nbsp;!

Tout, de la petite info à la petite innovation, du mouvement massif
à l’action solitaire la plus risquée, nous offre un
espoir de dingue&nbsp;: on peut changer le monde en mieux, on peut le
faire ici et maintenant, on peut avoir ce courage, et ce
qu’importe la taille de la destructivité contre laquelle
on lutte, que ce soit contre un régime autoritaire,
contre une situation de génocide, d’esclavage, de
manipulation, de crise, d’oppression, de dangers… On peut
tenter quelque chose pour que cette résistance
fonctionne au mieux, même si on n’a rien.
Et on peut le faire avec une arme de compassion massive qui
irradiera pour des générations entières. Franchement, c’est à pleurer de
joie et de soulagement prosocial que de lire les histoires des
sauveteurs durant la Seconde Guerre Mondiale,
c’est ouf la force de construction que transmettent les écrits de
Rosa Parks, Martin Luther King, et tant
d’autres.

J’ai tellement de gratitude pour toutes ces personnes, pour
quantité de mouvements, c’est pourquoi je tente d’en parler quand je
le peux, sous l’angle des sciences humaines&nbsp;: je suis
partageuse de contenus sur le Net à travers
Hacking-social/Horizon-gull depuis plus de 7
ans avec Gull, d’une façon engagée. On vulgarise les
sciences humaines d’une façon volontairement
non-neutre, c’est-à-dire en prenant
parti contre tout ce qui peut détruire, oppresser, exploiter,
manipuler les gens, et en partageant tout ce qui
pourrait aider à viser plus d’autodétermination.

L’œuvre des militants, des activistes et autres
engagés fait donc partie de ma ligne éditoriale
depuis la création du site HS.

Mes angles éditoriaux se sont transformés au fil du
temps, d’une part pour une raison assez positive qui
est une addiction de plus en plus prononcée pour fouiner dans la
littérature scientifique, et d’autre part parce que j’ai commencé à
rechigner à parler des combats militants
actuels, quand bien même je les soutenais, et parce que j’avais plein de
choses positives à dire.

Progressivement, j’ai opté pour des compromis, comme parler
de militance uniquement si celle-ci avait pu
être étudiée au travers de recherches scientifiques
en sciences humaines et sociales. C’est ce que j’ai pu faire d’ailleurs
en toute tranquillité avec la justice transformatrice,
qui aborde en partie le travail de militants
abolitionnistes du système pénal, et qui a été bien
accueilli sans doute parce que ce n’était pas un
sujet ni d’actu, ni français. Ça n’a hurlé que
très peu, et seulement sur un réseau social pour lequel je
m’attendais à ce genre de réaction épidermique.

Je me rends compte aussi que plus les années
ont filé, moins j’ai repartagé de contenus divers sur les réseaux
sociaux (tout confondus), moins j’ai fait de posts sur ceux-ci,
moins j’ai osé m’exprimer, poser des questions, faire des
remarques, etc. Actuellement, je constate que je ne partage que
des infos liées à nos thèmes habituels (alors
que je croise tout un tas de contenus que je pourrais
partager). Parfois, je n’informe même pas de
notre activité sur *certains* réseaux sociaux parce que
je n’ai pas l’énergie/la patience de gérer certaines
réactions&nbsp;: l’énervement à cause d’une
image/ de l’entête pour des raisons qui n’ont rien à
voir avec l’article&nbsp;; le mépris parce que j’ai
posté ça aussi sur une plateforme impure
liée aux GAFAM[^2]&nbsp;; la colère parce
que le sujet principal de l’article ne portait pas
précisément sur celui que certains auraient voulu&nbsp;;
le dégoût parce qu’il y a un anglicisme/un
néologisme&nbsp;; les injonctions à mettre des
TW[^3] ou autres balises alors que j’ai consacré des
paragraphes complets à faire des avertissements dans
l’article&nbsp;; la condescendance pour la recherche/la
statistique/l’expert·e citée, car elle a été pointée du doigt
un jour par un tribunal
pseudo-scientifique/pseudo-zététicien/pseudo-sceptique
(que je prenne en compte pendant X pages de critiques
scientifiques et que je les discutent ne compte pas, j’aurais
dû ne pas en parler tout simplement, car y faire
référence c’est déjà trop)&nbsp;; le rejet par un
tribunal grammar-nazi-académique parce que j’ai mis mes
sources bibliographiques à la fin de l’article et non
dans le corps de texte, etc.



Et dans ma tête, il y a ainsi une liste noire
qui grandit au fur et à mesure, de sujets à ne pas ou ne plus
aborder sur le Net, quand bien même je les estime
avec amour ou qu’ils comptent à mes yeux, que je
sens que ça pourrait être utile à autrui, parce que je sais que cela ne
sera pas entendu de la sorte, et que, avouons-le, je n’ai pas le courage
d’encaisser ce qui va s’ensuivre. J’ai autocensuré notamment
des problématiques militantes pour lesquelles je suis directement
concernée, parce que lisant comment cela se passait pour
d’autres sur les réseaux sociaux, en découvrant la
pureté requise pour pouvoir en parler, et la
méfiance qu’on aurait d’emblée à mon égard de part mes
«&nbsp;endogroupes&nbsp;»[^4] si je les révélais, je
sais d’expérience que les attaques
toucheraient trop à ma personne et
que, sous le feu de l’émotion, je serais capable de
m’auto-annuler, c’est-à-dire
détruire mon existence sur le Net, et mon
travail avec. Le fait d’avoir vu tant de collègues détruits
de la sorte, alors qu’ils avaient un propos
doux, juste, socialement tellement utile,
créatif, me confirme malheureusement que cette liste
noire de sujets, je dois la maintenir pour l’instant si je
veux continuer à faire ce que je fais.



«&nbsp;Encore un boomer qui crie "on peut
plus rien dire gnagnagnaa" parce que des militants
soulignent ses erreurs et qu’il ne veut
pas les assumer&nbsp;!!&nbsp;» pourriez-vous me dire&nbsp;; je
comprends tout à fait qu’on puisse avoir ce réflexe quand quelqu’un se
plaint du militantisme-correcteur du Net. Mais cette
liste noire qu’est la mienne ne veut pas dire que
j’estime que je ne peux plus rien dire. Elle veut surtout dire que
j’ai dû développer au fil du temps des stratégies et des
hacks plus ou moins sournois non pas pour pouvoir
m’exprimer sans me prendre des reproches plein la tête, non
pas pour convaincre[^5], mais pour ne pas être
dégoûtée de mes propres engagements à
cause de quelques militants qui partagent
ces mêmes engagements. Pour le dire plus
simplement&nbsp;: j’ai peur du jugement des alliés.

Je n’ai pas d’angoisses
particulières vis-à-vis des individus que je
sais «&nbsp;adversaires&nbsp;» de ce dont je vais parler. Par
exemple, je suis assez détendue sur le fait de parler de
l’autoritarisme comme quelque chose de problématique et
j’attends parfois avec impatience les défenseurs
de l’autoritarisme pour discuter avec eux. Comme je ne cherche
pas à convaincre, je n’ai absolument pas de problème à ce
qu’ils rejettent le contenu avec véhémence, ce qui
est d’ailleurs plutôt cohérent de leur part. Je
peux même être admirative quand ils arrivent à
exprimer pleinement, sans complexes, ce qui les gêne, parce qu’ils
s’affichent sincèrement avec leurs valeurs, quand bien
même elles sont parfois horribles (au hasard, en appeler à
tuer les 3/4 de la population ou envoyer en enfer les gens
comme moi). Et dans cette confrontation sincère, je
récolte des informations précieuses pour de
futurs contenus (les adversaires véhéments sont au fond
d’excellents contributeurs *involontaires*).

Par contre, c’est vraiment dur de se prendre de la haine de la part
d’individus qui, je le constate après discussion, ont les
mêmes valeurs que celles diffusées dans le contenu
qu’ils blâment, ont les mêmes engagements, les mêmes
objectifs. Ils me crient dessus parce que je n’ai
pas parlé de ceci ou de cela, que j’ai utilisé un mot qui ne
leur plaît pas, un en-tête qui aurait dû d’une
manière ou d’une autre contenir tout l’article, parce que je
n’ai pas utilisé un mot ou cité une référence qui leur
paraissait indispensable, etc. Bref, ces reproches
et cette colère de la part d’alliés, de camarades,
de confrères, j’ai eu beaucoup de mal à les digérer et à les
comprendre, d’autant plus qu’ils sont advenus pour des
sujets auxquels je ne m’attendais pas
le moins du monde.

Un jour j’ai parlé par exemple d’une
expérience d’éducation alternative[^6]. Je me suis
centrée sur ce qui s’y faisait et était
particulièrement cool dans le développement de l’autonomie et de
l’empuissantement de l’enfant, au top vis-à-vis
de ce qu’on sait en psycho- et neuro-. Puis j’ai
fait ce que j’estime à présent une erreur, j’ai repris
toutes les critiques disponibles à l’encontre de
cette expérience afin de les debunker. C’était du
débunkage ultra simple, puisque les 3/4 des critiques
portaient sur des éléments qui n’étaient même pas présents
dans cette expérience ou encore s’attaquaient
personnellement à la personne qui l’avait menée sous forme de procès
d’intention, or ces caractéristiques n’avaient strictement aucun
lien avec l’expérience elle-même. Plus important à mes
yeux, les critiques vantaient parallèlement un mode
d’éducation autoritaire, très conservateur (globalement, on
revenait 100/150 ans en arrière) basé sur le
contrôle&nbsp;; ils voyaient le plein
développement de l’autonomie chez l’enfant comme une
menace. Scientifiquement[^7], on sait pourtant que
le contrôle autoritaire sape la motivation et le potentiel
des enfants&nbsp;; le seul «&nbsp;atout&nbsp;» de l’éducation autoritaire est de
transformer la personne en pion, contrôlable
extérieurement par des autorités, et intérieurement
par des normes pressantes.

Les critiques n’ont pas fait gaffe à ça, et se
sont plutôt ralliés à ces autoritaires pour tenter
de me convaincre à quel point la personne ayant mené
l’expérience devait être annulée pour de
soi-disant accointances avec le
néolibéralisme (ce qui est totalement faux quand on
analyse le travail dans les faits), ignorant totalement les
détails de l’expérience et ses apports. Et ce type
d’accusations venait d’individus qui, très souvent,
était des militants actifs menant des expériences éducatives quasi
similaires à celles que cette expérience vantait…
C’était ouf, et même des années après publication de ce
contenu je reçois encore des messages pour tenter de me
convaincre que cette personne est mauvaise, donc que
l’expérience l’est, et que je devrais annuler mon avis
positif.

Tout ceci a été vraiment saoulant, d’autant plus
que naïvement je pensais qu’on pouvait espérer une sorte de
cohésion contre les modes éducatifs
autoritaires/contrôlants, ce qui est le point commun
à quasi toutes les expériences d’éducation
alternative. Mais non. Ça a été l’article le plus
«&nbsp;polémique&nbsp;» du site, je n’en reviens toujours
pas.

Depuis ce jour, je n’ai plus parlé d’éducation alternative, même si
pourtant d’autres militants m’avaient proposé de façon
stratégiquement plus intéressante de faire découvrir
leur école et leur mode de fonctionnement. Je suis passée à
des sujets plus _safe_, l’un de mes pare-feux étant
désormais de parler d’études, expériences, actions se déroulant
hors de notre pays et n’étant pas d’actualité, ou très
indirectement.

Aussi, je ne debunke plus rien (du moins, pas de cette
façon officiellement affichée), parce que cela n’a strictement
aucune résonance constructive[^8], et j’ai l’impression que
certains se servent de ce qui est alors estimé comme «&nbsp;vrai&nbsp;» pour
se permettre d’attaquer (de manière
disproportionnée) ceux qui sont dans le «&nbsp;faux&nbsp;», ce qui est une dynamique qui
ne m’intéresse pas du tout d’alimenter.

À la place, je bidouille pour trouver des sujets à écho et
je les tricote d’une façon à ce que, quand même, ils fassent
résonance avec ce que nous vivons ici et maintenant. D’un
côté, cela aura eu le grand avantage de me pousser à chercher des sujets
inédits et à développer une forme de créativité plus hackeuse
que je n’aurais peut-être pas eu sans cette saoulance.

Bref, tout ça pour dire que je ne m’autocensure pas par peur
des «&nbsp;ennemis&nbsp;», mais davantage par crainte de la
punition des alliés et acteurs de cette cause
commune que nous défendons. Je constate que d’autres
créateurs de contenus partagent cette même
crainte de l’endogroupe davantage que de leur ennemi&nbsp;:

<figure>
    <img src="Pictures/cancelling.png"
         alt="L’annulation (le canceling) - ContraPoint">
    <figcaption>L’annulation (le canceling) - ContraPoint. Sur Youtube sous-titré en français (<a href="https://www.youtube.com/watch?v=OjMPJVmXxV8">ici</a>) et sur Peertube, sous-titré en anglais et en espagnol (<a href="https://peertube.parleur.net/videos/watch/2c55681b-c3cb-4261-a7a0-42d05da5111f">ici</a>)</figcaption>
</figure>



Et ça vaut malheureusement aussi pour le libre&nbsp;: j’ai toujours
autant de respect et de sympathie pour le libre, parce que j’ai la
chance de connaître des libristes fortement prosociaux que
j’adore, et que j’utilise au quotidien du libre -- ça répond à
mes besoins de compétence et de sécurité numérique --, et,
ayant adopté depuis longtemps une certaine éthique hacker,
j’adhère au discours libriste qui fait partie de la grande famille
hacker.

Mais la campagne de certains militants pour
PeerTube a été, malgré tout, extrêmement
saoûlante.

Au départ, Gull et moi-même étions enthousiastes
pour promouvoir et publier sur PeerTube, on s’est
vite renseignés, on a été hébergés sur une instance dès que
cela a été possible. Parfois, il y avait des *down* sur
l’instance en question, alors selon l’état de la mise en
ligne, je partageais ou non le lien des vidéos via
PeerTube. Qu’importe notre présence ou
absence ponctuelle, on nous a reproché de ne pas être sur
PeerTube, on nous a fait de longs messages
condescendants pour nous expliquer pourquoi il fallait être sur
PeerTube et pourquoi il fallait
arrêter d’être sur YouTube, on nous a
engueulés parce que l’instance était *down* et
que de fait telle vidéo ne pouvait ponctuellement être vue
que sur Youtube. On ne pouvait pas annoncer
une vidéo avec joie sans que celle-ci soit
instantanément rabattue par un commentaire reprochant
notre manque d’éthique à mettre un lien Youtube et
non PeerTube (alors qu’au début on partageait
prioritairement le lien PeerTube) ou encore rappeler
une énième fois ce qu’était PeerTube comme si nous
l’ignorions et en quoi nous devions moralement y être. On
faisait au mieux, on était déjà convaincus par PeerTube,
on se démenait pour trouver des alternatives, même sur
YouTube on avait pris le parti dès le départ de démonétiser tout
notre contenu, mais visiblement ce n’était pas encore assez
parfait.



<figure>
    <img src="Pictures/messageframablog.png"
         alt="Un excellent article de framablog à ce sujet et pourquoi la saoulance n’est pas la solution pour promouvoir PeerTube">
    <figcaption><a href="https://framablog.org/2020/10/29/message-aux-youtubeurs-youtubeuses-et-surtout-a-celles-et-ceux-qui-aiment-leurs-contenus/">Un excellent article de framablog</a> à ce sujet et pourquoi la saoulance n’est pas la solution pour promouvoir PeerTube</figcaption>
</figure>


J’étais également très enthousiaste à notre
arrivée sur Mastodon, j’y postais même des trucs que
je ne postais pas sur les autres réseaux, j’envisageais de faire
là-bas un compte perso. Mais, à part
quelques personnes que je connaissais et qui
étaient bien sympas, progressivement les retours que
j’avais sur un article, un dossier, un bouquin, un live, ne
concernaient plus que notre faute à ne pas être des libristes
avant toute chose, il y avait suspicion que ce qu’on
utilisait était éthiquement incorrect (par exemple, la plateforme lulu
pour publier mes livres -- c’est sacrément ironique car
justement je l’avais choisie exclusivement pour éviter la vague
de reproches que des collègues peuvent avoir lorsqu’ils publient via
Amazon...).

Et d’autres personnes ont pu rencontrer ce même type
d’expérience&nbsp;:



<figure>
    <img src="Pictures/mcgodwin.png"
         alt="Messages sur Twitter">
    <figcaption>Messages sur Twitter (<a href="https://twitter.com/mcgodwinpaccard/status/1392055800076582912">source</a>)</figcaption>
</figure>







C’est terrible, mais d’une plateforme pour laquelle j’étais
enthousiaste, j’en suis venue à avoir la même politique de communication
que celle que j’avais sur Facebook (plateforme
que je déteste depuis des années),
c’est-à-dire&nbsp;: un minimum de publications, pas
d’interactions même quand il y a des remarques, écriture du
message la plus épurée possible par anticipation des
quiproquos, etc. Et si on me fait des corrections
légitimes sur la forme, mais sur un ton condescendant,
je les prends en compte à ma sauce (je n’obéis pas,
je transforme d’une nouvelle façon), et je ne fais aucun
retour. Je ne fais pas ça contre ce type de critiques, mais
davantage pour rester concentrée sur mon travail et ne pas être
plongée dans un énième débat où potentiellement
je dois me justifier pendant des heures d’un choix incompris, débat qui
est totalement infécond pour toutes les parties, et
où j’ai l’impression d’être la pire des merdes tant je dois
m’inférioriser pour calmer l’individu et lui offrir la
«&nbsp;supériorité&nbsp;» morale/intellectuelle qui l’apaise.


J’ai pris des exemples libristes, mais si cela peut vous rassurer,
j’ai le même vécu pénible avec des communautés
zététiques/sceptiques qui ont été hautement moralisatrices
et condescendantes, nous reprochant un manque de
détails dans le report des données scientifiques ou
suspectant que nous partagions une étude «&nbsp;biaisée&nbsp;» parce qu’ils
n’avaient pas compris un résultat (alors qu’il suffisait d’aller
simplement jeter un coup œil rapide à la source que nous mettions à
disposition). Ça a eu un impact sur notre activité et
notre motivation, nous amenant à un perfectionnisme
infructueux qui consistait à donner encore plus de détails
mais qui étaient pourtant inutiles à préciser, voire
nuisibles à une vulgarisation limpide[^9]. Plus grave, j’ai vu
des phénomènes dans des communautés zététiques/sceptiques qu’on
a décidé de quitter très rapidement tant le
climat devenait malsain&nbsp;: des activités marrantes selon eux
étaient de se faire des soirées foutage de gueule d’un créateur de la
même communauté zététique, faire des débats sans fin inféconds sur
l’usage d’un mot, et surtout ne jamais créer ensemble,
coopérer, s’attaquer aux problèmes de fond, aux «&nbsp;véritables&nbsp;»
adversaire~~s~~ qui ne sont pas tant des individus, mais
des systèmes, des structures, des normes, etc. Mais que les
zététiciens/sceptiques se rassurent, j’ai aussi vu ces
mêmes mécaniques chez des membres de syndicat (par exemple,
Gull s’est vu une fois reprocher d’aller simplement
discuter avec des membres d’un autre syndicat…), chez des
militants antipub (où Gull s’est vu corrigé non sans une
pointe de mépris parce qu’il avait osé citer une marque pour la
dénoncer car pour ce critique il était
interdit de citer une marque…), etc.



Là encore, ce n’est pas qu’un souci qui nous arriverait par
manque de bol, une fois de plus Contrapoints
explique à merveille ces mécaniques qui arrivent potentiellement dans
n’importe quel groupe réuni autour de n’importe quel sujet&nbsp;:



<figure>
    <img src="Pictures/cringe.png"
         alt="Le Cringe - ContraPoint">
    <figcaption>Le Cringe - ContraPoint. Sur Youtube, sous-titré <a href="https://www.youtube.com/watch?v=vRBsaJPkt2Q">en français</a>&nbsp;; et sur Peertube, <a href="https://peertube.parleur.net/videos/watch/551dcf2f-2aad-4c60-856a-e9a40d87d0f6">en anglais</a>.</figcaption>
</figure>



Le fait que je considère la culpabilisation, le jugement, le
mépris, la condescendance, l’attaque, l’injonction, l’appel à la
pureté, comme du militantisme déconnant (à entendre comme
dysfonctionnel)[^10] n’est évidemment pas étranger au fait que
je sois fragile, état que j’assume pleinement, parfois même
avec fierté&nbsp;: oui je craque en privé devant ce militant pour
l’Histoire qui, dans un mail de 3 pages, argumente sur
le fait que je suis médiocre, inutile et tellement inférieure aux hommes
présents à cette table ronde publique car
j’ai osé utiliser le mot «&nbsp;facho&nbsp;», usage qui
selon lui prouvait que je n’y connaissais
strictement rien parce que X et Y (imaginez un cours
d’histoire sur la Seconde Guerre
Mondiale). J’ai répondu en partie en assumant
l’un de mes défauts (être nulle à l’oral) et en envoyant les
liens vers mon bouquin en libre accès sur le
fascisme qui justifie en plus de 300 pages pourquoi je garde ce mot
et pourquoi il est pertinent pour décrire certains contenus (il
ne m’a pas répondu en retour).

J’assume aussi comme mon «&nbsp;problème&nbsp;» la
déprime que je peux avoir quand j’ai pour
premier commentaire une engueulade sur l’utilisation d’un anglicisme
dans un article qui a nécessité des heures de
recherches et d’écriture.

J’assume comme mon «&nbsp;problème&nbsp;» mon surmenage et
la lâcheté que je me permets à ne plus avoir
à me justifier ni répondre aux reproches hors-sujet.

J’ai ma sensibilité, mes faiblesses, mon temps et des capacités
limitées, je fais avec, je ne me plains pas de
souffrir de ces attitudes militantes qui me sont
reloues, et je sais à peu près comment
gérer ça et transformer ça[^11]. Je ne me sens pas victime
de quoi que ce soit, je ne suis pas à plaindre.
Au final, j’estime que nous sommes très chanceux d’avoir une
communauté si soutenante, de ne pas avoir été harcelés ou
annulés massivement comme d’autres acteurs du
Net.

Ce qui m’enrage dans un premier temps avec ces histoires de
militance déconnante, c’est que cela a détruit l’œuvre de
collègues très talentueux dont j’aurais voulu découvrir encore
de nouvelles œuvres, qui avaient un magnifique potentiel
d’influences positives,
autodéterminatrices, émancipatrices.
Mais merde, mais c’était tellement, tellement injuste de
s’en être pris à eux, pour un mot, une tournure de phrase. De
les avoir détruits pour ça, pas tant avec une seule
remarque, mais bien avec cette répétition incessante de
chipotage à chaque nouveau contenu. Tellement inapproprié de les
voir en adversaire alors qu’il n’y avait pas meilleur allié.

À force de voir cette histoire de déconnance se répéter
inlassablement partout dans tous les domaines, je m’inquiète pour
les visées fécondes de ces mouvements. J’en viens à
avoir peur que ces militants puristes dépensent une forte
énergie pour des ennemis qui n’en sont pas, qui ne représentent
pas une menace, qui au contraire sont de
potentiels alliés qui feraient avancer leur cause. J’ai
peur que cette division, voire hiérarchisation, entre
personnes potentiellement alliées ne serve finalement qu’à
donner davantage de pouvoir à leurs
adversaires. Peur que toute cette énergie gaspillée, au
fond, ne profite qu’à l’avancée de ce qu’on dénonce
pourtant tous et que les acteurs des oppressions, des
exploitations, des dominations, des manipulations,
aient le champ libre pour développer pleinement leur œuvre
destructrice, puisque les personnes pouvant les en
empêcher sont plus occupées à s’entre-taper dessus pour ou
contre le point médian, pour ou contre l’anglicisme, pour ou contre
l’usage de Facebook, et j’en passe.

Au-delà de ces peurs, je me désole aussi que tant
d’opportunités de collaboration, de construction commune, de
coopération, d’entraide passent alors à la trappe au profit du
militantisme déconnant qui ne fait que corriger, sanctionner
et contrôler. Parce que toutes les réussites que
j’ai pu voir étaient fondées sur une entraide, une
cohésion et une convergence entre militants, entre des groupes
différents, avec des acteurs qui, de base, ne
suivaient même pas le combat, n’avaient pas
les mêmes buts. Les réussites les plus belles que j’ai pu
avoir la chance d’apprécier regroupaient à la fois des groupes
militants assez radicaux (méthodes hautement destructives mais sans
violence sur les personnes), des groupes militants faisant du lobbying
auprès du monde distal[^12] (politiques, structures de
pouvoir), spectateurs lambda (soutien et sympathie pour la cause), et
résultat, une loi injuste ne passait pas, et tout le monde était en
sympathie avec tout le monde, ce n’était que joie.


<figure>
    <img src="Pictures/stopactaoctopus.jpg"
         alt="Par exemple, l’action conjointe de la Quadrature du Net, du Parti Pirate (notamment allemand), Anonymous (DoS), de la mobilisation citoyenne (manifestations dans toute l’Europe), et peut-être d’autres acteurs que j’oublie, a permis de stopper ACTA.">
    <figcaption>Par exemple, l’action conjointe de la Quadrature du Net, du Parti Pirate (notamment allemand), Anonymous (DoS), de la mobilisation citoyenne (manifestations dans toute l’Europe), et peut-être d’autres acteurs que j’oublie, a permis de stopper ACTA. Plus d’infos <a href="https://www.laquadrature.net/2012/07/04/acta-victoire-totale-pour-les-citoyens-et-la-democratie/">sur laquadrature.org</a>. Source image&nbsp;: <a href="https://commons.wikimedia.org/wiki/File&nbsp;:Stop_ACTA_Octupus.jpg">Wikimedia</a>.</figcaption>
</figure>


C’est pourquoi dans cet article on va tenter de comprendre pourquoi
on peut avoir tendance à attaquer, injonctiver et pourquoi
ça peut être déconnant vis-à-vis des buts d’un mouvement, et
ce qu’on pourrait faire à la place de potentiellement
plus efficace.



## Avertissements sur ce contenu



Vous l’aurez sans doute compris, mais je précise pour éviter tout
malentendu&nbsp;:



&#9733; *Cet article porte sur le militantisme déconnant, ou pureté militante, et non sur le militantisme tout court*. Nous nous considérons
nous-mêmes comme des personnes engagées à travers notre activité
sur Hacking-social/Horizon-gull, et
engagées contre l’autoritarisme sous toutes ses formes, et donc par
définition, militantes.

&#9733; *Je ne catégorise pas tout le militantisme libriste comme uniquement associé à des pratiques déconnantes*, pas du tout, d’autant
que nous écrivons sur le Framablog, ce qui serait un comble.
Idem, si vous avez des affinités avec un groupe dont je
décris la pratique déconnante. N’endossez pas la
responsabilité ou la culpabilité des pratiques déconnantes si vous
ne les avez jamais faites mais que vous partagez le même
groupe que ceux qui y ont recours. On ne peut pas être
responsable du comportement des autres individus en permanence,
ce serait un fardeau trop lourd à porter et impossible à gérer.
La responsabilité incombe à celui qui fait l’acte, sans omettre,
bien sûr, les raisons extérieures qui ont poussé celui-ci à
faire l’acte (évitons d’internaliser ces problématiques aux seuls
individus, nous verrons que c’est plus complexe que
cela).

&#9733; *On parlera de militantisme interpersonnel, c’est-à-dire entre personnes ne représentant pas une autorité quelconque*. Par exemple, untel corrige unetelle sur un de
ses mots sur Internet/lors d’un repas de
famille/lorsqu’elle se confie/lorsqu’elle présente une
création/etc.&nbsp;; untel critique untel car il a osé parler
de fromages sans mettre de trigger warning[^13], etc.
J’exclus de cette catégorie les rapports de pouvoirs entre
personnes, par exemple une négociation entre un syndicat
et un DRH autour d’un conflit, le débat public entre un
individu fasciste et une personne au combat antifasciste, etc.
Les deux derniers cas désignent des
porte-paroles d’un groupe et la situation n’a
rien du rapport interpersonnel habituel, il est hautement
stratégique et préparé en amont pour les deux parties en
confrontation.

&#9733; On ne parlera pas du militantisme d’extrême-droite, fasciste ou
lié à des poussées de haines sur une personne ciblée (par exemple les
raids de harcèlement). Parce que -- et c’est terrible à dire -- il est
cohérent avec l’autoritarisme de droite composé
d’une valorisation de l’*agressivité autoritaire*, de la
*soumission autoritaire* et du *conventionnalisme*[^14]. Si
c’est cohérent au vu de leur combat, alors ce n’est pas
déconnant. Ils sont convaincus du bienfait
d’attaquer et de soumettre une personne, donc il est logique
de s’attendre à ce qu’ils attaquent une personne, qu’ils
l’injonctivent, la poussent à rentrer dans des
normes conventionnelles ou à fuir la sphère publique.
Cependant, si cela vous intéresse&nbsp;:
- voici un excellent article qui montre leur méthode militante&nbsp;: *Un militant repenti balance les secrets de l’ultra-droite*, ([*Midi Libre*, 2012](https://www.midilibre.fr/2012/10/08/un-militant-repenti-balance-les-secrets-de-l-ultra-droite,574771.php))&nbsp;;
- On a parlé d’autoritarisme aussi dans [un dossier](http://www.hacking-social.com/2017/01/16/f1-espece-de-facho-etudes-sur-la-personnalite-autoritaire/)&nbsp;;
- et [cet article](https://www.hacking-social.com/2019/09/02/mcq-le-potentiel-fasciste-lautoritaire-et-le-dominateur/), le résumé des recherches qui ont suivi&nbsp;;
- Et dans [cette vidéo](https://skeptikon.fr/videos/watch/3308307a-fbff-4b6d-9740-1a5d3f30a0be) qui est le début d’une série.

<figure>
    <img src="Pictures/lesautoritaires.png"
         alt="Les autoritaires, partie 1">
    <figcaption>Les autoritaires, partie 1 (sur <a href="https://skeptikon.fr/videos/watch/3308307a-fbff-4b6d-9740-1a5d3f30a0be">skeptikon.fr</a>, sur <a href="https://www.youtube.com/watch?v=2__Dd_KXuuU&t=1275s">Youtube</a>, sur <a href="https://vimeo.com/496438064">Vimeo</a>)</figcaption>
</figure>



&#9733;  Pour cet article, il serait incohérent que je
m’autocensure par peur de la pureté militante, ainsi y aura-t-il des
néologismes, des anglicismes, des points médians et parfois une absence
de points médians, des liens YouTube ou pointant vers
des réseaux sociaux, structures non-libres. Cependant, je
justifierai parfois certains de mes choix «&nbsp;impurs&nbsp;»
en note de bas de page quand j’estime que cela est nécessaire. J’y
mettrai aussi les références. Désolée pour l’énervement
futur que cela pourrait vous causer.


<!-------------- Notes INTRO ---------------->



[^1]: J’ai gardé le terme anglais parce qu’on croise rarement des
    personnes se disant «&nbsp;fouineuse à chapeau gris&nbsp;» comme le
    recommanderait l’Académie française. Les hackers *grey hat* désignent les hackers faisaient des actions illégales pour des buts prosociaux/activistes, à la différence des *black hat* qui vont faire des actions illégales pour leur seul profit personnel par exemple.

[^2]: GAFAM = Google, Apple, Facebook, Amazon et Microsoft.

[^3]: TW = Trigger Warning = avertissements.

[^4]: Endogroupe = groupe d’appartenance. On peut en choisir certains
    (supporter telle équipe et pas telle autre, être dans une
    communauté de passionnés pour telle discipline, etc.) et d’autres
    sont dus au pur hasard comme la nationalité à la naissance, le
    genre, la génération dans laquelle on appartient, la couleur de
    peau, etc.

[^5]: Ça, c’est volontairement hors de mes buts, non pas que
    je sois défaitiste, mais parce que je ne vois pas l’intérêt de
    convaincre qui que ce soit&nbsp;: imaginons qu’untel
    serait par exemple convaincu que la justice
    réparatrice (un thème que j’aborde) c’est bien. Ok,
    super. Et ensuite&nbsp;? Qu’est-ce que ça change&nbsp;? Strictement rien. Au
    fond, je ne veux rien d’autre que ce que
    souhaiterait un blogueur tech&nbsp;: il partage un logiciel
    qu’il trouve cool et il est content si ça sert à d’autres qui vont
    l’utiliser pour d’autres besoins. Il peut même être super
    content de découvrir que ce logiciel qu’il a partagé a été hacké
    pour en faire un autre truc plus performant qui sert d’autres buts,
    parce qu’au fond, c’est la tech et son développement qui le font
    kiffer. Et c’est pas grave si les autres n’aiment pas, ne
    testent pas. Au fond, lui comme moi, on veut juste
    partager, c’est tout.

[^6]: Je n’ai ni envie de donner le lien ni même de référence au sujet
    de cet écrit&nbsp;; si le sujet vous intéresse intrinsèquement, vous le
    trouverez facilement.

[^7]: Deci et coll. (1999)&nbsp;; Deci et Ryan (2017)&nbsp;; Csiszentmihalyi (2014,
    2015)&nbsp;; Della Fave, Massimini, Bassi (2011)&nbsp;; Gueguen C. (2014,
    2018) références non exhaustives.

[^8]: Je parle de mes articles, je ne critique pas les débunkeurs en
    général. Il y a certains débunkeurs sur PeerTubeYouTube qui ont du
    talent, je ne doute pas qu’ils arrivent eux à avoir un écho positif.

[^9]: Gull a d’ailleurs constaté que cette surenchère à préciser
    toujours plus de détails en vidéo, sous la demande parfois pesante
    de certains septiques, contribuaient à dissoudre le discours
    principal, à ennuyer de nombreux *viewers* se plaignant
    désormais d’un contenu «&nbsp;trop académique&nbsp;».

[^10]: J’emploie le mot «&nbsp;dysfonctionnel&nbsp;» pour qualifier les
    comportements militants tels que l’injonction, l’attaque, etc., car
    la fonction de la militance est, entre autres, de combattre un
    problème et non d’être perçue comme un problème par ceux que les
    militants peuvent chercher à convaincre. En cela, ça
    dysfonctionne, là où un militantisme «&nbsp;fonctionnel&nbsp;» opterait pour des
    comportements et actions qui résolvent un problème, s’attaquerait à
    la destructivité et non aux potentielles forces de construction.

[^11]: Attention ces dernières phrases sont fortement imprégnées de mes
    biais d’internalité (ignorer les causes extérieures, se
    responsabiliser pour des problèmes extérieurs), je ne conseille pas
    du tout de reproduire la même dynamique que la mienne, potentiellement
    sapante. Si je les ai écrites, c’est parce que c’est ce que je
    ressens et que ce serait mentir que d’en enlever les biais
    d’internalité qui ont structuré ces sentiments.

[^12]: En psycho- sociale, notamment dans le champ de la théorie de
    l’autodétermination, on emploie le terme d’environnement distal pour
    décrire des environnements qui sont «&nbsp;distants&nbsp;» des individus (mais
    pas moins influents), tel que les champs politique, économique,
    culturel&nbsp;; il se distingue de l’environnement social proximal
    (famille, travail, et tout environnement social qui fait le
    quotidien la personne).

[^13]: Exemple tiré d’un témoignage réel, disponible sur [neonmag.fr](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html).

[^14]: Ce sont les caractéristiques de l’Autoritarisme de droite (RWA)
    telles que définies par la recherche en psychologie sociale et
    politique de ces dernières décennies, notamment par Altemeyer. Si
    une personne se disant de gauche a pour attitude principale
    l’agressivité autoritaire, la soumission et le conventionnalisme,
    alors il serait plus cohérent pour lui de se revendiquer d’un
    autoritarisme de droite, qui serait plus raccord avec ses attentes et
    valeurs. Plus d’infos sur [hacking-social.com](https://www.hacking-social.com/2019/09/02/mcq-le-potentiel-fasciste-lautoritaire-et-le-dominateur/).




<!-------------- CHAP 1 Le jeu Militant ---------------->


## Le jeu militant

Pour comprendre les différents problèmes, prenons la
métaphore du jeu (game). Par «&nbsp;game[^15]» j’entends la structure
du jeu militant, c’est-à-dire ses buts, ses règles implicites et
explicites qui définissent les possibilités ou les limites d’action&nbsp;;
cette structure peut être investie par la motivation
singulière du militant, son «&nbsp;play&nbsp;», c’est-à-dire sa façon de jouer
le jeu ou de le subvertir, et ce dans une forme de
légaliberté[^16] (une liberté nouvelle qui s’exerce *dans et
par* les règles singulières du jeu). Le jeu militant est donc une
création de mouvements singuliers qui auparavant
n’existaient pas dans la société.

Et dans ce jeu que je qualifierais de stratégie-gestion, les
militants peuvent rencontrer plusieurs types d’acteurs&nbsp;: les alliés,
les adversaires et les spectateurs.


### Les alliés/soutiens

Les alliés peuvent partager avec les militants des
mêmes buts, des mêmes valeurs liées à ces buts, ou une
même valorisation/dévalorisation de certains comportements. Il y
a une affinité commune quelque part, soit dans le groupe lui-même, soit
à l’extérieur. Ce soutien peut être incarné par une personne comme
par une structure (ou les deux à la fois si la personne
représente une structure), ou encore par un autre «&nbsp;game&nbsp;»
militant. Par exemple, durant l’occupation, les
résistants qui s’attaquaient directement aux structures de l’ennemi
pouvaient coopérer avec les sauveteurs[^17]: s’ils
trouvaient des enfants juifs en danger, ils contactaient des personnes
dont l’activité était de les cacher, ils étaient donc alliés contre la
destructivité nazie. Leurs actions étaient radicalement différentes,
parfois même leurs valeurs n’étaient pas du tout les mêmes, mais ils
visaient tous deux à entraver l’activité de l’adversaire (l’un en
détruisant les moyens de destructivité, l’autre en empêchant de détruire
davantage de personnes).

Les alliés le sont souvent via un but partagé, et cela n’a rien à
voir ni avec des caractéristiques personnelles communes[^18]
(caractère, physique, groupe d’appartenance, croyances,
convictions...) ni avec des interactions positives
personnelles (telles que l’amitié par exemple). On peut être
militant allié de personnes avec qui on ne s’entend pas du tout, comme
être adversaire politique de son meilleur ami, sans que cela se
passe mal au quotidien.

L’allié n’est pas non plus identifiable comme tel. Dans une
manif’ plutôt orientée thème du numérique, je me
rappelle avoir vu un couple de personnes âgées observant la foule d’un
regard sévère se mettre à hurler de façon très autoritaire à la foule de
jeunes&nbsp;: «&nbsp;C’est formidable ce que vous faites&nbsp;!!! Bravo
continuez&nbsp;!!!&nbsp;». Jamais je n’aurais pu imaginer qu’ils soient des
soutiens tant leur comportement semblait exprimer l’inverse, tant on
aurait pu préjuger autre chose d’eux (par exemple,
qu’ils ne s’intéressent pas aux questions numériques
du fait de leur grand âge).

### Les adversaires/l’adversité

Par rapport aux militants, les adversaires ne suivent pas les
mêmes buts, n’ont pas les mêmes valeurs liées à ces buts, ni
la même valorisation/dévalorisation de certains comportements.
L’adversaire ne désigne pas nécessairement un individu, il
peut être une structure (game) considérée par les
militants/engagés comme causant de la souffrance ou des
difficultés&nbsp;: par exemple, un système de *benchmark* au
travail à la Caisse d’Épargne était perçu par ses
opposants comme un adversaire à cause de la souffrance qu’il
engendrait[^19], et cet adversaire était par extension
niché dans les individus qui entretenaient, maintenaient
et défendaient ce système problématique. Dans ce
cas, un directeur avait joué le jeu du *benchmark* avec
zèle, harcelant quotidiennement les employés mis en compétition
constante et il y gagnait à pratiquer ce
*benchmark-game*, car il remportait d’énormes primes, sans
compter la «&nbsp;prestance&nbsp;» du statut. Mais par la suite,
il a subi le même type d’harcèlement, en a fait un
infarctus, ce qui lui a fait prendre conscience de l’horreur
à laquelle il avait participé. Dès lors, [il a lancé l’alerte](https://www.dailymotion.com/video/x2ymwlh).


<figure>
    <img src="Pictures/patronspression.png"
         alt="Reportage&nbsp;: les patrons mettent-ils trop la pression&nbsp;?">
    <figcaption>Reportage&nbsp;: les patrons mettent-ils trop la pression&nbsp;? (<a href="https://www.dailymotion.com/video/x2ymwlh">France 2, Envoyé Spécial</a>, 28 février 2013.</figcaption>
</figure>




L’adversaire, ce n’était pas lui en tant que personne,
l’adversaire se nichait dans sa soumission, dans son
zèle et dans son allégeance à un système déshumanisant&nbsp;:
en tant que personne, il a changé suite à son infarctus,
il a pu lever son aveuglement, prendre conscience de sa soumission
et de ce qu’il avait fait d’horrible.

Tout comme l’allié n’est pas défini par ses caractéristiques
personnelles, l’adversaire ne l’est pas non plus à cause de
caractéristiques personnelles, mais parce qu’il joue à un jeu adverse
dans lequel il est aliéné. Cette aliénation peut être dite
culturelle et s’alimenter par d’apparents gros avantages, comme
un statut très supériorisé par rapport à autrui, de très
fortes primes, des faveurs énormes dans la société… Tout cela
contribue à rendre l’individu aveugle à certaines
souffrances, parce que les regarder en face comporte le risque
d’une prise de conscience qui entraînerait à son tour de la
culpabilité, puis s’ensuivrait l’impératif compliqué de
devoir tout changer. Ces énormes avantages et autres
supériorisations sont aussi un contrôle sur l’individu&nbsp;: c’est la
carotte qui fait avancer l’âne, et prendre conscience de cet état de
pion peut être très pénible surtout quand on s’est senti si
supériorisé pendant longtemps. L’individu expliquera le
fait qu’il ait plus de carottes qu’autrui comme de l’ordre du mérite
personnel, alors que dans de nombreux cas, soit c’est un pur hasard,
soit c’est juste une façon d’entretenir sa pleine soumission.

L’individu soumis à l’adversaire peut donc s’embrumer dans
le déni, refuser de voir les conséquences des actes qu’on lui
ordonne (ou qu’il reproduit par obéissance à des injonctions
implicites de la société), parce que la prise de conscience serait une
claque majeure pour son ego et aurait un fort retentissement
dans l’organisation de sa vie. En d’autres termes, il
persiste dans cette aliénation car abandonner ses attitudes
et comportements aurait un coût bien trop élevé, et lui donnerait le
sentiment de s’être investi et engagé pour rien.

Cependant cette aliénation culturelle peut être levée. Un adversaire
qui a causé de la souffrance un temps peut tenter d’apporter une
réparation, voire aider les militants sans pour autant voler leur parole
ou les dominer encore une fois&nbsp;; dans le reportage sur le
*benchmark*, le harceleur a lancé l’alerte via son témoignage, a
pu donner une information précieuse sur le mode de fonctionnement
destructif de ce système. On voit aussi dans cette affaire que les
syndicats se sont attaqués à la structure, à l’entreprise pour régler le
problème et non à l’individu en particulier (manager,
directeur…) qui serait seul pointé du doigt et dont la simple
démission/licenciement suffirait à tout régler, car ils savaient
que l’ennemi principal était le benchmark. Et, suite à l’action
syndicale, la justice a interdit à la Caisse d’Épargne
d’employer ce mode de management.[^20]

### D’autres caractéristiques de l’adversité

&#9632; Selon les combats militants, l’adversité peut être
difficile à montrer aux autres, et il est donc difficile de faire
comprendre qu’il y a bien un problème. Par exemple, la
surveillance généralisée est rendue le plus invisible possible pour se
perpétuer, voire est dissimulée sous des artifices
*fun* et attractifs[^21]. D’où le fait que les
militants ont souvent besoin de faire un travail d’information
préalable.

&#9632; L’adversité peut être très complexe à montrer, car elle est
souvent systémique. Quand on veut expliquer un problème
(voire simplement le nommer), on arrive très rapidement,
à des choses totalement abstraites, des concepts qui sont
difficiles à représenter. Par exemple, la liberté, l’égalité et la
fraternité sont des valeurs abstraites, et chacun y plaque sa propre
représentation (par exemple pour Sade, la vraie liberté c’était
aussi d’avoir le droit de tuer et de violer sans être
réprimé[^22]) qui s’oppose à d’autres représentations (la
liberté entendue comme une responsabilité, qui inclut donc des limites,
notamment celle de ne pas porter atteinte à autrui puisque ce serait
trahir sa propre responsabilité).

Dès lors, on peut tomber dans un puits sans fond d’explications
très théoriques qui pourra rendre le combat militant élitiste, car il
demande un certain niveau culturel ainsi qu’une veille quasi permanente
des débats en cours afin d’être pleinement saisi. Ce faisant, le
discours militant pourra d’une part paraître difficile d’accès pour le
quidam et, d’autre part, ce haut degré de maîtrise théorique pourra
décourager plus d’un à rejoindre cette militance, pourra même être vécu
par d’autres comme une forme d’écrasement social et culturel, voire
comme une forme de violence symbolique (quand il s’agit de classes
sociales peu ou pas du tout favorisées). De plus, les militants
eux-mêmes pourront perdre pied avec le terrain, se retrouver dans une
tour d’ivoire sans prendre conscience de leur hauteur parfois
condescendante. Au lieu de construire, d’agir, de se confronter aux
adversaires, ils pourront perdre une énergie précieuse à force de
débattre plus que de raison en interne sur des éléments secondaires,
voire de se confondre dans des échanges interminables portant sur le
sexe des anges.

&#9632; L’adversité peut être très complexe à montrer, car
hautement technique. On en revient au point précédent&nbsp;:
si pour être un «&nbsp;bon&nbsp;» militant et comprendre le problème on
doit maîtriser X langages de programmation, savoir bidouiller en
profondeur tel OS, alors il n’y aura pas grand monde qui pourra voir
qu’il y a effectivement un problème, ni même pour s’y
confronter, tant ça demande de compétences et de
savoirs. Pour contrer ça, les mouvements peuvent se centrer sur
des problèmes plus accessibles aux personnes ou faciliter techniquement
la militance.

&#9632; L’adversaire, c’est souvent la soumission à l’autorité ou une
allégeance d’un individu qui n’a pas conscience de son statut de pion.
Au fond, les «&nbsp;vrais&nbsp;» ennemis qui auraient par exemple un plaisir
sadique à voir la souffrance sont plutôt rares. On pourrait donc
dans un mouvement militant s’interroger sur cet aspect, sur
ce pourquoi il y a une soumission à telle
attitude/comportement destructif et non à telle autre
attitude/comportement constructif&nbsp;; cela permettrait d’éviter de
s’attaquer aux personnes elles-mêmes (car ça ne réglera pas
le problème sur le long terme, on en discutera après), permettra
plutôt de transformer leur rapport à l’adversité
qu’ielles entretiennent, de les libérer de cette
allégeance et de cette soumission, ce qui peut, à terme, leur faire
éprouver une gratitude pour la cause militante.

### Le spectateur (ou témoin ou tiers[^23])

C’est l’individu qui ne fait rien d’autre que poursuivre son
train-train quotidien face à une situation qui
éveillerait en principe l’action militante, ou dans laquelle les
adversaires travailleraient à détruire. Il continue sa routine
habituelle et ne fait rien pour ou contre les éléments qui se
jouent dans la situation, qu’ils soient constructifs ou destructifs.
La recherche a montré que plus on est dans une situation où il y a
beaucoup de monde, moins on se sent acteur et moins on sera
par exemple enclin à aider autrui si nécessaire, c’est
[l’effet spectateur/témoin](https://skeptikon.fr/videos/watch/559631c3-70bd-40ca-b26c-83d1bafc404a)[^23b].


<figure>
    <img src="Pictures/effettemoin.jpg"
         alt="Effet du témoin. [XP] Peut-on compter sur un groupe pour nous porter secours&nbsp;?">
    <figcaption>Effet du témoin. [XP] Peut-on compter sur un groupe pour nous porter secours&nbsp;? Sur <a href="https://skeptikon.fr/videos/watch/559631c3-70bd-40ca-b26c-83d1bafc404a">Peertube</a>, sur <a href="https://www.youtube.com/watch?v=fJBPIQ3wwA0">Youtube</a>. Vous pouvez consulter aussi l’expérience elle-même (Darley et Latané, 1968)&nbsp;; plus d’infos aussi sur <a href="https://fr.wikipedia.org/wiki/Effet_du_t%C3%A9moin">Wikipedia</a>.</figcaption>
</figure>


Être spectateur, ce n’est donc pas être insensible aux
événements visiblement destructifs (harcèlement, agression), mais
davantage ne pas savoir s’il faut intervenir ou ne pas savoir comment,
ou encore avoir peur des conséquences si on sort de sa
routine.

En situation militante, cela se complique davantage
parce que l’individu lambda peut ne pas percevoir du tout le
problème. Par exemple, un militant antipub repérera très
vite en quoi tel panneau publicitaire sur la voie publique pose
problème et pourrait vous donner mille arguments contre l’existence
de cette pub. À l’inverse, le spectateur suit sa routine
habituelle, est plongé dans ses pensées, c’est comme si les
pubs n’existaient pas (et c’est exactement l’effet souhaité par les
publicitaires, la pub s’adresse d’abord à
nos processus inconscients[^24]). Le spectateur peut être
convaincu par les arguments des antipubs, peut-être qu’un temps il
parviendra à analyser les panneaux pubs pour ce qu’ils sont,
s’en énervera comme eux. Sa vision sera celle d’un antipub pendant un
temps. Puis le quotidien reviendra lui effacer cette perspective, parce
que nos processus attentionnels sont très limités et qu’on ne peut pas
tout traiter consciemment en permanence. Le militant antipub aux
stratégies constructives le sait, et le soulage en déboulonnant les
pubs, en les hackant, en les subvertissant, il le libère
ainsi de cette pollution mentale et attentionnelle le
temps d’une action. Il y a là à la fois une
confrontation avec l’adversaire ainsi qu’un soutien
au spectateur car son espace visuel est
libéré. Mais tout cela tombe à l’eau si la pub est remplacée
par une injonction ou une attaque faite au spectateur.

Autrement dit, contrairement aux expériences de l’effet spectateur,
la situation à potentiel militant ou dans laquelle un adversaire cause
un problème a ceci de particulier que&nbsp;:

&#9632; Le spectateur peut ne pas être informé ou ne pas pouvoir voir
les problèmes de la situation. Et cela ne vient nullement du fait qu’il
est «&nbsp;con&nbsp;», ses raisons peuvent être tout à fait légitimes,
normales, banales. Je doute par exemple que les clients
de la Caisse d’Épargne aient pu savoir que dans leur
banque il y avait un tel harcèlement systématisé. Pour
perdurer, la destructivité a besoin de se cacher, de
se travestir, voire se parer d’arguments ou de
promesses qui peuvent être très séduisants.

&#9632; Le spectateur ne peut pas conscientiser en permanence tous les
problèmes. Nos systèmes attentionnels sont vraiment très limités, et
porter conscience sur une chose va demander de supprimer de l’attention
sur une autre. Si on conscientise tout en même temps dans les détails,
on ne peut qu’agir contre un seul des problèmes ou sur une seule
chose à faire. Pire, l’hyperconscientisation peut être tellement
massive qu’on en vient à ne plus rien faire tellement ça nous nous
plonge dans un surmenage mental, ce qui est assez ironique. Tout
comme on ne peut pas porter une centaine de kilos toute la journée en
permanence, nos ressources mentales ne peuvent supporter mille
attentions continuellement avec le même zèle.

&#9632;  Le spectateur peut ne pas avoir les capacités d’agir
ou se sentir impuissant quant au problème
conscientisé. Ça peut être le cas lorsqu’on évoque des problèmes d’ordre
géopolitique ou qui nous paraissent bien éloignés. Soit le
spectateur ressent de la détresse et cela peut le plomber parce
qu’il ne peut rien faire, soit il va éviter le sujet parce qu’il sent
déjà qu’il pourrait être plombé par un sentiment
d’impuissance.

&#9632; Le spectateur peut savoir, être conscient, mais sa situation ne
lui permet pas de s’ajuster (d’ailleurs cela vaut aussi pour le
militant)&nbsp;: par exemple, il peut savoir que les produits bio locaux
sont bien meilleurs pour l’écologie, mais sa situation de pauvreté
l’oblige à devoir rationaliser financièrement le budget
nourriture (et donc acheter prioritairement ce qui est le moins
cher) parce que c’est ça ou ne pas pouvoir payer le loyer, ou
devoir sacrifier le chauffage l’hiver, etc.

&#9632; Le spectateur peut savoir que ce problème gêne certains, mais
il ne l’estime pas pour autant comme un problème
prioritaire. Par exemple, il peut s’en foutre de respecter ou non
l’orthographe et les recommandations de l’Académie (contrairement à un
grammar nazi), parce que ce qui compte pour lui c’est de réussir
à communiquer et à se faire comprendre, et ça marche y
compris lorsqu’il y a des fautes.

&#9632; Être un modèle de vertu ne va pas forcément avoir une
influence positive qui sera imitée. Je précise cela
parce que dans les expériences sur l’effet spectateur, si une
personne va aider, alors tous les autres spectateurs vont sortir de leur
passivité et aider à leur tour comme pour l’imiter. Mais
s’il y a cet effet mimétique c’est parce que la
souffrance est visible, que les spectateurs sentent qu’il y a un
mal-être et ont besoin d’un exemple pour savoir que faire ou
tout simplement s’autoriser à agir. Or, dans les situations à
militance, la souffrance/les problèmes peuvent être en premier lieu
totalement invisibles aux yeux du spectateur, qui ne
saura même pas pourquoi vous faites cela. Je me rappelle
un·e militant·e pour le zéro déchet qui rapportait son agacement
car iel s’était fait·e envoyer bouler après
avoir demandé à un traiteur d’utiliser sa propre
boîte pour contenir les aliments plutôt que de suivre la
procédure habituelle d’emballage du
magasin[^25]. Cette attitude n’est pas perçue comme «&nbsp;à
copier&nbsp;» ni par l’employé (qui peut avoir perçu cela davantage comme une
tentative de contrôle injuste de son comportement professionnel)
ni par les autres clients dans la file (qui voient juste un
autre client qui gaspille trop de temps et les fait
attendre).

Cependant, face à l’adversaire, être un modèle vertueux peut
effectivement être utile pour apparaître cohérent dans son
combat&nbsp;: si on négocie avec les pouvoirs publics pour l’arrêt d’une
politique polluante, mieux vaut ne pas arriver en SUV au premier
rendez-vous, c’est un coup à se décrédibiliser totalement et à
ne pas être écouté.

Certaines situations très particulières peuvent aussi
renforcer l’importance d’être un modèle, notamment celles où
n’importe qui peut faire la connexion entre ce comportement vertueux et
une utilité sociale directe. Par exemple, durant la Première
Guerre Mondiale, André Trocmé[^26], alors enfant,
rencontre un soldat allemand qui lui propose à manger. Il refuse
parce qu’on ne mange pas avec l’adversaire et Trocmé est déjà très
patriote à l’époque (un jeu patriote rejetant toute
interaction avec l’ennemi). Le soldat lui explique en toute
sympathie qu’il n’est en rien un ennemi, car il a refusé de
porter des armes, de tuer ou de faire du mal à qui que ce soit, il ne
s’occupe que des communications. Trocmé est fasciné parce qu’il ne
savait pas cela possible, ils continuent de parler, mangent le
pain ensemble. Ce modèle restera gravé à jamais dans sa mémoire, et
Trocmé participera plus tard avec sa femme et tout le village de
Chambon-sur-Lignon à résister, à cacher et sauver entre 3500 et
5000 Juifs de la mort[^27]; son patriotisme a évolué, et
l’adversaire n’est plus vu dans l’individu, mais dans la destructivité
auquel il peut être allégeant. À noter que la résistance de
Chambon-sur-Lignon a été très particulière, car elle s’est faite sans
chefs ni organisation formelle, simplement par une cohésion tacite.

Être un modèle même modeste dans ses actions, mais dont la
prosociabilité de l’engagement est visible, indéniable pour les enfants,
peut inspirer ceux-ci, pourra peut-être leur donner le
courage de savoir quoi faire face une adversité qu’on n’aurait pas
pu imaginer.

En fait, pour résumer ce débat «&nbsp;faut-il être un
modèle de vertu/de pureté devant les spectateurs?&nbsp;».
Je dirais que cela n’a un impact positif que si les bénéfices
prosociaux qu’apporte le comportement sont directement
perceptibles par autrui (via moins de souffrances, moins de
menaces, plus de relations sociales positives, plus de bonheur,
etc.).

### Que va faire le militant avec ce trio&nbsp;?

En toute logique, on peut imaginer que le militant va donc tenter de
renforcer la cohésion et la diversité des alliés, notamment en accordant
de l’attention positive au spectateur (lui donner des informations
utiles, lui faire vivre des prises de conscience qui l’aident aussi,
prendre le temps de chercher à le comprendre pour mieux
répondre à ses besoins, le libérer, etc.). Il va combattre
l’adversité tant au niveau distal, mécanique, systémique que dans la
recherche d’une prise de conscience chez l’adversaire (ce
dernier comprendrait alors que ce sont les
mécaniques adverses en lui qui l’empoisonnent, et
pourrait donc décider de les abandonner, les transformer,
etc.).

On peut déduire de cette logique trois pans d’actions
(potentiellement cumulables/menés de concert) dans le jeu militant, à
savoir&nbsp;: la confrontation, la construction et l’information.

### La confrontation

Le militant se confronte à l’adversaire. Cela peut se faire via des
négociations, du lobbying, des manifestations tout comme du sabotage, du
hacking, via un combat direct. Dans les mouvements non
violents[^28] du passé, cela a pu se manifester par
l’appropriation de droits qui étaient pourtant interdits à
certaines personnes de façon injuste&nbsp;: par exemple, Rosa Parks s’est
assise dans le bus à une place qui lui était interdite par sa couleur
de peau&nbsp;; les militants afro-américains pour les droits civiques sont
allés dans les restaurants, épiceries qui leur étaient interdits,
etc. La réponse/riposte de l’adversaire a été parfois
épouvantablement violente, frappant les militants, les
emprisonnant, les tuant, mais ils ont tenu bon, ont continué à se
confronter, en vivant, en étant là, dignes et laissant l’adversaire
révéler son vrai visage de haine. La confrontation peut avoir des
facettes très variées, allant d’une furtivité totale de ses
membres (par exemple le sabotage, le hacking) jusqu’à une forte
visibilité publique, elle peut accepter des actions illégales
comme prendre appui sur la loi. Il y a vraiment énormément de
possibilités de se confronter à l’adversaire, toutes très
différentes dans leurs stratégies et buts.

### La construction

Le militant construit un objet, un environnement social, des façons
de faire/de s’organiser, etc. Ce qui est à l’opposé ou radicalement
différent de ce que fait l’adversaire, et se pose comme
concurrence au niveau du bien-être (ce qui est construit cause moins
de souffrances, l’humain y trouve plus de bien-être, de bonheur, etc.).
Par exemple, j’ai découvert parmi les hackers des façons de
s’organiser do-ocratique «&nbsp;le pouvoir à celui qui fait&nbsp;», très
différentes de la façon dont on se structure
traditionnellement dans le monde du travail. Là, n’importe qui
pouvait monter une opération avec un pouvoir de décision
propre à son initiative&nbsp;; ou si untel était connu pour
avoir réussi tel aspect technique de l’opération, il était convié aux
opérations impliquant ces mêmes techniques. Par contre,
on pouvait l’envoyer balader s’il venait faire son chef dans une
opération pour laquelle il n’avait rien fait,
qu’importent ses faits d’armes dans tel autre domaine.
Il y avait là des valeurs anti-autoritaires,
anti-hiérarchiques qui étaient vécues concrètement dans le quotidien et
qui pouvaient être perçues dans la façon d’organiser les actions.

Le groupe militant construit et vit dans des modes
d’organisation qui peuvent aussi constituer une confrontation avec
l’adversaire, dans le sens où cette construction révèle à quel point ces
modèles adverses peuvent être périmés ou malsains,
puisqu’il prouve de fait que l’inverse peut fonctionner mieux,
de façon beaucoup plus humaine et plaisante.

### L’information

C’est la révélation aux spectateurs/futurs alliés du problème, par
exemple le lancement d’alerte, le témoignage, le *leak*…&nbsp;;
cela peut aussi se faire au travers d’ateliers/évènements à des
fins d’éducation populaire, ou à travers toutes sortes
de formes d’enseignement. Dans le milieu hacker (comme dans le milieu du
renseignement), on considère l’information comme le pouvoir&nbsp;: on ne peut
faire certaines choses que si on sait certaines choses. C’est ce qui
fait que chez les hackers la libération de l’information
à tous est considérée comme un empuissantement de la
population et une victoire en soi&nbsp;: c’est par exemple ce qu’a fait
Alexandra Elbakyan avec Sci-hub et qui permet à
n’importe quel chercheur ou personne curieuse d’avoir accès à
quasiment toutes les recherches scientifiques, sans avoir à se
ruiner.

À l’inverse, l’adversaire a souvent des pratiques de
rétention ou de déformation d’infos qui lui permettent
d’avoir un contrôle sur les personnes et les situations.

Manipuler l’information (en donner certaines et pas d’autres, mentir
sur les faits, informer certains et pas d’autres, inventer de fausses
informations, etc.) peut aussi permettre de dominer une personne et la
contrôler d’une façon ou d’une autre, que ce soit pour en tirer plus
d’exploitation de force de travail ou obtenir une allégeance&nbsp;: si je
fais croire à telle information fausse, alors j’obtiens de la légitimité
auprès de ceux qui vont y croire, voire une certaine autorité, donc je
peux mieux les exploiter (c’est ce que peuvent
faire les acteurs d’un mouvement sectaire, par exemple la
scientologie).




<figure>
    <img src="Pictures/scientologie.png"
         alt="En scientologie, chaque niveau pour être « clair » coûte plusieurs milliers de dollars, et consiste en des révélations...">
    <figcaption>En scientologie, chaque niveau pour être «&nbsp;clair&nbsp;» coûte plusieurs milliers de dollars, et consiste en des révélations. Les militants anti-sciento ont donc tout simplement révélé celles-ci&nbsp;: à OT3, pour 158 000 dollars, les adeptes apprennent que Xenu, un méchant extraterrestre a envoyé une partie de sa population dans nos volcans, et comme ça ne suffisait pas, il a aussi balancé des bombes H. Mais il restait des bouts d’extraterrestres, les bodythétans. Le clan de Xenu leur ont implanté des images de la future société terrienne&nbsp;: toutes les religions sont pures illusions inventées par la troupe de Xenu afin de tourmenter à jamais ces bodythétans. Et ces bouts d’extraterrestes plein d’illusions se sont mis en grappe en nous, heureusement la scientologie a la solution pour les virer (mais il va falloir payer). Pour 316 000 dollars, vous apprendrez aussi que Ron Hubbard était la réincarnation de Bouddha, que Lucifer était le représentant de la confédération galactique et que Jésus était homosexuel (la scientologie est homophobe). En principe, après avoir lu ça sans pour autant être scientologue, vous devriez être en combustion spontanée. Plus d’infos sur <a href="https://wikileaks.org/wiki/Church_of_Scientology_collected_Operating_Thetan_documents">wikileaks<a>, <a href="https://whyweprotest.net/threads/leak-ot8-hco-pl-7-4-70rd-hco-b-19-3-71.101594/">whyweprotest</a>, et <a href="https://www.hacking-social.com/2015/04/27/gamification-partie-3-un-exemple-dune-gamification-extreme-et-dangereuse-la-scientologie/">hacking-social</a>. (Image provenant du magazine <em>Advance</em> de la scientologie, dans les années 80).</figcaption>
</figure>


Le travail militant sur la libération de l’information n’est
donc pas juste une façon d’informer/d’éduquer les spectateurs ou
de renforcer la cohésion entre alliés. Il s’agit aussi
de hacker les mécaniques de l’adversaire, d’entraver les façons
dont il tire du pouvoir de domination. Ainsi, même le témoignage le plus
simple révélant comment se déroule dans le détail une journée de travail
dans telle entreprise (indépendamment d’une démarche
militante) donne potentiellement du pouvoir d’agir à celui qui le
lira, parce qu’il aura plus d’éléments pour décider de son comportement
face à cette entreprise. Il y a très longtemps je me souviens d’un
employé bossant à Quick qui avait été licencié et menacé de
procès pour avoir simplement raconté ses journées sur
Twitter[^29], tweets qui évoquaient les
manquements à l’hygiène et qui le choquaient à raison (il y avait
déjà eu un ado de 14 ans mort par intoxication[^30]
après avoir mangé dans ce même restaurant)&nbsp;: c’est
extrêmement révélateur à mon sens des mécaniques de domination
car les entreprises ont besoin de garder le contrôle de
l’information pour perpétuer leur contrôle, maintenir le statu quo,
préserver leur levier d’exploitation, de profit.

Cependant, précisons que nous avons la chance de vivre dans un monde où
l’humanité n’a jamais eu autant accès à l’information, mais que la
contrepartie est que nous sommes constamment bombardés de nouvelles
infos, qu’il y a une très forte concurrence sur le marché de
l’attention.

Autrement dit, il y a un autre enjeu qui se lie à cette
problématique de l’information&nbsp;: la question
de l’attention et des façons de la capter. L’information
même la plus empuissantante est en concurrence avec des vidéos d’animaux
mignons, de divertissement en général, des informations plus
émotionnelles et donc plus attractives (forte colère, drama,
peurs), et des stratégies puissantes de communicants qui
jouent sur le grand échiquier de l’attention avec brio pour
décider de ce qui occupera les esprits à tel moment (voir ce reportage
absolument sidérant, je vous le conseille vraiment vivement&nbsp;: *Jeu d’influences - les stratèges de la communication*, [partie 1](https://www.dailymotion.com/video/x1u8i9i), [partie 2](https://www.dailymotion.com/video/x1v5icc).


Libérer l’information ne suffit plus, il y a aussi tout
un art à développer pour la rendre accessible à tous, attractive,
afin que son potentiel empuissantement et son utilité
sociale soit mis en valeur.

### Le jeu militant déconnant

Les milieux militants jouent en général sur ces trois registres en
même temps, la construction permettant à la fois de renforcer la
cohésion entre alliés et spectateurs, voire à séduire l’adversaire qui va
alors laisser tomber sa destructivité&nbsp;; la mission d’information visant
souvent le soin des alliés ou spectateurs et la diminution du pouvoir de
domination de l’adversaire&nbsp;; et la confrontation se faisant contre les
mécaniques adversaires et ses systèmes.


Alors pourquoi la militance déconnante fait-elle l’exact inverse, jouant un
jeu aux règles inverses&nbsp;?

Pourquoi se confronte-t-elle aux alliés et spectateurs (mais pas à
l’adversaire)&nbsp;? Comme les grammar nazis qui se confrontent à
l’ensemble des gens faisant des fautes, mais jamais ne se
confrontent de manière radicale à la langue et ses
problématiques linguistiques qui pourtant sont directement en cause
dans nos erreurs (erreurs qui sont d’ailleurs
souvent très logiques).

Pourquoi la militance déconnante ne construit-elle rien et préfère
s’attaquer à ceux qui construisent&nbsp;?

Pourquoi la militance déconnante ne libère-t-elle pas
l’information pour empuissanter, mais préfère l’utiliser comme une
massue pour corriger les gens&nbsp;? Comme ceux qui
militent au nom d’une pseudo zététique via des échanges dont
la dynamique est identique à celle des grammar nazis, reprochant
à tout va les biais d’untel ou ses arguments mal construits, sans voir
que les biais et la «&nbsp;mal-construction&nbsp;» des arguments n’a rien d’une
faute, et en dit beaucoup plus sur les attentes, les
besoins, les motivations des personnes concernées et que c’était
peut-être cela qu’on pourrait peut-être prioritairement
regarder.

C’est cela que j’appelle du militantisme déconnant&nbsp;: en combattant
les alliés et spectateurs, en ne construisant rien d’utile, en
n’informant que pour taper, il produit un dégoût pour son sujet. Qui
veut se renseigner, soutenir, joindre quelque chose qui est perçu comme
menaçant&nbsp;? Plus grave encore, cette déconnance laisse tout le champ à
l’adversaire de s’étendre, laisse les causes premières de la
destructivité persister, invisibilise parfois les autres
militances non déconnantes.

Autrement dit, les mêmes objectifs que ceux d’un saboteur, d’un
ennemi au mouvement.


<!-------------- Notes CHAP 1 ---------------->



[^15]: Employer ce mot anglais plutôt que le français «&nbsp;jeu&nbsp;» est plus
    pratique (ici ou ailleurs) parce qu’en français (et d’ailleurs dans
    d’autres langues) nous n’avons pour seul mot que «&nbsp;jeu/jouer/jouet&nbsp;»
    dont l’orthographe est laborieuse pour séparer deux réalités
    totalement différentes&nbsp;: le game (jeu) ce serait le plateau de
    Monopoly, les règles, les buts&nbsp;; le play (jeu) c’est l’élan du
    joueur, sa motivation, son énergie à saisir ce jeu qui peut être
    très variable, comme jouer de manière très conformiste et dogmatique
    (suivre toutes les règles au pied de la lettre, atteindre le but),
    de manière hacker (on change les règles pour que ce soit plus fun,
    moins injuste pour les enfants, etc.), tricheur (faire semblant de
    suivre les règles pour gagner plus facilement), etc. Les façons de
    play peuvent changer le game (ou pas si on le joue conformiste). Et
    ça vaut aussi dans l’emploi de métaphores comme «&nbsp;peser dans le
    youtubegame&nbsp;», qui se réfère au respect du game de YouTube dans son
    play conformiste (=augmenter son nombre d’abonnés/de vues, être en
    tendance, respecter les règles de copyright et les pressions
    implicites à ne pas parler de sujets qui fâchent les annonceurs,
    etc.), mais le play sur YouTube pourrait être différent (faire une
    vidéo antiyoutube en cherchant à ne pas fâcher les annonceurs, en
    respectant le copyright, etc.).

[^16]: Terme provenant de Colas Duflot qui définit le jeu comme
    l’invention d’une liberté *dans et par* une légalité, et cette
    liberté singulière d’employer ce concept pour des structures qui ne
    sont pas du jeu, mais dans le champ de la recherche des
    game-designers portant sur la ludification/gamification, on peut
    constater que les différences entre une structure «&nbsp;sérieuse&nbsp;» et
    une structure de jeu ne sont pas si énormes, l’une se confondant avec
    l’autre, parfois volontairement, parfois involontairement. Sources
    (non exhaustives)&nbsp;: Colas Duflot, *[Jouer et philosopher](https://hal.archives-ouvertes.fr/hal-01628001)*, PUF, 1997&nbsp;; Eric Zimmerman et Katie Salen, *Rules of play. Game Design Fundamentals*, MIT Press, 2003.

[^17]: Oliner (1988)

[^18]: Sauf dans le cas de groupes fascistes, ethnocentriques,
    autoritaires, qui filtrent selon la couleur de peau, l’origine
    ethnique, des caractéristiques physiques. Sauf également dans des
    groupes dogmatiques où il pourrait y avoir une exigence à être selon
    une norme extrêmement stricte, tout du moins d’un point de vue
    comportemental.

[^19]: Je prends ici la question du *benchmark* qui a été mis en place à
    la Caisse d’Épargne, et qu’on peut voir décrite notamment dans ce
    numéro d’Envoyé spécial «&nbsp;[Les patrons mettent-ils trop la
    pression&nbsp;?](https://www.dailymotion.com/video/x2ymwlh)&nbsp;», datant du 28 février 2013.

[^20]: Libération, «&nbsp;[La Caisse d’Epargne Rhône-Alpes condamnée pour avoir mis ses employés en concurrence](https://www.liberation.fr/societe/2012/09/05/une-banque-lyonnaise-condamnee-pour-avoir-mis-ses-employes-en-concurrence-entre-eux_844175/)&nbsp;», 05/09/2012.

[^21]: En captologie, B. J. Fogg dans «&nbsp;Persuasive Technology&nbsp;: Using
    Computers to Change What We Think and Do&nbsp;» rapporte qu’une
    application ludique pour enfants questionnait ceux-ci de temps en
    temps sur les habitudes personnelles de leurs parents. Il s’agissait
    d’obtenir des informations très personnelles en vue de profit, et
    tout cela de façon la moins détectable possible. Vu que c’était
    intégré autour d’un jeu *fun*, impossible pour l’enfant de détecter
    le problème.

[^22]: Youness Bousenna, «&nbsp;[Viol et meurtre, la République selon Sade](https://philitt.fr/2016/04/24/viol-et-meurtre-la-republique-selon-sade/)&nbsp;», *Philitt*, 24/04/2016.

[^23]: Je garde prioritairement le terme «&nbsp;spectateur&nbsp;» plutôt que
    «&nbsp;témoin&nbsp;» ou «&nbsp;tiers&nbsp;», parce que d’une part c’est celui qui est
    le plus utilisé dans ma chapelle qui est la psychologie, et parce
    que d’autre part je trouve qu’il connote davantage la passivité face
    à un «&nbsp;spectacle&nbsp;» et pointe bien du doigt le problème. Le terme
    «&nbsp;tiers&nbsp;» (qui renvoie aussi au spectateur) est davantage utilisé
    dans l’analyse des génocides, notamment en histoire pour décrire
    l’inaction des États voisins alors qu’un génocide est imminent ou
    en cours mais qu’il y a passivité face à ce phénomène, voire un
    déni. J’utilise moins le mot témoin, parce qu’on aurait tendance à
    imaginer que celui-ci va un jour témoigner, ce qui est déjà ne plus
    être passif. Le témoignage peut potentiellement aider une victime,
    voire lancer l’alerte. Or, si aucune autorité ne le leur demande,
    les spectateurs n’apporteront pas leur témoignage pour autant, voire
    peuvent témoigner de façon incomplète pour cacher la passivité dont
    ils peuvent avoir honte a posteriori.
	
[^23b]: Vous pouvez consulter aussi l’expérience elle-même (Darley et Latané, 1968)&nbsp;;
    plus d’infos aussi [ici](https://fr.wikipedia.org/wiki/Effet_du_t%C3 %A9moin) (Wikipédia).

[^24]: Quand je parle de processus inconscients, je ne parle pas en
    langage psychanalytique, mais en termes neuro. On parle ici de
    processus *cognitifs* inconscients. Notre cerveau traite
    l’information même si on n’en a pas conscience, il enregistre les
    stimuli, les associations positives/négatives. Les décisions que
    l’on prend sont également d’abord prises de façon inconsciente avant
    d’arriver dans notre conscience. Les cours de Stanislas Dehaene
    expliquent ceci fort bien, vous pouvez les trouver [ici](https://www.college-de-france.fr/site/stanislas-dehaene/course-2008-2009.htm). Je sais qu’il est «&nbsp;cancel&nbsp;» par certains notamment parce qu’il a bossé avec le gouvernement, il n’en reste pas moins que ses cours
    sont très bien foutus, très sérieux et accessibles et n’ont rien de
    néolibéral ou de macroniste, c’est de la psychologie cognitive et de
    la neuropsychologie.

[^25]: Je ne retrouve plus la source, ça date, mais c’était peut-être
    issu de Béa Johnson dans son livre *Zéro déchet* à moins que ça
    ne vienne d’un témoignage masculin sur twitter&nbsp;; d’où également mon
    emploi de «&nbsp;iel&nbsp;».

[^26]: Dans Magda et André Trocmé, *Figures de résistance*, textes
    choisis par Pierre Boismorand, Cerf, 2008.

[^27]: Les chiffres sont difficiles à estimer.

[^28]: À ne pas confondre avec la notion de pacifisme&nbsp;: les
    mouvements non-violents peuvent détruire également, saboter,
    «&nbsp;s’attaquer à&nbsp;», ou se défendre légitimement...
    Autrement dit, ils peuvent être des «&nbsp;fouineurs à chapeau gris&nbsp;».
    C’est simplement qu’ils refusent de détruire les personnes. Même
    Gandhi, non-violent, disait à ses militants qu’il était OK
    de tuer l’adversaire si celui-ci s’apprêtait à le tuer.
    Il y avait des ateliers de défense physique (et mentale) chez les
    militants non-violents de Martin Luther King. Contrairement aux
    moqueries que je vois passer sur la toile et ailleurs, les
    non-violents étaient badass, n’avaient rien de
    tenants d’un pacifisme moralisateur et lâche se
    refusant à toute prise de risque, à toute confrontation.

[^29]: Le Courrier Picard, «&nbsp;[Le déluge s’est abattu sur l’équipier Quick après l’épisode Twitter](https://www.courrier-picard.fr/art/region/le-deluge-s-est-abattu-sur-l-equipier-quick-ia195b0n154213)&nbsp;», 08/08/2013.
    et aussi [ici](http://www.slate.fr/story/66845/equipier-quick-twitter-plainte-justice).

[^30]: Le Parisien, «&nbsp;[Quick d’Avignon&nbsp;: Benjamin, 14 ans, est bien mort intoxiqué](https://www.leparisien.fr/faits-divers/quick-d-avignon-benjamin-14-ans-est-bien-mort-intoxique-18-02-2011-1321649.php)&nbsp;», 18/02/2011.





<!------------ CHAP 2 le militantisme deconnant un sabotage ----------->

## Le militantisme déconnant, un sabotage&nbsp;?&nbsp;!&nbsp;?

Si nous parlons ici de sabotage, c’est parce que nous allons dans un
premier temps prendre exemple sur l’activité
malveillante d’infiltration dans des mouvements
militants afin de les rendre inefficaces voire de
les détruire, une activité pratiquée par les adversaires au
mouvement&nbsp;: il peut s’agir par exemple d’adversaires
idéologiques qui ne sont pas des professionnels, comme les
fascistes peuvent créer de faux profils
«&nbsp;SJW&nbsp;»[^31] (qu’ils estiment ennemis) et jouer à
la militance déconnante pour les décrédibiliser et détruire globalement
l’image de la cause. L’histoire évoquée précédemment où il a été
exigé d’ajouter un «&nbsp;TW&nbsp;»[^32] sur une recette contenant
du Kiri ressemble fortement à une pratique de ce genre, jouée par
quelqu’un qui chercherait à ridiculiser les végans, à les faire
passer pour des «&nbsp;fragiles&nbsp;» (mais ce n’est qu’une hypothèse,
rien ne permet d’affirmer qu’il y aurait derrière un véritable
saboteur anti-vegan, je me permets de prendre cette illustration
justement parce qu’un saboteur hostile ne s’y serait pas mieux
pris).

Ici, on va surtout aborder le jeu de l’infiltration selon
les conseils des renseignements, à savoir l’OSS (ex-CIA, qui a
déclassifié des documents datant de la Seconde Guerre
Mondiale).

Attention, si nous nous appuyons sur les techniques de sabotage social
de l’OSS pour illustrer le militantisme déconnant, ce n’est aucunement
pour affirmer que tout saboteur qui nuit au mouvement est
systématiquement un adversaire infiltré. Ce que nous voulons au
contraire montrer, c’est qu’un militant déconnant s’évertue sans le
vouloir à saboter de l’intérieur son propre mouvement, à savoir&nbsp;:
susciter de la méfiance ou de la répulsion chez le potentiel allié ou
spectateur, offrir à l’adversaire une relative tranquillité permettant à
ce dernier de nuire de plus belle, de gripper le travail d’information
efficace, empêcher de construire de régler les problèmes de fond qui
sapent la militance.



### Les leçons de déconnage par l’OSS


Dans les années 40, l’OSS a rédigé un guide à
l’usage des citoyens de pays occupés durant la Seconde Guerre
Mondiale afin de les aider à entraver l’activité des
Nazis, notamment parce que les citoyens étaient forcés de
travailler pour eux. Tout était bon à prendre pour générer et
propager l’improductivité, l’inefficacité dans presque tous les
corps de métiers, ce qui était bénéfique pour les citoyens,
résistants et alliés puisque la production était pompée par
l’occupant et servait à la destructivité. Ainsi, ces leçons font
l’inventaire de ce qui pourra perturber, entraver les environnements
sociaux.


<figure>
    <img src="Pictures/simplesabotage.png"
         alt="Simple Sabotage Field Manual by United States. Office of Strategic Services">
    <figcaption><em>Simple Sabotage Field Manual by United States. Office of Strategic Services</em>.  Vous pouvez retrouver le guide au complet sous tous les formats <a href="http://www.gutenberg.org/ebooks/26184?msg=welcome_stranger">ici</a>&nbsp;; un mot de la CIA à son sujet sur <a href="https://www.cia.gov/news-information/featured-story-archive/2012-featured-story-archive/simple-sabotage.html">cia.gov</a>&nbsp;;  on a traduit une partie du guide <a href="https://www.hacking-social.com/2016/05/09/être-stupide-ou-lart-du-sabotage-social-selon-les-lecons-de-la-cia/">ici</a>.</figcaption>
</figure>



Par exemple, l’OSS conseille aux saboteurs ceci&nbsp;:

> «&nbsp;(2) faites des «&nbsp;discours&nbsp;». Parlez aussi fréquemment que possible et très longuement. Illustrez vos «&nbsp;points&nbsp;» par de longues anecdotes et expériences personnelles. N’hésitez pas à faire quelques commentaires patriotiques appropriés.
> 
> (6) Reportez-vous aux questions résolues de la dernière réunion et tentez de rouvrir le débat à leur sujet.
> 
> (8) Inquiétez-vous au sujet de la légalité et de la légitimité de toute décision&nbsp;: posez la question de savoir si telle action envisagée relève ou non de la compétence du groupe, inquiétez-vous publiquement du fait que cela pourrait être une action qui entre en conflit avec la politique des supérieurs.&nbsp;»

On pourrait retrouver ces comportements dans la militance&nbsp;:
par exemple, lors de réunions ou de communication portant sur la
création d’un site web, les saboteurs vont discourir
sans fin sur un point hors-sujet ou peu pertinent alors que l’ordre du
jour était clair et précis&nbsp;; ils vont s’énerver
sur l’usage inapproprié d’un smiley&nbsp;; ils vont tenir un discours
narcissisant en vantant leurs victoires/qualités
ou compétences&nbsp;; ils vont débattre sans fin sur la pureté
éthique de l’hébergeur&nbsp;; etc. Sur les réseaux sociaux/les
tchats, cela peut se faire en détournant une
conversation portant sur un sujet X très clair en parlant
de toute autre chose, par exemple en lançant un débat
enflammé sur tel mot employé (et en ignorant totalement le sujet X).

Résultat&nbsp;: l’action n’avance pas, donc la cause non plus. Tout le
monde craint la future réunion ou communication parce que ça va être
très saoulant et inutile, certains n’osent plus soulever des points
pertinents tant la parole est occupée et le débat dominé par ces
individus. Le sentiment d’impuissance s’installe.

> «&nbsp;(4) Posez des questions non pertinentes aussi fréquemment que possible.
> 
> a. donnez des explications longues et incompréhensibles quand vous êtes interrogé&nbsp;»

Par exemple, lors d’un live sur internet, cela peut
se manifester par le fait de soulever des questions qui ne sont pas
du tout dans le sujet. Imaginons une discussion portant sur la
place de la philosophie dans la vulgarisation, une remarque sapante
consistera à demander pourquoi l’interlocuteur boit un coca,
emblème d’une horrible multinationale alors qu’il
faudrait tous être parfaitement anticapitaliste/écolo&nbsp;; en
lui posant des questions sans lien avec le propos, s’il connaît
l’UPR (question qui sera réitérée autant de fois que possible)&nbsp;;
le suspecter de mensonge et d’incompétence parce qu’il ne cite pas de
tête l’intégralité des chiffres de telle étude et ceux des 15
méta-analyses qui ont suivi&nbsp;; en lui demandant avec un ton
accusateur pourquoi il ne parle pas de l’attentat
qui a eut lieu il y a 7 mois alors que c’était
horrible et que c’est ça le vrai ennemi, le principal sujet,
etc.

On pourrait aussi taxer ces méthodes de trolling, mais le
but du trolling consiste juste à instaurer une
forme de chaos et pour le troll, le but de tester n’importe quel
comportement&nbsp;; or un militant déconnant peut sincèrement croire que sa
technique autosabotante est bonne pour son mouvement&nbsp;:


<figure>
    <img src="Pictures/upr.png"
         alt="Campagne UPR">
    <figcaption>J’ai été étonnée que la technique extrêmement saoulante de 2017 de l’UPR était en fait assez formalisée, semblait être considérée par ces militants comme une «&nbsp;bonne stratégie&nbsp;» (<a href="https://twitter.com/UPR_Ressources/status/844165147165908992">Source</a>).</figcaption>
</figure>



Résultat&nbsp;: ça saoule les intervenants et ceux qui sont intéressés
par le sujet, ça coupe ou empêche de voir les questions pertinentes
(donc ça entrave une communication, un débat constructif qui
aurait pu avancer), ça dégrade l’image de la cause des
questionneurs auprès des spectateurs parce que c’est associé à une
espèce de prosélytisme intempestif ou
à des interruptions non pertinentes.

> «&nbsp;(5) Soyez tatillon sur les formulations précises des communications, des procès verbaux, des bilans.
> 
> d. soyez aussi irritable et querelleur que possible sans pour autant vous mettre dans l’ennui.&nbsp;»

Ici on arrive à un grand classique des réseaux sociaux, qui a débuté il
me semble par le mouvement des grammar nazis (qui sont à ma grande
surprise de «&nbsp;vrais&nbsp;» militants qui croient sincèrement en leur
croisade[^33], même si manifestement personne n’est jamais tombé
amoureux de la langue française en se faisant juger comme un criminel
sous prétexte qu’il avait mal gèrè l’inclinaison d’un accent) qui va
commenter uniquement pour souligner les fautes d’orthographe, de
grammaire, ou pour critiquer l’usage des anglicismes, la prononciation
des mots, l’accent, l’espace en trop, la TYpogrAPHie..................
ETC............ Et ça donne ce genre de chose&nbsp;:

> «&nbsp;Un jour, j’ai discuté avec un mec qui avait signalé une faute de ponctuation dans le statut Facebook d’une amie qui remerciait les gens pour leurs condoléances à la suite du décès de sa mère... Il n’avait même pas vraiment lu le statut en question. Et j’ai compris que moi non plus, je ne lisais pas vraiment les statuts. Je me contentais de chercher les fautes.&nbsp;» ([20minutes.fr](https://www.20minutes.fr/culture/1925415-20160919-orthographe-grammar-nazis-repentis-racontent-pourquoi-embeteront-plus-fautes))


Ce type de chasse et de procès à la «&nbsp;faute&nbsp;» peut aussi se faire à
l’encontre de termes qui sont accusés d’être malveillants&nbsp;: par exemple,
une personne vient se confier, à bout, dans l’espoir de trouver du
réconfort et de l’aide (même en privé) auprès de ses alliés, et ne
reçoit en retour qu’une violente correction «&nbsp;*ce mot que tu as employé
est psychophobe/insultant envers les travailleurs du sexe/homophobe/,
etc*&nbsp;», sans que son propos ait même été entendu&nbsp;:

> «&nbsp;Récemment, elle [une militante] a vu avec amertume une jeune mère, victime de violences, qui sollicitait de l’aide sur un groupe Facebook de parentalité féministe, se faire corriger, car elle n’employait pas les termes jugés inclusifs pour les personnes trans ou non binaires. «&nbsp;Elle avait besoin de manger, pas qu’on lui dise comment s’exprimer.&nbsp;» ([neonmag.fr](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html))

On voit que cette tâtillonerie s’oppose directement au travail militant,
dans le sens où s’attaquer au terme mal employé a pour conséquence de ne
pas aider cette femme bien que ce soit pourtant un objectif central du
mouvement. Le travail est donc directement saboté avant même de
commencer.

La formulation précise devient un devoir tellement suprême que le
militant déconnant semble supprimer toute empathie pour autrui, nie son
émotion, ses besoins et encore plus si c’est un allié.

Résultat&nbsp;: les personnes n’osent pas parler sans over-justifier leur
propos, n’osent pas exprimer des émotions ou aborder des sujets qui les
touchent de peur d’utiliser de mauvais termes malgré eux, voire n’osent
même pas rejoindre les mouvements de peur de ne pas avoir les bons mots,
les bons codes&nbsp;:

> «&nbsp;Elle est étudiante et souhaite s’engager pour la première fois dans l’association dont je suis membre. Au téléphone, elle tourne autour du pot, hésitante, comme tourmentée. Et finit par admettre qu’elle a très peur de mal s’exprimer. De ne pas employer les mots justes. De ne pas savoir. Sa crainte a étouffé jusqu’ici ses envies d’engagement. Tandis que je tente de la rassurer, je lis dans son angoisse la confirmation d’un phénomène que j’observe depuis que j’ai l’œil sur les mouvements de défense de la justice sociale&nbsp;: une forme d’intransigeance affichée, propre à inhiber ou décourager certaines bonnes volontés. Une course à la pureté militante qui fait des ravages. ([neonmag.fr](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html)).

L’autre conséquence de tout cela est d’en venir à percevoir le mouvement
comme un ennemi, puisqu’à force les seules interactions que l’on peut
avoir avec lui en tant que tiers peuvent n’être qu’attaque et
réprobation, et par un phénomène d’escalade et de réactance[^34], le
tiers ciblé va progressivement se positionner dans le clan adverse.

Globalement, l’action est freinée, l’adversité n’est pas combattue,
voire est protégée par ces comportements (elle peut continuer sa
destructivité en toute tranquillité). Plutôt que de convaincre, la cause
est de plus en plus décrédibilisée et associée à une nuisance, les
alliés et spectateurs sont usés de tant d’inefficacité ou de subir tant
de reproches constamment. Il y a donc sabotage de la cause. Du moins ce
sont des pratiques conseillées par les renseignements si on veut rendre
inefficace, infécond un environnement social qu’on estime adversaire et
ne pas être repéré comme saboteur. Si on est un militant sincère dans
ses engagements, je doute que d’obtenir ces résultats soit satisfaisant.

Je n’ai parlé que de l’OSS pour montrer à quel point le militantisme
déconnant ressemble à un sabotage (donc, pourquoi le poursuivre,
pourquoi persister à croire que c’est un «&nbsp;bon&nbsp;» jeu&nbsp;?), mais on aurait
pu continuer le parallèle avec le travail d’infiltration ou de
contre-propagande, avec des exemples plus modernes comme ceux du
renseignement/contre-renseignement (Cointelpro[^35], JTRIG[^36]) ou
même les stratégies des fascistes[^37].


<figure>
    <img src="Pictures/trig.png"
         alt="GCHQ / JTRIG">
    <figcaption>Toutes les disciplines dans lesquelles le renseignement (ici anglais, GCHQ / JTRIG) pioche pour mieux duper, détourner, manipuler, etc. sur le Net. Il s’agit d’un <em>leak</em> de Snowden. Quand des membres du gouvernement dénigrent les sciences humaines et leur utilité, c’est de la pure hypocrisie (ou de l’ignorance&nbsp;?? mais j’en doute) puisque le renseignement (comme la communication politique) pioche largement dedans pour ses stratégies. Plus d’infos sur <a href="https://theintercept.com/2014/02/24/jtrig-manipulation/">The Intercept</a>.</figcaption>
</figure>



<!------------ notes CHAP 2 ----------->



[^31]: SWJ = *Social Justice Warrior*, guerrier pour une justice
    sociale. Le terme a pris avec le temps une connotation négative,
    plus d’infos [sur Wikipédia](https://fr.wikipedia.org/wiki/Social_justice_warrior).

[^32]: TW = *Trigger Warning* (avertissement) il s’agit d’une mention
    utilisée souvent sur les réseaux sociaux afin de prévenir d’un
    contenu pouvait choquer ou réveiller des traumatismes chez les
    personnes

[^33]: Benjamin Chapon, «&nbsp;[Orthographe: Des Grammar Nazis repentis racontent pourquoi ils ne vous embêteront plus avec vos fautes](https://www.20minutes.fr/culture/1925415-20160919-orthographe-grammar-nazis-repentis-racontent-pourquoi-embeteront-plus-fautes)&nbsp;», *20 Minutes*, 19/09/2016.

[^34]: Lorsqu’on interdit quelque chose à quelqu’un, qu’on le rend
    moins accessible ou qu’on lui retire une possibilité d’action qu’il
    avait auparavant, l’individu aura tendance à la vouloir plus, quand
    bien même il n’en avait cure avant. C’est la réactance, une réaction
    irréfléchie devant l’interdit, qui parfois fait choisir à l’individu
    des choses qui lui nuisent, lui sont inutiles, ou nuisent à ses
    proches/la société. Par exemple, vouloir polluer à cause d’interdits
    écologiques, refuser des vaccins à cause d’obligations à se faire
    vacciner de la part des autorités, se mettre à aimer un contenu nazi
    parce que celui-ci a été censuré, etc. Nous avons fait [une vidéo à
    ce sujet](https://skeptikon.fr/videos/watch/674aa752-4bd0-40d7-9298-66c845e4872a).

[^35]: Plus d’infos sur [Korben.info](https://korben.info/techniques-secretes-controler-forums-opinion-publique.html) et sur *[Le Monde Diplomatique](https://www.monde-diplomatique.fr/1995/08/COMBESQUE/6545)*.

[^36]: Une présentation générale sur [Wikipédia](https://fr.wikipedia.org/wiki/Joint_Threat_Research_Intelligence_Group). Tous les documents révélés par Snowden à ce sujet sur [search.edwardsnowden.com](https://search.edwardsnowden.com/search?utf8=%E2 %9C%93&q=JTRIG&document_topic_facet=Information+Operations) (qui détaille certaines stratégies et montre l’efficacité des
    opérations de contre-propagande/infiltration pour saper l’image d’un mouvement ou carrément le détruire)

[^37]: Midi Libre, «&nbsp;[Un militant repenti balance les secrets de l’ultra-droite](https://www.midilibre.fr/2012/10/08/un-militant-repenti-balance-les-secrets-de-l-ultra-droite,574771.php)&nbsp;», 08/10/2012.




 
<!------------ CHAP 3 le militantisme deconnant un sapage des besoins fondamentaux ----------->

## Le militantisme déconnant, un sapage des besoins fondamentaux&nbsp;?

On pourrait encore arguer que quand bien même tout ceci ressemble à un
sabotage de l’OSS, il n’en reste pas moins que les cibles finissent par
adopter les bons mots, par faire attention à leurs comportements, sont
plus «&nbsp;pures&nbsp;» dans leurs pratiques. Il y a plus de perfection, plus de
conscienciosité, un tri dans les membres les plus capables de
conscienciosité vertueuse, le combat ne comporte plus ces zones grises
portées par ses membres les plus faillibles. On aurait en quelque sorte
une «&nbsp;éducation&nbsp;» parfaite où seuls les meilleurs resteraient en
course et obtiendrait leur diplôme de militant méritant sa place, une
élite de chevalier blanc à l’alignement «&nbsp;loyal bon&nbsp;», qui serait
ensuite la plus à même de partir en croisade en ne laissant rien passer.

<figure>
    <img src="Pictures/croisadesemilesignon.jpg"
         alt="Horreur des croisades">
    <figcaption>Les croisades étaient hautement «&nbsp;impures&nbsp;», c’était un bain de sang et une multitude d’horreurs que je n’oserais même pas rapporter tant c’est insupportable… Dans «&nbsp;The psychology of genocide, massacre, and extreme violence&nbsp;: why normal people come to commit atrocities&nbsp;», Donald D. Dutton a donné tous les détails de ces horreurs si cela vous interesse&nbsp;; mais à choisir (et pour éviter la dissociation, les traumatismes) je vous conseille la biblio de Semelin qui explique de façon moins traumatique les mécanismes de haute violence, génocidaire.</figcaption>
</figure>


Cependant, ces techniques, en plus de saboter l’efficacité d’un
mouvement, sapent aussi les besoins psychologiques fondamentaux de ceux
qui en sont cibles (spectateurs, alliés, adversaires). Ces techniques,
notamment lorsqu’elles sont employées par des militants ne voulant pas
saboter (mais le faisant néanmoins malgré eux, avec une justification de
pureté) sont aussi révélatrices que leurs besoins psychologiques
fondamentaux sont peut-être sapés, et que c’est pour cela qu’ils
adoptent ces techniques dysfonctionnelles.

La théorie de l’autodétermination[^38], issue de la psychologie
sociale de la motivation, a sélectionné trois besoins psychologiques
fondamentaux chez l’humain qui, lorsqu’ils sont comblés par un
environnement social, vont l’aider à se développer de façon
autodéterminée (et aucunement selon un modèle préétabli par des normes).
La personne autodéterminée évolue, développe des motivations puissantes,
dans un bien-être qui a un impact positif sur la société (les
autodéterminés vont chercher à susciter l’autodétermination et le
bien-être chez les autres, ils sont très motivants, prosociaux,
altruistes et n’ont pas peur de s’exposer à des adversaires très
effrayants).

<figure>
    <img src="Pictures/theorieautodetermination.png"
         alt="La théorie de l’autodétermination">
    <figcaption>La théorie de l’autodétermination</figcaption>
</figure>


Autrement dit, un militant autodéterminé serait bienfaiteur dans un
mouvement parce qu’il va transmettre sa motivation, soutenir les alliés,
comprendre comment informer les spectateurs sans les démotiver, avoir
assez de courage pour prendre le risque de se confronter à
l’adversaire/l’adversité.

Mais pour espérer développer son autodétermination, il est nécessaire
que quelques environnements sociaux nourrissent ces trois besoins
fondamentaux, que sont l’autonomie, la compétence, la proximité sociale.
Est-ce que le militantisme puritain[^39] y répond&nbsp;?

### L’autonomie

&#9784; C’est pour l’individu être à l’origine de ses actions, pouvoir choisir,
pouvoir décider, ne pas être contrôlé tel un pion. Cela ne veut pas dire
être indépendant, vivre seul&nbsp;: on peut être dépendant d’autrui tout en
étant autonome&nbsp;; par exemple, on peut être dépendant d’autrui pour se
nourrir (c’est-à-dire ne pas cultiver sa propre nourriture, et devoir
aller en acheter) tout en étant autonome (on choisit ces lieux de vente
de nourriture selon ses valeurs, on décide de consommer ceci et pas
cela, etc.). C’est différent aussi du fait de vivre une situation de
liberté&nbsp;: on peut vivre objectivement une situation où toutes nos
fantaisies seraient possibles, où personne ne nous contraint à quoi que
ce soit et nous laisse décider, mais pour une raison ou une autre, on ne
parvient pas à faire ce qu’on voudrait faire, on n’arrive pas à décider
quoi faire, etc.

Lorsqu’un individu nous injonctive «&nbsp;tu dois/tu ne dois pas&nbsp;; il
faut/il ne faut pas&nbsp;!&nbsp;» le besoin d’autonomie est sapé parce qu’on se
sent sous contrôle de l’autre. Même si on refuse cette injonction, on
sent qu’on réagit automatiquement, et non selon notre propre décision&nbsp;;
pensez à l’ado à qui on interdit de regarder tel film et qui, dès qu’il
le pourra, ne saura résister à l’envie de le visionner, aurait-il eu
envie de voir ce film si on ne le lui avait pas interdit en premier
lieu&nbsp;? Moi-même je n’ai jamais eu autant envie de sortir que lorsque le
premier confinement a été mis en place.

C’est ce qu’on nomme la [Réactance](https://skeptikon.fr/videos/watch/674aa752-4bd0-40d7-9298-66c845e4872a
).

<figure>
    <img src="Pictures/reactance.jpg"
         alt="La réactance">
    <figcaption>La réactance. Hacking Social. Sur <a href="https://skeptikon.fr/videos/watch/674aa752-4bd0-40d7-9298-66c845e4872a">Peertube</a>, sur <a href="https://www.youtube.com/watch?v=hZueeA9b1xY">Youtube</a>, sur <a href="https://vimeo.com/542521803">Vimeo</a></figcaption>
</figure>


On réagit automatiquement à l’interdit, au censuré, à l’inaccessible, en
voulant y accéder encore plus parce que l’injonction est un sapage de
notre besoin d’autonomie. On veut pouvoir décider, choisir, être libre,
maintenir ouvertes des possibilités, donc dès lors qu’un environnement
social coupe un pan de possibilités, surtout sur le ton de l’injonction,
automatiquement on peut vouloir faire l’exact inverse. Le militantisme
puritain déconnant va donc probablement nous pousser à faire l’inverse
de ce qu’il recommande, à cause de la réactance. Par exemple, si un
créateur sur la toile ne cesse de recevoir des injonctions militantes
l’incitant à rejoindre telle plateforme libre, cette insistance
catégorique fera office de repoussoir&nbsp;: le créateur associera ces
injonctions déplaisantes à la plateforme vantée et s’en détournera
définitivement alors qu’il était pourtant à la base pleinement ouvert à
l’idée de s’y installer. Donc sa stratégie est totalement
contre-productive et sabote la croisade du militant puritain, sauf s’il
visait secrètement à énerver tout le monde.

L’injonction faite à autrui, lorsqu’il ne demande pas à être évalué,
guidé, est une tentative de contrôle de son comportement et, que le
militant le conscientise ou non, ça va saper la personne visée. Par
conséquent, l’injonctivé va soit réagir d’une façon réactante, ou à
terme voir l’injonctiveur et les valeurs qu’il porte en ennemi (qui aime
les grammar nazis et leur «&nbsp;combat&nbsp;»&nbsp;?)&nbsp;; soit l’injonctivé va obéir,
mais avec du mal-être (cette sensation déplaisante dans le ventre, cette
gêne qui nous donne l’impression d’avancer à reculons, ce sentiment
contradictoire où on accepte à la fois de se lancer dans un projet tout
en espérant secrètement pouvoir emprunter dès que possible la première
porte de sortie). Il va alors se sentir pion, s’inférioriser,
culpabiliser, avoir honte, puis autocontrôler son comportement avec un
stress et une pression qu’il aura intériorisés. C’est ce qu’on appelle
une introjection (qu’on expliquera plus tard en détail) et qui génère
une motivation très médiocre pour le combat, peu efficace, créant plus
de mal-être chez les personnes, n’étant pas durable dans le temps et peu
efficace.

Mais l’autonomie n’est pas uniquement sapée par les injonctions&nbsp;: toutes
les manœuvres autoritaires qui vont tenter de contrôler l’individu, qui
sont perçues comme des contrôles, vont le saper et en conséquence
provoquer soit une obéissance «&nbsp;introjectée&nbsp;» soit une réactance, mais
certainement pas une motivation de haute qualité (par exemple, devenir
si fan d’orthographe que corriger des erreurs générerait un plaisir et
une efficacité aussi intenses que si on jouait à un jeu vidéo).

Parfois, selon le niveau de violence de la tentative de contrôle, de
sapage de l’autonomie de l’autre, cela peut conduire l’individu à une
amotivation totale, et détruire tout élan. Je me rappelle un
informaticien avec qui j’avais échangé par mail, passionné par le
domaine, avec ce genre de passion qui éveille une vocation et guide
votre vie. Il avait perdu toute motivation après avoir connu une
entreprise extrêmement pressante, injonctive, harcelante. De même dans
l’enseignement, Gull me rapportait avoir entendu de nombreux collègues
qui avaient le cœur à l’ouvrage à leur entrée dans la profession, des
projets pleins les yeux, une motivation inébranlable… et qui, à force
de contrôles sur leur travail, de nouvelles procédures à respecter, de
surveillance et d’évaluation, de refus ou de complication de la part de
la hiérarchie et des parents qui semblaient savoir mieux qu’eux comment
faire cours, avait perdu toute motivation, ne proposaient plus aucun
projet, parfois quittaient l’enseignement ou attendaient impatiemment,
pour les plus anciens, la retraite. L’autoritarisme, le contrôle,
détruisent l’élan des personnes qui étaient les plus susceptibles d’être
extrêmement performantes si on les avait laissées tranquilles.

L’injonction ne fonctionne que pour se faire obéir sur le court terme,
mais reste néanmoins utile dans une situation de haut danger&nbsp;: quand un
pompier vous hurle de vous éloigner de ce trottoir parce qu’il y a un feu à
proximité, il y a tout intérêt à ne pas être réactant. Et c’est
exactement pour cette raison aussi que je pense que les militants n’ont
pas nécessairement une volonté malsaine de contrôle des autres, et
qu’ils ne cherchent pas à dominer lorsqu’ils ordonnent&nbsp;: pour eux, il y
a un feu que personne ne voit et ils endossent le rôle de pompier. Ils
peuvent être comme ça pour des raisons de surmenage, de climat social
menaçant[^40], ou comble du comble, parce que leur propre besoin
d’autonomie est sapé… Effectivement, tenter de contrôler l’autre,
c’est décider, faire un choix, agir&nbsp;: le militant puritain peut se
sentir ponctuellement comme restauré dans son autonomie, dans son besoin
de compétence quand il contrôle l’autre. Cependant comme cela ne mène
généralement à rien, il va recommencer sans cesse pour sentir à nouveau
cette petite dose de satisfaction, le plongeant dans un cercle infini ou
rien ne se règle, ni ses besoins, ni les besoins d’autrui, ni les buts
de la militance. C’est assez proche d’une mécanique d’addiction au
final&nbsp;: le militant a trouvé un moyen de satisfaire un besoin, sauf que
la solution n’est qu’illusoire (comme le shoot d’une drogue qui finira
par disparaître), que le besoin n’est jamais comblé, donc la personne
insiste encore plus fort dans sa stratégie illusoire, ça ne donne rien,
il continue plus fort, etc. C’est un cercle vicieux qui ne prendra fin
que lorsqu’une voie totalement différente sera envisagée.

L’autre raison de ces tentatives de contrôle d’autrui chez les militants
puritains sapant l’autonomie des personnes (et frustrant leurs propres
besoins) s’explique tout simplement parce que quasi tous nos
environnements sociaux fonctionnent de la sorte&nbsp;: école, travail, champ
politique, champ culturel, messages médiatiques… Tous injonctivent,
ordonnent des comportements, y compris hautement contradictoires[^41],
alors on pense que c’est la chose à faire pour changer les comportements
des autres afin qu’ils soient plus vertueux. On valorise le «&nbsp;bon&nbsp;»
comportement en le rehaussant dans une espèce de hiérarchie sociale,
voire en le récompensant, et le mauvais est infériorisé et puni. On
reproduit ce modèle de contrôle par crainte d’être en bas de cette
pyramide ou pour rester en haut, garder une valeur, ne pas être
ostracisé, parce qu’il n’y a que très peu d’environnements où les choses
fonctionnent différemment. En cela, la militance déconnante reproduit
les logiques dominantes, et pourrait perdre son titre de déconnante ou
de puritaine pour celle de conformiste.

Voici un petit schéma qui résume le besoin d’autonomie avec ce qui le
menace ou le soutient&nbsp;:



<figure>
    <img src="Pictures/autonomie.png"
         alt="Autonomie">
    <figcaption>Par supervision, on peut entendre toute personne qui va tenter d’avoir une influence sur le comportement de l’autre. On peut menacer l’autonomie en étant contrôlant (cadre mauve à gauche), mais même si je n’en parle pas dans cet article, on peut aussi la saper en étant manipulateur (cadre marron en bas). En militance déconnante, ça pourrait être perçu par la cible comme de la manipulation. Par exemple, une injonction enrobée dans des étiquetages positifs «&nbsp;toi qui es si engagé, tu devrais être exclusivement sur PeerTube&nbsp;», que ça soit volontairement manipulateur ou non, pourra être perçu par la cible comme une tentative de sapage de son autonomie, un contrôle manipulateur sur elle.</figcaption>
</figure>




### La proximité sociale

&#9626; C’est pour l’individu le besoin d’être connecté à d’autres humains, de
résonner dans la société humaine, de recevoir de l’attention et des
soins par autrui, de se sentir appartenir à un groupe, à une communauté
par son propre apport significatif et reconnu comme tel&nbsp;; à l’inverse,
une proximité sociale est sapée par l’exclusion, l’ostracisation, les
humiliations, les dévalorisations, l’indifférence, les insultes, le
mépris, l’absence d’écoute, etc. Elle peut être frustrée par le manque
de contacts sociaux signifiants/ résonnants (par exemple, une
interaction d’achat n’est généralement pas très résonnante ni
comblante&nbsp;; un moment avec des proches qui ne réagiraient à rien de ce
qu’on dit ou qui n’écouteraient même pas serait très frustrante). Le
fait d’avoir un profil particulier concernant les rapports sociaux (par
exemple introverti VS extraverti) n’a aucun impact sur le fait d’avoir
plus ou moins besoin de proximité sociale&nbsp;: c’est simplement que
l’extraverti ou l’introverti n’auront pas les mêmes modes relationnels
préférés pour combler ce besoin (par exemple, l’extraverti peut préférer
échanger avec un groupe, l’introverti échanger avec une seule personne à
la fois). Mais tout le monde a besoin d’une connexion positive avec
d’autres[^42] humains, qu’importe son logiciel de base et les modalités
sociales préférées pour y accéder.

Je pense qu’il n’est pas nécessaire de revenir sur les exemples déjà
évoqués pour montrer en quoi l’injonction ou d’autres pratiques
déconnantes (les soirées foutage de gueule d’un collègue…), surtout
quand elle advient dans un contexte particulier (rappelez-vous l’exemple
des condoléances, de la femme violentée cherchant de l’aide, etc.), sont
un énorme sapage de proximité sociale, puisque l’échange social est
rendu impossible, tout du moins fort déplaisant.

Dans les cas où la proximité sociale est nourrie, on a un schéma de
communication qui ressemble à ceci&nbsp;:

<figure>
    <img src="Pictures/competenceemotionnelles.png"
         alt="La dynamique interpersonnelle du partage social des émotions">
    <figcaption>Schéma issu de&nbsp;: Moïra Mikolajczak, <em>Les compétences émotionnelles</em>, Paris, Dunod, 2009. «&nbsp;la dynamique interpersonnelle du partage social des émotions d’après Rimé (2009)</figcaption>
</figure>


Or, dans un militantisme déconnant, tout ceci s’arrête à la première
étape et à la place «&nbsp;**B**atman corrige/critique/injonctive/, etc.
**A**quaman[^43]&nbsp;»&nbsp;: l’émotion d’Aquaman est niée ou non prise en
compte, il n’y a aucun mécanisme d’empathie à son égard. Or, si à chaque
fois qu’une personne veut partager une émotion (y compris positive, par
exemple un intérêt pour un sujet scientifique), et qu’elle est rembarrée
parce qu’elle a fait une faute, alors je parie qu’elle va se mettre à
déprimer. Et si Batman ne cherche qu’à partager, exprimer son émotion
en répliquant à l’expression d’Aquaman, par injonction ou remarques
hors sujet, alors il n’y aura jamais la suite positive de ce schéma
puisque soit Aquaman déprime/se tait, soit il se met en colère contre
lui. Tout le monde est sapé dans sa proximité sociale. Batman et Aquaman
ne rejoindront jamais la Justice League.

Mais ça peut être aussi exactement pour cette raison que des militants
de certaines mouvances peuvent déconner et avoir des attitudes
contrôlantes/autoritaires&nbsp;: ils savent que ça va faire taire les Aquaman
qui cherchent à partager leurs émotions et c’est le but (par exemple,
les militants d’extrême-droite n’hésitent pas à être très sapants parce
qu’ils veulent vraiment détruire l’autre). Et si Aquaman s’énerve, ce
militant peut voir cela comme une victoire car l’énervement d’Aquaman
met en lumière le propos et la cause de Batman, captant potentiellement
l’audimat d’Aquaman. Cela peut devenir une tactique pour gagner en
visibilité, mais parfois aussi c’est une réponse désespérée à des
sapages passés&nbsp;: mieux vaut vivre une guerre avec les autres qu’être
fantôme auprès d’eux. C’est aussi pour cette raison qu’une des premières
règles d’Internet a été *don’t feed the troll*, cela permettait de
ponctuellement casser le cercle vicieux&nbsp;: cependant ce n’est pas une
stratégie pérenne ni adaptée à toutes les situations (faire comme si de
rien n’était face à un harcèlement massif est tout aussi sapant,
puisqu’on dénie soi-même sa souffrance).


<figure>
    <img src="Pictures/competencesemotionnelles2.png"
         alt="La dynamique interpersonnelle du partage social des émotions (corrigé)">
    <figcaption>Schéma issu de&nbsp;: Moïra Mikolajczak, <em>Les compétences émotionnelles</em>... version corrigée</figcaption>
</figure>





Tout ceci est un sacré cercle vicieux tant que Batman persiste à ne
s’exprimer qu’en s’appuyant sur l’expression d’Aquaman (et non en
s’appuyant sur lui-même, via son sujet partagé), mais le problème c’est
que lorsqu’on est en posture A, il n’y a parfois aucune écoute, ce qui
peut nous amener nous-mêmes à devenir un B déconnant. Une solution
serait tout simplement d’être un Batman écoutant jusqu’au bout (parce
que la relation sera plus sympa, enrichissante, qu’on construit une
amitié, un respect mutuel) ou un Aquaman partageant d’une façon nouvelle
pour laquelle des patterns n’ont pas été encore automatisés chez les
militants déconnants. Et cela a pour conséquences de prendre soin des
spectateurs, des alliés voire des adversaires qui verront peut-être que
ce membre d’un mouvement adverse nourrit sa proximité sociale plutôt
qu’il ne la sape, et donc peut être que ce mouvement est profitable.
C’est une action qui serait alors de l’ordre d’une construction.


<figure>
    <img src="Pictures/proximitesociale.png"
         alt="La proximité sociale">
    <figcaption>La proximité sociale</figcaption>
</figure>




### La compétence

&#9874; C’est pour l’individu, se sentir efficace dans son action, exercer ses
capacités, maîtriser les défis, se sentir compétent. Ce besoin est très
connecté à l’autonomie&nbsp;: si l’individu est sous contrôle de
l’environnement, il ne peut pas pleinement exercer ses compétences,
parce qu’il a besoin lui-même d’avoir du contrôle sur ses actions. Et il
peut se sentir autonome ou chercher plus d’autonomie par besoin
d’exprimer ses compétences.

Typiquement, plus un environnement social contrôle le moindre geste/mot
d’un individu -- par exemple, comme peuvent l’être des environnements de
travail qui imposent des scripts pour parler aux clients -- plus le
besoin de compétence est sapé, puisqu’on ne peut pas développer ses
propres manières de faire, son art singulier, son expérience de la
compétence, sa créativité. Et dans ces environnements très sapants,
l’une des voies rapides pour restaurer ce besoin de compétence est… de
dominer l’autre, de le contrôler. Nous voici encore face à un cercle
vicieux&nbsp;: on va tenter de contrôler l’activité de l’autre parce que
nous-mêmes avons été contrôlés et qu’exercer ce contrôle satisfait un
peu notre besoin de compétence et d’autonomie qu’on nous a préalablement
refusé.

Je prends un exemple&nbsp;: sur un Discord, j’ai vu une personne qui accusait
de volonté de domination un diagnostic très précis de neuro’ et y
voyait là un risque énorme d’emprise. Là, j’ai senti en moi comme une
pulsion de correction, mes mains se sont approchées du clavier
instantanément, l’argumentaire galopait dans ma tête, prêt à sortir pour
le «&nbsp;corriger&nbsp;». J’allais entrer dans le débat pour lui dire que c’était
totalement faux, qu’il n’avait rien compris à ce diagnostic et j’allais
lui balancer à la tête des tas de sources de neuropsycho. Vraiment de la
pure militance déconnante, conformiste, puritaine, alors que je ne suis
même pas neuropsy, ni militante dans une association défendant la vérité
vraie. Finalement, je ne l’ai pas fait parce que je savais d’expérience
que ça ne servait absolument à rien ni à moi, ni aux autres, et ça
aurait été juste un moment pénible pour tout le monde. Mais pourquoi
cette pulsion&nbsp;? Eh bien, si je remonte à la source de mon apprentissage
sommaire de la neuro en fac de psycho’, il y a des professeurs
formidables qui étaient vraiment extrêmement intéressants en cours
magistral. Mais ceux-ci faisaient aussi officiellement des partiels
visant à éliminer le maximum d’entre nous (ils prévenaient que le barème
serait volontairement très dur, voire injuste, parce qu’on était trop
nombreux), ainsi aucune minuscule erreur n’était tolérée&nbsp;: sur plus
d’une centaine d’étudiants, les meilleures notes en neuro étaient des
10. Mais cela n’avait même pas une «&nbsp;vraie&nbsp;» valeur académique, puisque
j’ai vu des collègues continuer en cursus neuropsy avec des moyennes de
3 à cette matière, l’institution ne prenait en compte que la moyenne générale.

Mon cerveau a bien enregistré le message menaçant qu’était «&nbsp;faute
minime = sanction lourde et injuste&nbsp;», et je peux reproduire ce schéma
sans m’en rendre compte. Non pas que j’adhère au fait de juger durement
les personnes, mais aussi parce qu’au fond je souhaite protéger autrui
des punitions lourdes associées à cette faute. Tout comme on corrige le
collègue de travail de ses fautes pour lui éviter la sanction du chef
qu’on sait encore plus intolérant et violent dans son jugement.

Autrement dit, on peut avoir tendance à corriger autrui parce qu’on a
soi-même parfois été corrigé de façon encore plus menaçante pour des
fautes encore moins lourdes. On s’en est généralement sorti et on a
réussi à faire avec, alors croyant protéger autrui et l’aider à réussir,
on reproduit l’évaluation, le jugement pour lui apprendre à mieux faire
les choses, lui éviter les menaces. Et là encore, vous voyez le cercle
vicieux&nbsp;: on maintient un même système en le reproduisant soi-même.



<figure>
    <img src="Pictures/lacompetence.png"
         alt="La compétence">
    <figcaption>La compétence</figcaption>
</figure>





<!------------ notes CHAP 3  ----------->



[^38]: Deci et Ryan, 2017 (revue du champ de la recherche sur
    l’autodétermination, mais cela a commencé en 1985 et de nombreux
    autres chercheurs s’y sont joints)

[^39]: «&nbsp;Personne qui montre une pureté morale scrupuleuse, un respect
    rigoureux des principes&nbsp;» selon le *Petit Robert*. La militance
    déconnante a pour particularité de viser cette pureté morale chez
    autrui (pas forcément chez elle), en le corrigeant, lui reprochant
    le moindre détail impur, etc.

[^40]: Et nous vivons tous depuis un an dans un climat social hautement
    susceptible d’être perçu comme menaçant, que ce soit d’un point de
    vue sanitaire, économique, écologique, politique ou autre.

[^41]: Pensez à l’époque pas si lointaine où l’on nous disait qu’il ne
    fallait absolument pas porter de masque puis ensuite qu’il fallait
    absolument porter le masque.

[^42]: Le besoin de proximité sociale est aussi universel (cf Chen et al. (2015)).

[^43]: J’ai mis des noms suivant ces lettres, parce que sinon se
    représenter un «&nbsp;a&nbsp;» ou un «&nbsp;b&nbsp;» me semble peu chaleureux. Je
    préfère imaginer pour ma part une confrontation ou une interaction
    positive entre Batman et Aquaman. Libre à vous de remplacer les
    lettres par d’autres personnages, tels que Bilbo et Aragorn ou
    Bigard et Astier.





<!------------ CHAP 4 Le militantisme déconnant causant une motivation de piètre qualité ----------->



## Le militantisme déconnant causant une motivation de piètre qualité

Ce sapage des besoins fondamentaux (tant du militant déconnant dans son
passé, que chez la cible qu’il vise) va ensuite générer chez celui qui
en est cible une motivation de piètre qualité, que sont les régulations
introjectées, externes ou une amotivation non autodéterminée.



<figure>
    <img src="Pictures/lesmotivations.png"
         alt="Les différentes motivations">
    <figcaption>En jaune les motivations qui naissent de situations répétées où nos besoins ont été comblés (ou non sapés) et en mauve les motivations issues des situations répétées où nos besoins ont été sapés (ou non nourris).</figcaption>
</figure>


### La motivation intrinsèque, détruite par le militantisme contrôlant

La motivation intrinsèque est la motivation la plus puissante qu’on
puisse avoir pour quelque chose&nbsp;: c’est la passion, cette activité qu’on
fait pour elle-même, qui nous ravit, nous comble, pour laquelle on
rêverait de faire carrière. De façon moins épique, toutes les activités
qu’on fait pour elles-mêmes et non pour ses résultats sont généralement
réalisées par motivation intrinsèque (jouer aux jeux vidéo, regarder des
séries, lire, se balader… bref tout ce qu’on peut aimer faire en soi).
C’est puissant, parce que l’élan l’est, qu’il n’y a besoin de rien de plus
pour nous motiver à la faire.

Or, nos environnements sociaux, s’ils ont un modèle contrôlant sapant
les besoins fondamentaux, ont tendance à détruire nos motivations
intrinsèques.

<div style="background: #F5F5F5">
&#8505; &#8674;  Dans une expérience de Deci, Schwartz, Sheinman et Ryan (1981), 36
professeurs ont été étudiés durant l’été, avant la rentrée scolaire. Il
a été testé leur orientation de causalité[^44] (qui était soit
autonome soit contrôlée), les actions qu’ils envisageaient pour le
contrôle des élèves (punir, récompenser) ou les actions de soutien
(écoute du problème, guide pour le résoudre). À 2 mois de l’année
scolaire entamée puis à 8 mois, leurs élèves ont complété des enquêtes
évaluant leur motivation et leur perception de soi. Ceux qui avaient eu
les professeurs les plus contrôlants avaient une motivation intrinsèque
en chute, une estime de soi en baisse et leurs compétences cognitives
avaient également chuté. Ces élèves avaient moins de curiosité quant au
travail scolaire, ils préféraient les tâches faciles plutôt que
difficiles, faisaient preuve de moins d’initiatives scolaires. Ils ont
renouvelé cette étude dans un autre district scolaire. Ils ont
sélectionné des professeurs soit hautement contrôlants soit soutenant
l’autonomie. La motivation intrinsèque des élèves a été testée durant la
2e semaine d’école puis deux mois plus tard. Avec les enseignants
soutenant l’autonomie, la motivation intrinsèque a augmenté, ainsi que
la compétence perçue. C’était le contraire avec les professeurs
contrôlants.
</div>

Plus précisément, les façons de faire contrôlantes nous dégoûtent de ce
qu’on aimait naturellement faire, puisque la motivation intrinsèque
chute lorsqu’on est surveillé[^45], menacé de punition[^46], qu’on a
un objectif et un temps d’exécution limités[^47], qu’on est mis en
compétition[^48], évalué avec des feedbacks négatifs[^49], qu’il y a
la présence de personnes totalement indifférentes à notre
activité[^50], qu’on est récompensé⋅e selon une performance
donnée[^51] (par exemple, avoir son salaire/son cadeau/un compliment
uniquement si on atteint une performance demandée par le superviseur&nbsp;;
le salaire ne sape pas la motivation intrinsèque s’il est prévu en
amont, qu’importent les performances).

À l’inverse, lorsqu’on vise la préservation de la motivation intrinsèque
avec sa transmission (par exemple, un militant qui montre tout le fun
qu’il y a à une pratique écolo), alors la personne a tendance à
s’engager et il y a un effet de débordement[^52] (ici, elle se
mettrait d’elle-même à chercher d’autres pratiques écolos qui pourraient
être tout aussi fun). C’est plaisant pour tout le monde, efficace en
termes de militance, pas plus coûteux que d’injonctiver.

Pourtant, le militant aux pratiques déconnantes va plutôt reproduire le
modèle de contrôle (et pas celui de la transmission de la motivation
intrinsèque), quand bien même ce modèle a détruit certaines de ces plus
belles motivations par le passé[^53]. Pourquoi&nbsp;? Eh bien parce qu’en
plus de détruire notre motivation intrinsèque, ce vécu sous modèle
contrôlant peut nous plonger dans des motivations contrôlées de
l’extérieur, par exemple la motivation introjectée&nbsp;: l’enfant dans la
classe au professeur contrôlant perd non seulement sa motivation
intrinsèque, mais cherchant à réussir les objectifs pour ne pas être
ostracisé, humilié, il fait alors tout pour éviter la honte, la
culpabilité, etc. C’est pourquoi l’estime de soi chute&nbsp;: les résultats
scolaires «&nbsp;mauvais&nbsp;» sont sans doute accompagnés des remarques
négatives et de la dévalorisation de la part du professeur. Tout
jugement militant puriste peut voir des effets similaires sur une
personne visée.

### La motivation à régulation introjectée, celle du militant déconnant&nbsp;?

À force d’être dans des environnements qui tentent de contrôler notre
comportement, notre comportement général est complètement guidé par le
potentiel jugement de l’extérieur, sans même qu’une autorité soit
présente&nbsp;: on fait alors les choses prioritairement pour éviter d’avoir
honte, de se sentir coupable, d’être pointé du doigt, de perdre encore
de la valeur auprès des autres, d’être marginalisé, ridiculisé, etc. La
motivation introjectée est la plus répandue chez les personnes, pour à
peu près n’importe quelle activité.

Le militant déconnant peut provoquer une motivation introjectée chez
autrui en étant contrôlant&nbsp;: «&nbsp;je vais éviter de faire des fautes,
sinon les grammar nazis vont encore me tomber dessus&nbsp;», il n’y a aucune
motivation intrinsèque qui guide ce comportement (telle que «&nbsp;je
ressens de la satisfaction à écrire sans fautes&nbsp;») ni intégrée («&nbsp;je
vais tenter d’écrire sans fautes pour que les autres comprennent bien
mon message&nbsp;»). S’il n’y avait pas de grammar nazi, alors cette
personne à motivation introjectée cesserait de faire attention, ce qui
signifie que la valeur intrinsèque à l’orthographe n’était absolument
pas transmise. Mais on voit bien là-dedans que les militants déconnants
vont interpréter ce constat comme une justification de leur contrôle&nbsp;:
«&nbsp;si on ne les juge/surveille/injonctive pas, alors les gens font
n’importe quoi&nbsp;», or ce n’est pas cela le problème. Le problème c’est
que ces grammar nazis n’ont pas transmis l’orthographe d’une façon qui
soit perçue comme agréable, fun, socialement utile, connectante, donc
pourquoi les gens suivraient-ils leurs recommandations de manière autonome&nbsp;?

Le militant déconnant peut lui-même être en motivation introjectée pour
la cause qu’il défend, donc il est contrôlant envers autrui parce qu’il
n’a lui-même aucune motivation intrinsèque ou intégrée pour la cause
(comment dès lors transmettre quelque chose dont il ne connaît pas la
dynamique et les conséquences positives&nbsp;?). Par exemple, le grammar nazi
a peut-être appris l’orthographe à coup d’humiliation, donc humilie
autrui à son tour pensant lui faire «&nbsp;bien&nbsp;» apprendre. Il peut même
avoir un authentique élan altruiste à contrôler autrui tel que «&nbsp;il faut
que je lui montre comment être parfait sinon il va se faire humilier
encore plus&nbsp;»&nbsp;; cependant quand bien même ce n’est pas méchant ou
égoïste, c’est néanmoins la perpétuation d’une pratique qui cause un
mal-être, et le légitime. La seule voie de sortie de ce cercle vicieux
contrôle &#10141; introjection &#10141; contrôle &#10141; introjection &#10141;, etc. est de procéder
différemment face à un contrôle initial ou de décortiquer ces
introjections pour les comprendre, puis décider ce que l’on souhaite
vraiment en faire.

### La motivation compartimentée&nbsp;: ou comment la militance peut devenir violente

La motivation introjectée n’est pas la «&nbsp;pire&nbsp;» pour autant, puisqu’elle
n’est généralement pas liée à une violence envers les autres. Si on est
militant à motivation introjectée ou qu’on provoque de l’introjection
chez les autres par nos introjections, on ne va pas pour autant se
transformer ou transformer les autres en combattants violents. On
alimentera juste une saoulance générale, et les motivations pour la
cause ne seront pas de très bonne qualité[^54] (tant chez les
militants que chez les spectateurs, alliés ou toute cible de cette
saoulance).

Par contre d’autres configurations complexes de la motivation amènent à
soutenir une violence envers des personnes, voire à l’être soi-même&nbsp;;
c’est le cas de la motivation à identification compartimentée (ou dite
fermée, défensive), dont les tenants et aboutissants sont complexes à
démêler.

Rassurez-vous, dans les cas cités en introduction, je ne crois pas qu’un
seul des exemples déconnants cités n’ait été conduit par ce type de
motivation, encore moins il me semble chez les libristes (du moins je
n’en ai pas vécu personnellement). Généralement on repère ces
motivations malsaines lorsque c’est la haine qui conduit l’activité,
qu’il y a un «&nbsp;nous contre eux&nbsp;» ethnocentrique (voir définition dans le
cadre ci-dessous)&nbsp;: le groupe zététicien que j’ai évoqué, dont une des
activités était de passer des soirées à se foutre d’un autre zététicien,
de se gargariser à le haïr tous ensemble, avait tout de même un côté
«&nbsp;motivation identifiée compartimentée&nbsp;», puisque l’identification au
groupe passait uniquement par le fait de haïr un «&nbsp;ennemi&nbsp;»
désigné, sans rien créer. Cependant, je peux difficilement analyser
cette dynamique et comprendre son origine, parce qu’on a quitté le
groupe dès qu’on a vu ces signaux malsains, et je ne connaissais pas du
tout l’histoire personnelle de ses membres.



<figure>
    <img src="Pictures/ethnocentrisme-perso-autoritaire.png"
         alt="Ethnocentrisme">
    <figcaption>Je pense qu’on pourrait ajouter qu’il y a aussi ethnocentrisme lorsque l’endogroupe veut dominer (et qu’il ne domine pas forcément objectivement un environnement social) ou subordonner un autre groupe (qu’un tiers pourrait ne même pas voir comme différent tant ils semblent proches à de nombreux titres). Les militants ethnocentriques ne vont donc plus chercher à diminuer une domination, ne vont pas remettre en cause la hiérarchie, mais au contraire vont se conformer aux modèles habituels, les reproduire à leur propre niveau, causant de la souffrance. Ils font sans doute cela parce que c’est un moyen d’obtenir enfin de la valeur auprès d’autrui ou pensent que cela va combler les besoins fondamentaux (spoiler&nbsp;: non, c’est cette mécanique qui génère
des sapages, qu’importe qui est placé dans cette hiérarchie illusoire).</figcaption>
</figure>

Dire qu’il y a ethnocentrisme ou identification compartimentée
n’explique pas vraiment pourquoi il y a cet élan d’attaque&nbsp;: certes, ces
mécaniques se font souvent en groupe, sont animées par une dynamique de
groupe, type «&nbsp;bouc-émissaire&nbsp;», certains militants comparent ce genre
de situation au harcèlement scolaire[^55]. Mais ce n’est pas parce que
c’est répandu que c’est «&nbsp;inévitable&nbsp;», que ce serait sans raison ou que
cela s’expliquerait par une prétendue «&nbsp;nature humaine&nbsp;». Quand on
creuse, on trouve des réponses&nbsp;: chez les ados par exemple, l’identité
est en pleine construction et c’est pour cela que des individus vont
parfois se rassembler pour attaquer les élèves perçus comme marginaux.
Cela leur permet de construire/légitimer leur identité à moindres frais,
et de compenser le mal-être général lié à l’adolescence elle-même.
Autrement dit, on voit poindre des solutions lorsqu’on comprend mieux la
cause première&nbsp;: soutenir les ados, créer des climats qui ne soient pas
menaçants, leur montrer des voies de constructions personnelles qui ne
passent pas par la destruction d’autres personnes[^56].

<div style="background: #F5F5F5">
&#8505;  &#8674; L’identification compartimentée peut être totalement connectée à des
stéréotypes ancrés dans la société&nbsp;:

Weinstein et al. (2012) ont postulé
que lorsque des individus grandissent dans des environnements menaçant
l’autonomie, ils peuvent être empêchés d’explorer et d’intégrer
certaines valeurs ou identités potentielles, et en conséquence être plus
enclins à compartimenter certaines expériences qui sont perçues comme
inacceptables.

Comme l’homosexualité est stigmatisée, l’hypothèse des chercheurs a été
que les personnes qui ont grandi dans des environnements sapant ou
frustrant leur autonomie pourraient être plus enclins à compartimenter
leur attirance pour le même sexe autant pour les autres que pour
eux-mêmes, ce qui conduit à des processus défensifs. Les quatre études
des chercheurs ont consisté à voir le soutien parental de l’autonomie
des personnes, prendre note de leur identification sexuelle, puis
mesurer leur orientation sexuelle implicite grâce des tests
d’association implicite. Ces tests se basent sur le temps de réaction,
sans que la personne puisse avoir le temps de mettre en œuvre des
mécanismes de défense.

Résultat, il s’est avéré que plus l’environnement paternel avait été
contrôlant et homophobe, plus il y avait une forte différence entre leur
hétérosexualité annoncée et les résultats aux tests d’association
implicite montrant leur attirance sexuelle pour les personnes du même
sexe. C’est-à-dire qu’ils n’étaient pas cohérents dans la forte
hétérosexualité qu’ils annonçaient alors qu’ils avaient pourtant des
désirs homosexuels. En plus, pour protéger cette identification
compartimentée, ces individus préconisaient plus d’agression envers les
homosexuels.

Autrement dit, cette identification «&nbsp;hétérosexuelle&nbsp;» était fortement
ancrée dans ce qu’ils annonçaient mais elle était fermée et défensive,
parce que l’individu avait des désirs, des besoins sexuels homosexuels
plus forts que ce qu’ils annonçaient. Ce qui entraînait des processus
défensifs, c’est-à-dire qu’il défendait l’identification hétérosexuelle
en préconisant l’agression des homosexuels&nbsp;: on voit là comme une
projection sur la société de leur lutte interne contre leurs propres
désirs et envies.
</div>

Attention, afin d’éviter un malentendu que l’on peut lire ci ou là[^57]
quand on évoque les études portant sur l’homophobie en psycho, précisons
que ce type d’études ne consiste pas à dépolitiser le problème, à tout
plaquer sur l’individu. C’est même l’exact opposé puisque les études
montrent les conséquences de l’environnement culturel, politique et
social sur le développement de la personne&nbsp;; de plus, étudier les
facteurs qui poussent un individu à une agressivité homophobe ne
consiste pas à l’excuser, à lui trouver des circonstances atténuantes&nbsp;:
les sciences humaines et sociales, telles que la socio ou la psycho,
consistent à comprendre, non à excuser (n’en déplaise à Monsieur Valls).
Et lorsqu’on comprend dans le détail, on peut ajuster ces stratégies
militantes, les optimiser, voire tenter de nouvelles actions en fonction
de ces nouvelles informations issues de la recherche.

Cela peut apparaître comme assez contre-intuitif, et très complexe à
démêler/deviner chez autrui puisque dans ces identifications
compartimentées se niche une histoire secrète de l’individu qui se
confronte à des pressions environnementales, puis endosse ces pressions
de la société comme «&nbsp;bonnes&nbsp;» quand bien même son corps et des parties
de lui-même lui signifient que non, qu’au contraire, elles sont sources
de mal-être. Quand on étudie la déshumanisation[^58], on peut tomber
aussi sur ce genre de mécanismes très contre-intuitifs où ce n’est pas
parce que la personne déshumanise une autre personne qu’elle va
recommander de la violence contre lui, mais plutôt parce qu’elle 
**doit** être violente contre lui qu’elle va le déshumaniser. Il y a un
besoin qui commande la violence contre un autre, alors advient ensuite
la déshumanisation qui permet de supprimer toute empathie pour la
personne visée. La grande question est alors&nbsp;: quel est ce besoin&nbsp;? La
réponse varie évidemment selon la situation et des influences distales&nbsp;:
par exemple, si un autoritaire influent interprète une crise économique
comme étant de la faute d’un groupe ethnique particulier qui
s’accaparerait richesses et emplois, alors les gens, par besoin
matériel, peuvent s’accrocher à cette interprétation et s’attaquer à ce
groupe, même si l’interprétation ne tient pas debout. C’est pour cela
qu’en temps de crise on assiste à une plus grande crédulité quant à ce
type d’interprétation discriminante, car fondamentalement les besoins de
la population ayant été sapés ou étant menacés de l’être,
l’interprétation donnant la plus grande promesse de «&nbsp;défense&nbsp;» à
moindre coût recueillera bien plus d’adhésion.

Il y a donc d’abord toujours un besoin chez l’individu, parfois
détourné, parfois extrêmement caché, et donc très difficile à deviner
pour le tiers.

Il se peut aussi que l’individu qui recommande de la violence contre un
autre veuille parfois supprimer quelque chose chez l’autre, parce que
c’est précisément ce quelque chose qu’il veut supprimer en lui&nbsp;; la
vidéo de Contrapoints sur le *Cringe* est assez éloquente à ce sujet.

Non seulement les pratiques déconnantes sont donc le reflet d’un
mal-être (besoins sapés, besoins frustrés que la personne ne s’avoue
pas, motivations de piètre qualité), mais mettent aussi ceux
qui les reçoivent dans un mal-être, et sont du même coup
inefficaces pour l’avancée de la cause qui est décrédibilisée
par la déconnance. De plus, un mouvement militant veut généralement une
transformation des comportements sur le long terme, et non
juste ponctuellement sous la pression d’un ordre (motivation
externe) ou sous la pression sociale (utiliser Firefox un seul jour pour
être perçu comme quelqu’un de bien parce qu’il y a des libristes
chez soi), or c’est précisément ce que génère la militance déconnante.
La militance déconnante, par son comportement, endosse aussi un modèle
de contrôle extrêmement conformiste, conservateur&nbsp;: ce faisant,
le militant déconnant démontre à autrui qu’il ne veut rien
changer de structurel, si ce n’est tenter simplement d’avoir sa part
de domination en prenant le contrôle sur autrui. C’est une dynamique
cohérente lorsqu’on soutient une idéologie autoritaire, mais
c’est incohérent si on vise un changement de paradigme
progressiste et ouvert, puisqu’on répète alors un vieux
paradigme autoritaire. Être «&nbsp;pur&nbsp;» dans ses pratiques ne compense
pas le fait que les autres verront dans l’injonction, l’attaque, la
répétition d’un vieux paradigme contrôlant, et donc n’y
trouveront rien de bien séduisant.



<!------------ notes CHAP 4  ----------->


[^44]: Les individus en orientation contrôlée ont tendance à contrôler
    autrui, à ne voir que les contrôles dans une situation&nbsp;; les
    personnes en orientation autonome ont tendance à voir les
    possibilités, les potentiels d’une situation, les espaces de
    liberté/de créativité possible et ont tendance à nourrir
    l’autonomie, la liberté des autres. L’orientation d’une personne
    dépend de comment la situation actuelle et passée est nourrissante
    ou sapante des besoins (quand bien même on peut être très autonome,
    on peut être en orientation contrôlée dans une situation autoritaire
    par exemple, parce qu’il n’y a aucune place laissée à l’initiative.
    Inversement, on peut être en orientation contrôlée dans une situation
    pourtant très libre, non contrôlante)

[^45]: Pittman, Davey, Alafat, Wetherill, et Kramer (1980)&nbsp;; Lepper &
    Greene (1975)&nbsp;; Plant & Ryan (1985)&nbsp;; Ryan et al. (1991)&nbsp;; Enzle et
    Anderson (1993).

[^46]: Deci et Cascio (1972).

[^47]: Amabile, DeJong, et Lepper (1976)&nbsp;; Reader and Dollinger (1982).

[^48]: Deci, Betley, Kahle, Abrams, and Porac (1981).

[^49]: Anderson et Rodin (1989)&nbsp;; Baumeister and Tice (1985).

[^50]: Anderson, Mancogian, Reznick (1976)

[^51]: Deci (1975)&nbsp;; Lepper, Greene et Nisbett Ross (1975).

[^52]: Dolan et Galizzi (2015).

[^53]: Quantité d’études (Deci et Ryan 2017) montrent que l’école, le
    travail, ou d’autres situations sociales ont tendance,
    majoritairement, à détruire nos motivations intrinsèques. On a donc
    tous probablement connu un nombre plus ou moins grand de sapages de
    nos motivations intrinsèques.

[^54]: La motivation introjectée est liée à une baisse de vitalité, une
    augmentation de l’anxiété, plus de sentiments de honte, de
    culpabilité, parfois à la dépression, à la somatisation et à une
    faiblesse face à la manipulation Vallerand et Carducci (1996)
    Koestner, Houlfort, Paquet et Knight (2001) Ryan et al. (1993) Assor
    et al. (2004) Moller, Roth, Niemiec, Kanat-Maymon et Deci, (2018).

[^55]: Pauline Grand d’Esnon, «&nbsp;[Pureté militante, culture du ’callout’&nbsp;: quand les activistes s’entre-déchirent](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html)&nbsp;», *Neonmag*, 13/02/2021.

[^56]: ça peut passer par la pratique d’un sport, l’apprentissage des
    compétences socio-émotionnelles, une éducation systémique sur la
    façon de créer son bien-être, comprendre son mal-être (psychologie,
    sociologie), une éducation basée sur la coopération et le soutien
    entre personnes, un enseignement des sciences humaines et sociales
    dès le collège, etc.

[^57]: Comme ici&nbsp;: Maëlle Le Corre, «&nbsp;[Pourquoi il faut en finir avec le cliché du «&nbsp;mec homophobe qui est en réalité un gay refoulé](https://www.madmoizelle.com/homophobe-gay-refoule-1115343)&nbsp;», *Madmoizelle.com*, 30/03/2021.

[^58]: Cf Semelin (1994&nbsp;; 1983&nbsp;; 2005&nbsp;; 1998)&nbsp;; Straub (2003)&nbsp;; Hatzfeld (2003)&nbsp;; Terestchenko (2005).







<!------------ CHAP 5 D’autres causes du militantisme déconnant ----------->



## D’autres causes du militantisme déconnant

### Le surmenage

Quand on est surmené, on essaye de régler les problèmes au plus vite
pour en traiter d’autres plus urgents, donc il est totalement logique
qu’on en vienne à être plus sec dans notre ton, qu’on ait plus tendance
à l’injonction pour obtenir de l’autre un comportement immédiat afin
qu’il cesse de nous solliciter. Le problème ce n’est ni nous, ni l’autre
qui sollicite ou fait un truc pour lequel on va l’injonctiver en
réaction, mais bien la situation de surmenage. Or, c’est extrêmement
courant en militance, parce que les mouvements n’ont pas souvent les
moyens de gérer tout ce qu’il y a à gérer, parce que la militance mène à
affronter des situations particulièrement surmenantes, stressantes,
parfois oppressantes et violentes. Et même lorsque la situation
surmenante est loin derrière, il y a toujours cette menace qu’elle
revienne sous peu, d’autant qu’elle laisse souvent des traces. En
conséquence, notre cerveau maintient ce mode «&nbsp;sous tension&nbsp;» par
prévention, parce que cela s’est avéré être une manière efficace de
gérer le moment tendu.

Autrement dit, dans ce cas de figure ce n’est ni la faute du militant,
ni de l’allié qui faute ou qui aurait un comportement qui va générer une
critique, mais bien un problème situationnel qui demande des solutions
organisationnelles. La situation d’urgence, de surmenage peut être
inévitable, en ce cas, l’idéal est d’avoir un mode de fonctionnement
préétabli pour ces situations particulières, et d’autres modes de
fonctionnement pour les autres situations. Ce n’est pas forcément
incohérent d’avoir un mode plus «&nbsp;hiérarchique&nbsp;» dans une situation de
forte confrontation avec l’adversaire, avec des règles plus serrées,
parce que la violence ou les risques peuvent obliger à cela, et parfois
le rôle donné à chacun dans un groupe peut avoir des effets
protecteurs&nbsp;; parmi les hackers, j’ai pu voir à l’œuvre à la fois un
mode quasi-militaire lors d’opérations risquées impliquant beaucoup de
monde, avec des instructions très strictes qui ne laissaient pas de
place à de l’initiative personnelle, parce que c’était à la fois le moyen de
mener à bien l’opération et de protéger tout le monde de
risques très concrets. Mais dès que l’opération était terminée,
l’autogestion sans chef, do-ocrate (le pouvoir à celui qui fait/initie
un projet), anti-autoritaire, reprenait le dessus pour fomenter de
nouvelles opérations. Il s’agit de pouvoir switcher, être flexible dans
l’organisation et dans les modes d’agir afin de coller aux besoins
particuliers de la situation, et ne pas rester en mode «&nbsp;menaces&nbsp;»
lorsque celles-ci ne sont pas présentes.

Quoi qu’il en soit, le surmenage et les dérives que cela entraîne ne
peuvent être résolus que par des modes d’organisation qui sont pensés en
fonction des situations. Cela n’est pas un problème qui peut être résolu
en se focalisant sur un individu «&nbsp;fautif&nbsp;».

### Le manque d’information

Pour reprendre l’exemple de «&nbsp;vous connaissez PeerTube&nbsp;?&nbsp;» qu’on a eu
des centaines de fois, c’était saoulant non pas parce que les gens
l’étaient, mais parce qu’il leur manquait l’information que nous étions
déjà partisans de PeerTube, que nous avions déjà nos vidéos sur des
instances, que des dizaines d’individus avant lui n’arrêtaient pas de
nous le dire, et qu’ils ne devinaient pas eux-mêmes qu’il leur
manquait ces informations. Et si nous l’avions répété sans cesse, nous
aurions été nous-mêmes saoulants, c’est pourquoi nous ne l’avons pas
fait. J’ai vu aussi le même genre de problème chez des individus
participant à des formes de *cancel culture*[^59] -- malgré eux&nbsp;: ils se
permettaient une certaine agressivité se pensant seuls dans les
commentaires à avoir ce ton et ne se rendant pas compte qu’ils
contribuaient à rejoindre une meute qui attaquait déjà de toutes parts
sur le même ton.

Avant de conseiller, ordonner, critiquer, s’énerver contre quelqu’un ou
un groupe, on pourrait tenter de s’informer au préalable des positions
de la personne qu’on cible, en regardant ce qu’elle a pu déjà répondre
par le passé à ce sujet, si elle a parlé de ses positions quelque part,
si elle n’a pas déjà fait ce qu’on voudrait qu’elle fasse, etc. Parfois,
cela suffira à combler le manque d’informations et il n’y aura pas
besoin d’interpeller la personne (par exemple, on pourra voir qu’elle
connaît déjà PeerTube ou qu’elle a déjà exprimé son choix pour/contre en
public).

Il s’agirait avant toute interaction de partir du principe qu’on ne sait
pas d’emblée les positions des personnes, leur savoir ou leur ignorance
d’un sujet, mais d’enquêter avant.

Cela peut fonctionner en situation où l’on initie l’interaction avec un
autre sur le net, comme dans une situation où l’on est attaqué par un
autre. Même si on repère que l’autre veut par exemple nous humilier ou
nous écraser, on peut partir du principe que ce n’est peut-être pas ça,
et tout simplement poser des questions pour bien comprendre sa
position[^60]. Par exemple «&nbsp;*Vous me dites que d’avoir mis le mot
«&nbsp;bonheur&nbsp;» dans ce titre est odieux et insupportable, quel est l’élément
associé à bonheur qui vous parait odieux&nbsp;?*&nbsp;» et on cherche à comprendre
ce qui a éveillé le sentiment négatif chez l’autre, on enquête sans
jugement ni défensivité. Cela peut lever pas mal de malentendus et
pacifier l’échange.

Sur Internet, le manque d’informations c’est aussi l’absence de langage
non verbal (absence du ton de la voix, des mimiques de visage, des
gestes du corps, etc.). Ainsi, on a tous un déficit d’informations
parfois énorme sur l’état émotionnel dans lequel a été posté un message
et dans quelle visée. Et encore une fois, on oublie totalement qu’il
nous manque quantité d’informations pour interpréter ce message parce
qu’IRL, lorsqu’on est neurotypique, on a l’habitude d’avoir toutes ces
informations automatiquement sans qu’on en ait conscience. Sur la toile,
on va alors avoir le même réflexe et interpréter le message
automatiquement, en voyant une offense dans une ironie, en voyant de
l’ironie dans un message pourtant sérieux, etc. Pour pallier ce manque
d’informations non verbales, on va se concentrer sur d’autres indices
tels que la ponctuation, y plaquant un sens qui n’est pourtant pas celui
du locuteur. D’autant que l’usage et la connotation des ponctuations
varient selon des facteurs socio-culturels, tels que l’âge de la
personne&nbsp;: les boomers pourront avoir tendance par exemple à terminer
tous leurs tweets d’un point, selon l’usage «&nbsp;académique&nbsp;» qu’ils ont
profondément intériorisé, sans exclamation ni smiley[^61], ce qui pourra
donner l’impression, selon le propos tenu, à un ton brutal, voire un
mode passif-agressif, alors qu’il s’agissait parfois tout simplement
d’une volonté de soigner son écriture, sans froisser son interlocuteur.
Même chose pour l’usage des points de suspension dans un message, qui
pourra être utilisé différemment et suggérer de multiples
interprétations contradictoires… On se focalise sur ces petits
détails, car on cherche une substitution à ce langage non-verbal qui
nous manque cruellement. S’ensuivent donc quantité de malentendus de
toutes parts.

Là encore, on peut prévenir la situation en étant très explicite
lorsqu’on s’exprime, avec tout ce qu’on a disposition (smiley,
formulation de politesse, soin aux styles de la phrase, mots, expression
explicite de son émotion/son état/ses buts, etc.).

Ou encore lorsqu’on est l’interlocuteur, demander des précisions sur le
message, poser des questions jusqu’à être sûr de bien comprendre, avant
de juger son but. Ça peut paraître long dit comme ça, mais en fait poser
une question ce n’est parfois qu’une seule phrase. Et parfois la réponse
suffit à se faire une idée.


<figure>
    <img src="Pictures/Ambassadeur.jpg"
         alt="Abassadeur">
    <figcaption>«&nbsp;Ambassadeur&nbsp;: Honte et fierté mélangées. Nos ennemis nous ont appelés «&nbsp;tanks vivants&nbsp;». Ainsi que par des noms moins flatteurs.&nbsp;» On peut même s’amuser à utiliser la méthode Elcor (dans les jeux Mass Effect, les Elcors sont des êtres qui ne peuvent partager une communication non-verbale avec les autres espèces, ni même faire transparaître leurs émotions dans leurs voix&nbsp;; pour pallier ce manque, ils commencent systématiquement leur propos par un mot qui donnera la bonne teinte émotionnelle à leur discours). D’autres exemples <a href="https://www.youtube.com/watch?v=8eJt1GBon7o">ici</a>.</figcaption>
</figure>

### La réaction à la notoriété bizarre du Net&nbsp;: les relations parasociales

C’est un terme qui a été formulé en 1956 par Horton et Wohl pour décrire
les relations unilatérales d’un·e artiste avec son public&nbsp;: les
spectateurs peuvent se sentir comme amis avec ceux-ci, donc croire tout
connaître de lui, alors qu’en fait non. Aujourd’hui, ce type de
relations est encore plus répandu parce qu’on peut tous être cet
«&nbsp;artiste&nbsp;» qui envoie ou partage du contenu avec une communauté qui le
suit.

D’une part, la personne qui une petite ou grande notoriété sur le Net ne
sait rien de vous et ne peut rien déduire de votre comportement habituel
(par exemple, elle ne peut pas savoir que lorsque vous employez des
injures, c’est du second degré ou une marque d’amitié&nbsp;; elle ne sait pas
que vous êtes peu versé dans les formules de politesse mais néanmoins
cordial), elle peut donc difficilement interpréter des remarques qui
seraient à double sens, encore plus sans avoir accès à votre langage non
verbal pour comprendre. Le militant déconnant peut croire que cette
personne à notoriété va parfaitement le comprendre, qu’il est sympa
d’office, qu’importe le style du message, parce que lui, il la connaît
bien mais oublie qu’elle, elle ne le connaît pas du tout. Et là peuvent
se créer de très forts malentendus.

D’autre part, en tant que spectateur, bien qu’on ait ce sentiment de
familiarité avec la personne à notoriété, on ne la connaît pas du tout&nbsp;:
on ne peut pas savoir si elle est en dépression ou si elle traverse une
phase difficile, elle peut très bien partager quelque chose de sombre
tout en étant dans une situation joyeuse dans son quotidien, tout comme
partager de la joie en broyant du noir. Là encore, avant d’entamer une
démarche qui risque potentiellement d’être dure à digérer pour l’autre,
on peut poser des questions, «&nbsp;tâter le terrain&nbsp;» pour savoir si c’est
le bon moment ou non de parler de telle chose&nbsp;; on peut aussi se
rappeler qu’on ne connaît la personne qu’à travers son
travail/œuvre/partage, pas sa vie tout entière qui peut être
radicalement différente. Même des vlogs réguliers qui pourtant
renseignent sur la vie de la personne sont sélectifs, ne sont qu’un
aperçu de sa vie, ce qu’elle accepte de montrer. Tout comme on ne peut
déduire le bien-être d’un vendeur de sandwichs à la qualité dudit
sandwich (qui peut par exemple avoir été cuisiné sous une pression
énorme), on ne peut pleinement déduire l’état d’esprit d’un partageur à
son seul partage. Pour connaître un peu le milieu, je dirais que lorsque
vous vous adressez un partageur/créateur sur le Net, il est probable
qu’il est en dépression, en burn-out ou surmené, qu’importe la vivacité
dont il peut faire preuve dans ses œuvres. Il serait plus prudent
d’éviter de partir du principe qu’il peut encaisser toutes les
récriminations.

L’autre aspect de cette relation parasociale, c’est que parfois, les
spectateurs confondent ces petites célébrités du Net avec les célébrités
classiques&nbsp;: c’est-à-dire qu’ils partent du principe que la notoriété
est accompagnée d’un statut supérieur (plus de pouvoir, plus de
possibilités, plus d’argent, plus de moyens, plus d’influence, etc.),
donc qu’elles auraient en quelque sorte pour devoir d’utiliser ce
trop-plein de privilèges qu’il leur serait offert, notamment pour vanter
ou exercer une pureté militante. Or, même des gens qui ont une forte
audience sur le Net peuvent n’avoir aucun privilège matériel par rapport
au spectateur moyen, peuvent toujours être salarié smicard, au chômage,
voire dans des situations de grande pauvreté, de sérieuses difficultés. Et
vous n’en saurez généralement rien.

Cependant je comprends, ça peut être trompeur qu’une petite célébrité
sur le Net en galère au quotidien puisse avoir le même nombre de
followers[^62] qu’une petite célébrité de la télévision qui elle, peut
avoir des moyens plus importants, le soutien d’une structure, des
relations qui la mettent à l’abri, etc. Bref, la notoriété du Net doit
être déconnectée dans nos représentations des privilèges, car la
notoriété sur la toile n’est pas synonyme d’avantages matériels ou
sociaux[^63].

### La suspicion d’infiltrés/d’ennemis

L’infiltration dans un groupe militant est malheureusement une pratique
existante, d’autant plus sur le Net où il est souvent facile de
rejoindre le Discord d’un autre groupe militant pour glaner des
informations ou pour troller en interne (ce que l’on peut retrouver par
exemple dans des groupes politiques fortement engagés, notamment entre
fascistes et anti-fascistes). La suspicion d’infiltrés (ou la présence
effective de ceux-ci) peut nous faire nous méfier des alliés,
des spectateurs, et nous mettre en mode paranoïa. C’est un cercle vicieux
terrible, et j’avoue que je n’ai pas la solution contre cela d’autant
que je l’ai malheureusement déjà vécu dans certains mouvements (présence
réelle d’infiltrés professionnels, confirmée par des leaks découverts
plus tard et publiés dans certains médias). L’idée serait peut-être de
se concentrer davantage sur les actions qui sont proposées, de les évaluer
au regard du mouvement et des buts de celui-ci, ce qui
permettrait d’éviter des catastrophes. Les infiltrés ou individus
malveillants auront tendance à diviser, créer des conflits internes,
proposer de s’attaquer aux alliés et spectateurs, chercher à obtenir des
postes à pouvoir de décisions, épuiser les éléments les plus doués,
proposer des actions honteuses/inefficaces qui ne permettent pas de se
confronter à l’adversaire. Donc, ce n’est pas tant qu’il faudrait le
traquer pour le virer, mais davantage prendre soin des alliés, des
spectateurs car c’est une politique plus puissamment établie&nbsp;: ces
projets saboteurs ne seront alors pas suivis parce qu’ils apparaîtront
incohérents, inadaptés.

La suspicion qu’il y ait des infiltrés ou qu’untel ait des projets
malveillants ou potentiellement destructeurs pour le groupe (par exemple,
un membre qu’on pense vouloir nuire au mouvement suite à un conflit mal
résolu en interne, ce qui arrive assez fréquemment&nbsp;: tout militant
d’expérience aura sans doute en mémoire l’exemple d’un ancien camarade
qui, sous l’effet du ressentiment, a pu se mettre à saper activement un
mouvement ou à vouloir nuire à ses membres) peut également n’être qu’une
simple suspicion qui s’avérera plus tard infondée, et ça serait dommage
que l’activité militante soit détournée juste parce qu’on est en mode
méfiance et qu’on a peur des menaces internes. Cependant, là aussi, je
pense qu’on peut tenter d’éviter les problèmes en se concentrant sur les
actions au cœur du mouvement, celles qui sont les plus concrètes et les plus
cohérentes.

<!------------ notes CHAP 5 ----------->



[^59]: «&nbsp;Cancel culture&nbsp;»&nbsp;: «&nbsp;pratique qui consiste à dénoncer des
    individus (ou structures) dans le but de les ostraciser&nbsp;». Plus
    d’infos sur [Wikipédia](https://fr.wikipedia.org/wiki/Cancel_culture), ou sur [Neonmag](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html).

[^60]: Ici, je me base sur les pratiques et méthodes de Carl Rogers,
    psychologue humaniste qui visait l’empuissantement et
    l’autodétermination des personnes, tant dans des contextes
    thérapeutiques, de groupes aux buts divers (académique, religieux,
    politique à visée de résolution de conflits, etc.). Ces écrits sont
    particulièrement accessibles, y compris pour les personnes non
    formées à la psychologie, notamment ses ouvrages *Liberté pour apprendre*, *Le développement de la personne*.

[^61]: Si vous êtes acolyte des illustres de l’académie française, vous
    devez dire «&nbsp;binette&nbsp;» ou «&nbsp;frimousse&nbsp;» pour désigner [un smiley](https://www.academie-francaise.fr/smiley).
    

[^62]: Follower = «&nbsp;acolyte des illustres&nbsp;» si votre allégeance va à
    l’Académie française, quoique je pense qu’elle se fout un peu de la
    gueule des personnes utilisant Internet, voire de la population tout
    court, quand on voit qu’elle a rejeté l’usage commun du masculin
    pour «&nbsp;covid&nbsp;» à la grande joie des Grammar Nazis qui auront une
    occasion supplémentaire de corriger leurs interlocuteurs.
    Voir [l’explication de l’académie sur cette traduction](https://www.academie-francaise.fr/followers)&nbsp;; on pourrait dire «&nbsp;abonnés&nbsp;» mais il me semble que cela reste trop associé à l’image de
    quelqu’un qui a acheté un abonnement pour accéder à un contenu. Le
    terme «&nbsp;adepte&nbsp;» est utilisé aussi par bing, mais là encore il me
    semble que cela nous renvoie à une image erronée du follower (qui
    n’est pas forcément partisan du contenu suivi, encore moins fidèle à
    lui comme il le serait d’une religion).

[^63]: Une étude sur les vulgarisateurs le montre bien&nbsp;: «&nbsp;Frontiers. French Science Communication on YouTube&nbsp;: A Survey of Individual and Institutional Communicators and Their Channel Characteristics    Communication&nbsp;», [frontiersin.org](https://www.frontiersin.org/articles/10.3389/fcomm.2021.612667/full)&nbsp;;
    ou en vidéo&nbsp;: [Analyse des vulgarisateurs scientifiques
    sur Youtube](https://www.youtube.com/watch?v=T6UzuDaPvqA)&nbsp;; ou dans ce
    thread&nbsp;: «&nbsp;On a analysé plus
    de 600 chaînes et 70 000 vidéos de vulgarisation scientifique en
    français, et complété cette analyse par un sondage auprès de 180
    youtubeurs. Nos résultats (avec \@SciTania \@MasselotPierre
    \@tofu89) viennent d’être publiés dans Frontiers in
    communication&nbsp;», [Stéphane Debove sur Twitter](https://twitter.com/stdebove/status/1394322535244869636)&nbsp;; par exemple seul 12 % des vulgarisateurs (sur 600 chaînes
    françaises) gagnent plus de 1000 euros par mois, 44 % ne gagnent
    rien du tout.







<!------------ CHAP 6 Que faire à la place de la déconnance&nbsp;? ----------->




## Que faire à la place de la déconnance&nbsp;?

On a déjà pu apercevoir dans les points précédents qu’on se mettait à
avoir des pratiques déconnantes non pas parce qu’on est persuadé que ce
sont de bonnes pratiques, mais davantage malgré nous, parce que nos
propres besoins sont sapés (par exemple faute d’avoir son autonomie
comblée, on tente de contrôler l’autre, ce qui nous donne une
satisfaction ponctuelle de notre besoin de compétence), parce que c’est
le modèle de fonctionnement majoritaire dans nos environnements sociaux,
parce qu’on manque d’informations, qu’on est surmené, etc.

### Viser les besoins fondamentaux et vivre sa motivation intrinsèque

La solution est donc pour casser ce cercle vicieux est de commencer à
*nourrir les besoins fondamentaux* à travers nos activités (tous les
conseils dans les cadres jaunes des schémas précédents), tant
les nôtres que ceux des autres en même temps. S’ensuivront des
motivations de plus haute qualité pour l’activité, et celles-ci vont
aussi nourrir en retour nos besoins en un cercle cette fois-ci vertueux.
Et quand on aime profondément une activité, on cherche à en décupler le
plaisir, donc on tente de la partager, les personnes aimant partager
des émotions plaisantes écoutent et sont à leur tour entraînées dans une
motivation à cette activité. Le truc serait juste de *vivre et de communiquer pleinement sa motivation intrinsèque* pour telle activité.

Cette transmission de la joie et du vécu positif pour une activité qu’on
a avec une motivation intrinsèque (comme peuvent l’être tous les loisirs
actifs, les disciplines qui ont pu passionner des personnes dans le
monde) peut se faire dès qu’on lève toute crainte quant au jugement de
celles et ceux qui pourraient observer ce vécu joyeux, crainte qui peut
potentiellement s’effacer lorsqu’on est concentré dans l’activité
elle-même. Généralement les gens perçoivent la passion, ressentent les
émotions positives sincères lorsqu’elles sont explicitement vécues, de
façon authentique (par exemple, on ne se forcerait pas à transmettre la
joie de faire ceci, on serait juste effectivement joyeux de faire
cela)[^64]. La plus grande difficulté de ce partage de motivation
intrinsèque réside dans la crainte des atteintes à la proximité
sociale&nbsp;: être authentique, c’est être à nu, donc s’exposer
potentiellement au jugement d’autrui, à son mépris, à son indifférence,
à sa future ostracisation. On peut donc avoir des réticences à s’exposer
sincèrement, tout particulièrement lorsqu’on a déjà vécu des situations
de forte indifférence ou de mépris alors qu’on était pleinement
authentique et bienveillant. Cela demande alors un même type de courage
qu’un saut du plongeoir, on ne peut que se jeter à l’eau, s’immerger
(ici dans le sujet, en vivant totalement avec lui), et nager jusqu’à
l’atteinte du but. La crainte du regard d’autrui, son jugement potentiel
est mis de côté, on se concentre sur son rapport authentique à sa
passion. Vivre pleinement sa motivation intrinsèque et l’exposer n’est
pas tant un effort, un travail, mais plutôt une immersion de l’attention
qui est telle que les craintes liées au sapage de la proximité sociale
sont pour le moment comme hors sujet.

### Viser les motivations extrinsèques intégrées

Cependant, on sait aussi tous que la vie n’est pas forcément composée
d’activité attractive. Changer la litière du chat, descendre les
poubelles, suivre le Code de la route… Je ne connais personne qui
ait de motivation intrinsèque pour ces activités, et c’est tout à fait
normal parce que celles-ci peuvent avoir intrinsèquement des stimuli
aversifs (l’odeur de la litière, des poubelles), demander des actions
ennuyeuses qui n’apportent rien (attendre au stop n’est en rien une
expérience qui nous apprend quelque chose), etc. De même, sans
motivation intrinsèque, certaines activités militantes peuvent être tout
aussi répulsives en premier lieu.

Toutefois on peut avoir des *motivations extrinsèques intégrées* pour
ces actions répulsives, et celles-ci sont puissantes, durables sur le
long terme et rendant l’acte moins pénible ou coûteux en efforts. On
change alors la litière pour maintenir un foyer plus agréable pour ses
habitants (y compris pour le chat qui vous remerciera en cessant de vous
faire découvrir au petit matin de petites surprises puantes sur le sol du
salon), on suit le Code de la route parce qu’on ne veut pas causer
d’accident, on trie ses poubelles correctement pour faciliter le travail
des éboueurs et de tous ceux qui travaillent sur le traitement des
déchets.

Et comme c’est particulièrement intégré en nous, ça ne nous coûte rien
de le faire, on ne rechigne pas, on n’a plus besoin d’y penser, on n’a
pas de crainte d’être jugé, on ne sent pas de menaces, on n’agit pas par
injonction.

L’autre avantage de cette motivation à régulation intégrée, c’est la
résistance aux tentatives de manipulation/d’influence néfaste&nbsp;: par
exemple, une personne à motivation intégrée pour le tri triera non
seulement tout le temps sans que personne n’ait à lui ordonner quoique
ce soit, sans qu’il y ait une seule pression, mais plus encore elle ne
sera pas influencée par les arguments tentant de la convaincre que c’est
pathétique de trier, et elle continuera son comportement. On a donc là
une motivation très puissante, potentiellement préventive face aux
menaces et aux tentatives d’influence.

Mais comment transmettre ça à un autre, sans être injonctif, saoulant,
culpabilisant&nbsp;? 

<div style="background: #F5F5F5">
Une expérience de la théorie de l’autodétermination est
assez éloquente à ce sujet&nbsp;: 

&#8505; &#8674;  Koestner et coll. (1984). Au travers d’une expérience sur la
peinture avec des enfants, il a été testé différentes façons de
présenter une règle consistant à respecter la propreté du matériel. Pour
soutenir l’autonomie malgré une imposition de règles, il a été vu qu’il
fallait présenter les choses ainsi&nbsp;:

- *Minimiser l’usage d’un langage contrôlant* («&nbsp;tu dois&nbsp;» «&nbsp;il faut&nbsp;»...),
- *Reconnaître le sentiment* des enfants à ne pas vouloir être soigneux avec les outils,
- Fournir aux enfants une *justification* de cette limite/règle (c’est-à-dire expliquer pourquoi on a voulu que les outils restent propres).

En présentant ainsi les limites de façon non contrôlante, la motivation
intrinsèque des enfants pour la peinture était préservée et beaucoup
plus haute que dans un cadre contrôlant (c’est-à-dire avec juste l’ordre
de ne pas salir les outils, sans justification ni reconnaissance du
sentiment de l’enfant).
</div>

Si on transpose cela à l’acte militant, vous avez plus de chances de
réussir à transmettre un changement d’habitude, une nouvelle pratique
qui supplante une ancienne, une alternative, en n’étant pas contrôlant
dans son langage&nbsp;: on supprime l’impératif, «&nbsp;il faut&nbsp;» «&nbsp;tu dois&nbsp;». À
la place, on peut mettre «&nbsp;on peut&nbsp;», «&nbsp;il est possible de&nbsp;»&nbsp;; je trouve
que le conditionnel est aussi très doux pour montrer des possibilités.
Et un discours non injonctif qui connote l’ouverture à des possibilités
est un discours qui permettra d’éviter des comportements réactants.

&#10083; *Reconnaître les émotions* d’autrui, c’est soit se mettre en empathie
cognitive avec l’autre (par exemple, imaginer ce que peut ressentir un
militant antivax), soit essayer de comprendre ses émotions en l’écoutant
activement, sans jugement.

Le thread suivant explique formidablement bien comment on peut communiquer avec
«&nbsp;l’adversaire&nbsp;» à sa cause d’une façon qui respecte son autonomie, ses
besoins (ici c’est la personne antivax, mais ça pourrait concerner un
autre sujet, la méthode d’écoute des émotions et besoins serait tout
aussi pertinente).

> **[Thread] Comment parler à une personne Antivax&nbsp;?**
> - (le) Deuxième Humain
> - @DeuxiemeHumain
> 
> 
> &#10093; J’ai vu plein de gens parler de leurs proches qui veulent pas se faire vacciner / ont peur des vaccins / pensent qu’il faut pas faire confiance à la médecine,
(4:24 PM · 21 mai 2021 · Twitter)
> 
> &#10093;  et qui aimeraient bien les faire changer d’avis ou les pousser à se faire vacciner (pour rester en vie), donc voici quelques astuces pour y arriver&nbsp;:
> 
> &#10093; Précision&nbsp;: tout ce dont je vais parler ici concerne les proches / personnes qu’on connait plutôt bien.
> 
> &#10093; Malheureusement  faire changer d’avis un·e inconnu·e sur twitter, surtout un sujet aussi chargé émotionnellement, ce n’est souvent pas un objectif réaliste. Mais si tu as de la patience et du temps, tu peux toujours essayer&nbsp;:)
> 
> &#10093; Le plus important c’est de ne pas prendre les personnes antivax pour des idiotes. C’est pas parce qu’on est antivax qu’on est plut bête qu’un·e autre.
> 
> &#10093; Et même si c’était le cas&nbsp;: se faire prendre de haut ça n’a jamais fait évoluer personne. Et ça n’a jamais n’a définitivement jamais fait évoluer personne de façon saine.
> 
> &#10093; Ne monopolisez pas la parole&nbsp;: c’est important d’avoir une vraie discussion où vous écoutez sincèrement la personne en face, sinon elle ne va pas avoir envie de vous écouter en retour et vous risquez de parler dans le vide.
> 
> &#10093; Il faut essayer d’avoir un véritable échange, ne placez pas uniquement les sources avec les faits ou statistiques sur les vaccins qui montrent que c’est mieux de se faire vacciner comme si vous étiez en train de jouer aux échecs.
> 
> &#10093; Écoutez. Écoutez. Écoutez. Personne ne «&nbsp;naît&nbsp;» antivax. Il y a toujours une raison derrière.
> 
> &#10093; Ça peut être une histoire personnelle, une peur des «&nbsp;élites&nbsp;», une peur ou une incompréhension de la science derrière les vaccins, une perte de confiance envers la médecine, des positions politiques ou religieuses…
> 
> &#10093; Et si la personne en face ne rentre pas dans les détails, posez des questions. Intéressez-vous sincèrement à la personne face à vous et aux raisons qui ont poussé à être contre les vaccins.
> 
> &#10093; Ça vous aidera à mieux l’aider à comprendre ce sujet et aussi à mieux la comprendre de manière générale dans la vie (et c’est toujours cool d’être plus proche de ses proches).
> 
> &#10093; Tant que vous y êtes&nbsp;: parlez de vous aussi. Pourquoi est-ce que vous êtes d’accord-vax&nbsp;? (Je viens d’inventer ce mot, j’en suis très fier)
> 
> &#10093; Est-ce que vous avez eu des doutes à certains moments&nbsp;? Comment avez-vous fait pour vous renseigner&nbsp;? Qu’est-ce qui vous a fait vous décider&nbsp;? Pourquoi vous faites-vous vacciner&nbsp;?
> 
> &#10093; Je passe beaucoup de temps dessus, parce que c’est très important d’avoir une vraie discussion et de ne pas venir avec son Powerpoint, balancer plein de chiffres ou de noms qui font sérieux puis repartir direct.
> 
> &#10093; Et la deuxième étape, après avoir pris le temps d’écouter et de parler posément avec la personne antivax, c’est d’apporter des réponses ou des solutions à ses problèmes.
> 
> &#10093; La personne que vous souhaitez convaincre ne fait pas confiance aux positions du gouvernement parce que, honnêtement, c’est des positions qui changent toutes les 2 semaines c’est chelou&nbsp;?
> 
> &#10093; Parlez-lui des recommandations de l’OMS-qui ont d’ailleurs plusieurs fois été gentiment ignorées par le gouvernement.
> 
> &#10093; Vous êtes face à quelqu’un qui a peur des effets secondaires potentiels&nbsp;? Regardez ensemble quels sont les effets secondaires potentiels des vaccins et les effets primaires du Covid (Spoiler&nbsp;: le Covid a l’air franchement plus violent).
> 
> &#10093; Et vous pouvez aussi regarder la liste d’effets secondaires de médicaments courants ou qu’elle prends, pour lui montrer que ça n’est pas si différent et qu’il s’agit de cas rares. Ils existent, mais sont rares.
> 
> &#10093; Quelqu’un ne veut pas se faire vacciner parce que «&nbsp;c’est chiant je sais pas comment faire avec internet et tout&nbsp;»&nbsp;? Vous pouvez l’aider à prendre rendez-vous, le faire pour elui voire même l’accompagner si vous êtes disponible&nbsp;!
> 
> &#10093; Rien que proposer de prendre des rendez-vous au même moment ça peut motiver certaines personnes qui n’étaient pas sûres&nbsp;: avec l’effet de groupe on se dit «&nbsp;allez, tant qu’on y est&nbsp;!&nbsp;» et c’est toujours plus rassurant d’y aller à plusieurs, surtout avec des proches.
> 
> &#10093; Storytime&nbsp;: Quelqu’un dans ma famille (anonymysé·e pour des raisons d’anonymat) vient d’un pays où il y a littéralement eu des tests de vaccins et médocs faits sur la population «&nbsp;pour voir si ça fonctionne bien avant de les envoyer dans les pays riches&nbsp;».
> 
> &#10093; Allez savoir pourquoi, cette personne n’a pas super méga confiance en la vaccination contre le Covid du coup. Et bah on va se faire vacciner avant elui, comme ça iel pourra voir si on va bien et aller se faire vacciner en ayant confiance.
> 
> &#10093; Le plus important c’est d’écouter les besoins ou peurs des personnes et les aider à les surmonter -et ça, quels que soient ces problèmes et ces peurs, même si elles nous paraissent ridicules&nbsp;: un peu de compassion punaise&nbsp;!
> 
> &#10093; Félicitations, vous êtes arrivé·es à la fin de ce thread&nbsp;! Pour fêter ça vous pouvez le RT où l’envoyer à des gens que ça pourrait aider. Et pour me soutenir vous pouvez aller sur https://utip.io/vivreavec , ça nous soutient Matthieu et moi&nbsp;!
> 
> --- [Source Twitter](https://twitter.com/DeuxiemeHumain/status/1395747396630589443).

&#9874;  Concernant la *justification rationnelle* à apporter sur «&nbsp;pourquoi&nbsp;»
selon le militant il faudrait changer de comportement (ne plus employer
tel mot, tel logiciel&nbsp;; porter le masque, se faire vacciner, ne pas
croire ceci, etc.), des méta-analyses révèlent celles les plus
convaincantes&nbsp;:

<div style="background: #F5F5F5">
&#8505; &#8674;  Steingut, Patall et Trimble (2017) ont fait une méta-analyse de 23
expériences portant sur le soutien à l’autonomie qui fournissait une
explication ou une justification rationnelle sur la tâche à faire. Ils
ont découvert que cette explication augmente la valeur perçue de la
tâche, mais peut parfois générer un effet négatif sur le sentiment
d’être compétent. En effet, toutes les explications n’ont pas la même
valeur autodéterminante, et peuvent être classées en 3 types&nbsp;:

- contrôlantes&nbsp;: le comportement est dit important pour des raisons
externes, tel que «&nbsp;cela vous rapportera de l’argent, une promotion&nbsp;»,
ou concernant l’apparence physique ou canalisant le sentiment de
culpabilité&nbsp;;
- autonomes&nbsp;: le comportement est dit important pour soi, ses valeurs
personnelles, son développement personnel «&nbsp;cela améliorera votre
mémoire/votre indépendance/votre esprit critique...&nbsp;»&nbsp;;
- prosociales&nbsp;: le comportement est dit important pour autrui, «&nbsp;cela
va apporter du confort et du bien-être à vos proches/aux personnes
présentes&nbsp;».
</div>

*C’est lorsque l’explication ou la justification est prosociale que le comportement est ensuite le plus efficace, avec une meilleure motivation autonome, un meilleur engagement.*

Autrement dit, l’humain étant un animal social, il est davantage motivé
de suivre un comportement qui va clairement montrer que ça aide un autre
humain&nbsp;; ça le motive plus que les récompenses, l’argent, l’évitement de
la culpabilité, ou la croissance de ces capacités ou compétences
personnelles. À mon avis, cette justification prosociale, pour être
transmise efficacement, pourrait être formulée au plus concret et
proximal possible&nbsp;: dire que le tri des déchets va sauver l’humanité ne
sera pas une justification qui motivera le locuteur à changer son
comportement, par contre dire que ça facilite le travail de l’éboueur
qu’on peut croiser de temps en temps dans sa rue sera bien plus
efficace. Parce que la réussite «&nbsp;sauver le monde&nbsp;» est à la fois un
défi trop important, quand bien même il serait réussi, il n’y aurait pas
de feedback de réussite direct («&nbsp;ah vous êtes le type qui avait eu une
pratique écologique parfaite et depuis nous n’avons plus de pollution,
merci beaucoup&nbsp;!&nbsp;»)&nbsp;; alors que voir les éboueurs de bonne humeur dans
la rue parce qu’il n’y a pas de problème avec les poubelles et les
déchets tels que les gens en ont pris soin, est un feedback directement
visible, appréciable, concret.

&#9784; *Soutenir l’autonomie* (en n’étant pas contrôlant, en donnant des
explications rationnelles et prosociales) est stratégiquement le plus
approprié si on souhaite transmettre à la personne l’adoption d’un
comportement à long terme, qui peut potentiellement «&nbsp;déborder&nbsp;» (Spill
Over effect), c’est-à-dire entraîner un comportement analogue (par
exemple on apprend un comportement écologique de tri, la personne va
faire déborder ce comportement par elle-même en commençant à faire
attention à sa production de déchets).

<div style="background: #F5F5F5">
&#8505; &#8674;  Dolan et Galizzi (2015) ont constaté que cet effet de débordement est
au plus fort lorsque les interventions visent la motivation
intrinsèque&nbsp;; et inversement, les interventions basées sur
l’augmentation de la culpabilité ont les effets les plus négatifs.
</div>

La motivation intrinsèque + la motivation intégrée sont le carburant des
résistants et génèrent, selon les situations, un courage, une créativité
rebelle et une puissance exceptionnelle, qu’eux-mêmes ne comprennent pas
quand elles adviennent[^65]. En cela, il me semble que ce sont les
motivations qu’on pourrait davantage tenter de nourrir lorsqu’on est
militant ou engagé, puisqu’une seule personne avec une telle motivation
peut transformer toute une situation concernant des centaines d’autres.

### Un militantisme autodéterminateur plutôt que contrôlant

La théorie de l’autodétermination donne des outils vraiment très
accessibles, testables, qui ont déjà démontré une forte efficacité. Mais
avant de trop nous emballer, il y a malheureusement à se rappeler que
même la militance la plus efficace ne pourra réparer immédiatement tout
le mal que des décennies d’environnements sociaux déconnants ont pu
générer, ni même réussir à combler les besoins d’individus qui sont
encore aux prises d’environnements sociaux sapants. Un oncle peut
arriver à rendre joyeux et libre son neveu, mais si l’enfant est battu
par ses parents dès qu’il les retrouve, tout le travail de nourrissement
des besoins par l’oncle est réduit en miettes. Parfois la meilleure aide
à apporter à autrui est de l’aider à fuir des environnements sociaux
destructifs, que ce soit la famille maltraitante, le travail où il y a
harcèlement, ce village où il n’y a que surveillance, mépris et
solitude, etc. Cet exemple peut apparaître éloigné des situations de
militance, mais pas tant que ça&nbsp;: lorsqu’on discute, qu’on tente de
comprendre l’adversaire ou le spectateur voulant rester dans sa routine
et ne rien changer, ceux-ci nous décrivent rapidement des environnements
sociaux dans lesquels ils sont sous emprise, parfois de manière très
complexe, et dont on peut difficilement les aider à s’extirper pour 
de meilleurs environnements sociaux (par conséquent, le
changement de comportement qu’on propose peut apparaître à la personne
comme un effort trop grand, ou ridicule par rapport à la souffrance
vécue).

Cependant, pour reprendre cette métaphore familiale, cet oncle qui aura
rendu heureux cet enfant maltraité, quand bien même il n’a pas réussi
pour le moment à trouver une solution pour libérer cet enfant, lui a
tout de même offert un modèle d’environnement social sain, lui aura
montré que les choses peuvent fonctionner d’une bien meilleure façon.
Cet acte n’est absolument pas anodin, au contraire, il permet d’aider
l’enfant à ne pas intérioriser le modèle maltraitant comme étant la
bonne chose à reproduire (puisque le modèle concurrent est producteur de
bonheur), et ça c’est extrêmement important pour le futur, pour son
développement.

Voilà pourquoi ça vaut le coup d’essayer de nourrir les besoins
fondamentaux des personnes, surtout dans un travail engagé/militant,
quand bien même on n’arrive pas dans l’immédiat à résoudre les grands
problèmes, ni à changer aucun comportement ou à convaincre. Il ne s’agit
pas de placer un arbre de force, mais de distiller quelques graines ci
et là. Si on a nourri un peu les besoins de la personne par notre
écoute, c’est déjà beaucoup, parce que c’est montrer concrètement qu’un
environnement social peut être nourrissant. Pour donner un exemple
concret, des discussions sympas peuvent amener un adversaire à
abandonner une idéologie qui le ravageait et se transformer&nbsp;: un incel[^66] raconte comment le fait d’avoir des discussions banales avec
des féministes et autres personnes non-incel lui a apporté quelque chose de libérateur. Le fait de rencontrer d’autres environnements sociaux ne fonctionnant pas de la même manière peut constituer une expérience
paradigmatique qui les transforment&nbsp;:

> «&nbsp;Quand j’étais un incel je ne sortais jamais. Je n’avais jamais mis un pied dans un bar, un club, je ne connaissais rien de ce style de vie.  Du coup, c’était facile de croire tout ce qui se racontait en ligne sur les bars, les clubs, les femmes, parce que je n’avais aucun élément de comparaison issu de la réalité qui m’aurait permis de séparer le vrai du faux. 
> 
> La première fois que j’ai été dans un bar, j’ai vu un mec faisant bien 10 centimètres de moins que moi et le double de mon poids, installé dans le carré VIP avec plein de femmes sexy gravitant de son côté. Voir ça, ça a anéanti ma vision du monde. Parce que si on en croit la communauté des incels, ce que faisait ce mec, là, c’était littéralement impossible.
> 
> En gros, j’ai remplacé ce que j’ai appris des incels par des connaissances tirées d’expériences réelles.&nbsp;» (tiré de [reddit.com](https://www.reddit.com/r/IncelTears/comments/8fd7in/a_story_from_a\_former_incel_how_i\_got_myself_out/), traduit par [Madmoizelle.com](https://www.madmoizelle.com/incel-temoignage-920619))[^66b].



Voilà ci-dessous tout ce que la théorie de l’autodétermination conseille
pour nourrir les besoins et tout ce qui pourrait aider la personne à
s’autodéterminer&nbsp;; il y a aussi tout ce qu’il y a à ne pas faire car
cela sape l’autodétermination, envoie les individus vers des motivations
de basses qualités. Cependant, si vous êtes autoritaire et si vous visez
le contrôle des individus afin d’en faire des pions, que vous avez
d’énormes moyens afin de développer ce mode de contrôle (par exemple
installer une surveillance massive de tout instant, embaucher de
nombreux militants injonctiveurs tels que des chefs, sous-chefs,
surveillants, contremaître, *black hat trolls*[^67], etc.), évidemment
il serait incohérent de suivre les conseils autodéterminateurs puisque
cela irait contre vos buts. À noter que ce ne sont pas des conseils
juste lancés comme ça, tout a été testé par des expériences et études
répliquées.

Table:  *Recommandations de la SDT pour viser l’autodétermination (= motivation intrinsèque + motivation intégrée + besoins fondamentaux comblés + orientation autonome)*

| Ce qui aide à l’autodétermination et au bien-être des individus dans les environnements sociaux. (Environnements autodéterminants) | Ce qui empêche l’autodétermination, contribue au mal-être, et pousse les individus à être pion dans les environnements sociaux (Environnements contrôlants) |
|:-----------------------------------------|:------------------------------------------------|
| &bull; Viser le bien-être<br> &bull; Viser le comblement des besoins<br> &bull; Chercher à ce que les individus soient autodéterminé, puissent  s’émanciper grâce à nos apports  ou  être  libres  dans  la  structure  (viser  la  préservation,  le  developpement,  le  maintien  de  la  motivation  intrinsèque,  la  régulation  identifiée/intégrée,  l’orientation autonome, l’amotivation pour les activités/comporte-ments sapant les besoins des autres/de soi) <br> &bull; Formuler, transmettre, encourager et nourrir les buts et aspirations intrinsèques, montrer les possibilités de la situation  | &bull; Viser le mal-être<br> &bull; Viser la frustration des besoins pour mieux déterminer son   comportement/ses idées...<br> &bull; Chercher  à  déterminer  totalement  les  individus,  a  avoir  un  contrôle  total  sur  eux  (orientation  contrôlée/impersonnelle,  pas  de  motivation  intrinsèque,  introjection,  régulation  externe,  amotivation  pour  les  activi-tés/comportements  nourrissant  ses  ou  les  besoins  des  autres) <br> &bull; Formuler, transmettre, encourager et nourrir les buts et aspirations extrinsèques, éliminer/nier buts intrinsèques, montrer les impossibilités et les contrôles de la situation    |
| **Concevoir un environnement favorisant l’autonomie** | **Concevoir un environnement contrôlant** |
|   &bull; transmission autonome de limites (pas de langage contrôlant&nbsp;; reconnaissance des sentiments négatifs&nbsp;; justification rationnelle et prosociale de la limite)<br> &bull; proposition et soutien de vrais choix, pas simplement des options interchangeables<br> &bull; fournir des explications claires et rationnelles<br> &bull; permettre à la personne de changer la structure, le cadre, les habitudes si cela est un bienfait pour tous<br> &bull; ne pas condamner les prises d’initiatives<br> &bull; modèle horizontal, autogouverné, en appuyant sur le pouvoir constructif de chacun | &bull; punitions  <br> &bull; transmission contrôlée des limites (langage contrôlant, déni des émotions, absence de justification) <br> &bull; récompenses (conditionné à la performance, conditionnelles) <br> &bull; mise en compétition menaçant l’ego <br> &bull; surveillance <br> &bull; notes / évaluations menaçant l’ego <br> &bull; objectifs imposés/temps limité induisant une pression <br> &bull; appuyer sur la comparaison sociale <br> &bull; évaluation menaçant l’ego <br> &bull; modèle de pouvoir hiérarchique, en insistant fortement sur son pouvoir dominant |
| **Concevoir un environnement favorisant la proximité sociale** | **Concevoir un environnement niant le besoin de proximité sociale ou uniquement de façon conditionnelle** |
| &bull; faire confiance<br > &bull; se préoccuper sincèrement des soucis ou problèmes de l’autre<br > &bull; dispenser de l’attention et du soin<br > &bull; exprimer son affection, sa compréhension<br > &bull; partager du temps ensemble<br > &bull; savoir s’effacer lorsque la personne n’a pas besoin de nous<br > &bull; écouter | &bull; ne jamais faire confiance<br> &bull; être condescendant, exprimer du dédain envers les personnes<br> &bull; terrifier les personnes <br> &bull; montrer de l’indifférence pour les autres<br> &bull; instrumentaliser les relations<br> &bull; empêcher les liens entre les personnes de se faire<br> &bull; comparaison sociale<br> &bull; appuyer sur les mécanismes d’inflation de l’ego (l’orgueil, la fierté d’avoir dépassé les autres) |
| **Concevoir un environnement favorisant la compétence** | **Concevoir un environnement défavorisant la compétence ou n’orientant que la compétence via la performance** |
| &bull; être clair sur les procédures, la structure, les attentes<br> &bull; laisser à disposition des défis/tâches optimales, adaptables à chacun<br> &bull; donner des trucs et astuces pour progresser<br> &bull; permettre l’autoévaluation<br> &bull; si besoin, proposer des récompenses «&nbsp;surprises&nbsp;» et congruentes (sans condition)<br> &bull; donner des feed-back informatif, positif ou négatif, mais sans implication de l’ego. | &bull; ne pas communiquer d’attentes claires, ni donner de structures ou procédures concernant les choses à faire<br> &bull; donner des taches et défis inadaptés aux compétences des personnes voire impossible. <br> &bull; Évaluer selon la performance<br> &bull; donner des feedback menaçant l’ego de la personne (humiliation, comparaison sociale)<br> &bull; donner des feedback flous sans informations<br> &bull; traduire les réussites et échecs en terme interne allégeant.<br> &bull; feed-back positif pour quelque chose de trop facile<br> &bull; valoriser les signes extérieurs superficiels de réussite |



<!------------ notes CHAP 6 ----------->


[^64]: Les recherches sur l’autodétermination démontrent que
    généralement les gens détectent et jugent très positivement les
    personnes à motivation intrinséque, passionnées, et souhaitant
    empuissanter autrui. Leur propre motivation intrinsèque augmente
    aussi via l’exposition à ces profils (que ce soit la ou le
    conjoint·e, les professeur·es, les coachs, les superviseurs, etc.).
    Cependant, s’ils sont contrôlants, sapent l’autonomie, cela ne
    marche pas du tout et fait l’effet inverse. Cf Deci, Schwartz,
    Sheinman et Ryan (1981)&nbsp;; Ryan et Grolnick (1986)&nbsp;; Deci et Ryan
    (2017).

[^65]: Dans *Un si fragile vernis d’humanité, banalité du mal, banalité du bien*, Michel Terestchenko rapporte comment un routier, voyant une situation où un groupe de Juifs allait se faire
    expulser ou incarcérer en camp, s’est d’un coup fait passer pour un
    diplomate auprès des nazis et a pu interdire aux autorités de nuire
    à ce groupe. Il a fait ça sans l’avoir prémédité. On trouve quantité
    d’actes non prémédités d’altruisme hautement stratégique et très
    efficace également dans «&nbsp;Altruistic personnality&nbsp;» des Oliner (dont
    on a traduit [des morceaux ici](https://www.hacking-social.com/2019/03/25/pa1-la-personnalite-altruiste/)&nbsp;; globalement, cela semble dû à un altruisme à motivation intégrée, ou
    à une amotivation autodéterminée à faire du mal qui a été forgée
    dans le passé, notamment grâce au fait que les désobéissants aient
    eu au moins un proche ou un ami nourrissant leurs besoins
    fondamentaux et présentant concrètement des actes altruistes.

[^66]: Idéologie anti-femme / anti-couples qui considère (entre autres)
    que seuls certains hommes exceptionnellement beaux ou riches
    attireront les femmes, donc qu’ils seront célibataires à jamais. Il
    y a aussi chez eux un rejet des femmes non-blanches et/ou
    non-blondes, un rejet des femmes ne suivant pas un modèle
    traditionnel (par exemple, si elles travaillent, si elles ont fait
    des études), un rejet du fait qu’elles puissent être des personnes
    (l’incel considére que s’il rend service à une femme, elle doit
    coucher avec lui&nbsp;; il y a une infériorisation de la femme et une
    objectivation). Ils disent haïr les femmes tout en disant crever
    d’envie d’être en couple avec elles. Les incels ont commis des
    tueries de masse à l’encontre des couples et des femmes, cf. le
    listing [sur Wikipédia](https://fr.wikipedia.org/wiki/Incel) (il est
    malheureusement régulièrement mis à jour).

[^66b]: Ici aussi&nbsp;:  Jack Peterson, *Why i’m leaving incel* ([Youtube](https://www.youtube.com/watch?v=ng29REVIyTQ))&nbsp;; 
    Nina Pareja, «&nbsp;[un *incel* repenti regrette le manque d’ironie du mouvement masculiniste](http://www.slate.fr/story/163424/peterson-incel-repenti-regrette-manque-ironie-mouvement-masculiniste)&nbsp;», 
	*Slate.fr*, 2018&nbsp;; et un [article du Guardian](https://www.theguardian.com/world/2018/jun/19/incels-why-jack-peterson-left-elliot-rodger).

[^67]: Ou «&nbsp;farfadet de la dialectique à chapeau noir&nbsp;». Ceci n’est pas
    un terme de l’Académie française pour troll, mais une proposition
    d’un internaute qui a répertorié [d’autres propositions ici](https://twitter.com/Nosferalis/status/1260468733274882063).







<!------------ CHAP 7 D’autres voies pour trouver d’autres façons de faire ----------->



## D’autres voies pour trouver d’autres façons de faire

Je me suis beaucoup appuyée sur la théorie de l’autodétermination
pour analyser et extirper des solutions alternatives au militantisme
déconnant, mais c’est juste une perspective parmi d’autres qui
pourraient être tout aussi bonnes&nbsp;; ce n’est pas «&nbsp;la&nbsp;» chose à faire ni
«&nbsp;la&nbsp;» perspective qu’il faudrait avoir, bien au contraire. Je l’ai
choisie juste parce que je la trouve à la fois suffisamment précise pour
entrer dans le détail, et suffisamment libertaire pour que chacun
puisse réfléchir *à partir* de lui-même et non
selon ses «&nbsp;règles&nbsp;». Bref, ce n’est pas une théorie qui ordonne, mais
qui laisse le maximum de possibilités et essaye de donner des pistes
d’extension à celles-ci, c’est pour cela que j’aime la
partager. Mais tout est bon pour trouver d’autres sources
d’inspiration.

### Rétro-ingénierie du kiff

On pourrait trouver quantité de solutions, d’alternatives, de façons
de faire, en analysant dans le détail tout ce qu’on adore, tout ce qui
nous a motivés, dans ce qu’on a trouvé de merveilleux et de
mémorable. Il s’agit de faire de la rétro-ingénierie du kiff
pour tenter de le reproduire un jour au travers de nos activités. Un peu
comme le travail de recherche des game-designers lorsqu’ils
cherchent les mécaniques qu’ils voudront reproduire dans leur
jeu&nbsp;: les conseils de la dev du dimanche sont excellents à ce
sujet, et perso je pense qu’on peut les transposer tout à fait à des
domaines qui ne sont pas de l’ordre du jeu, si on cherche à
créer quelque chose qui générera une expérience motivante&nbsp;:

<figure>
    <img src="Pictures/journalbordEP02.jpg"
         alt="Journal de bord EP 02 - Coucou, tu veux voir mon pitch&nbsp;?">
    <figcaption>Journal de bord EP 02 - Coucou, tu veux voir mon pitch&nbsp;? Voir sur <a href="https://www.youtube.com/watch?v=RIvclun0utQ">Youtube</a></figcaption>
</figure>


J’ai aussi énormément aimé les méthodes et les façons de penser que
j’ai trouvées dans «&nbsp;l’art du game design&nbsp;» de Jesse Shell,
«&nbsp;Rules of play&nbsp;» de Katie Salen, et globalement le champ du
game-design me fascine parce qu’il nous apprend comment construire une
structure -- un jeu -- qui va motiver au maximum autrui, lui
faire vivre des expériences mémorables ou des moments sociaux uniques en
leur genre.

### Rétro-ingénierie de l’adversaire et bidouillage

L’adversaire, surtout si sa création domine, a réussi un truc. Le
problème qu’on peut avoir avec lui c’est que sa création génère
de la souffrance, de l’injustice, et/ou sert uniquement des intérêts
personnels. Cependant, sa manière de faire a eu une
puissance d’influence qu’on peut décortiquer et qu’on peut transformer
de façon beaucoup plus profitable. Par exemple, bien que je sois assez
anti-pub, je sais que lire les publicitaires à travers leurs manuels
a été très utile&nbsp;: par exemple, si je veux retenir quelque chose
par cœur, j’emploierais les méthodes qu’ils
utilisent pour que le consommateur mémorise un message. On peut
faire des lectures, des analyses de l’adversaire et déjà commencer à
hacker et transformer ses méthodes. Par exemple, je suis très critique
du *nudge* (manipulation des comportements via
le design de l’environnement), d’autant que le
*nudge* est souvent utilisée sous une idéologie
néo-libérale, mais je le trouve aussi fun et super
intéressant&nbsp;; récemment un doctorant m’a montré son étude[^68]
où il avait hacké le *nudge* pour en supprimer
l’aspect manipulateur et lui substituer
à la place de l’autodétermination. Les résultats ont bien
été là, c’était jubilatoire. Tout est transformable, hackable et la
bonne nouvelle c’est que ce mécanisme de hack et de transformation est
ultra fun à vivre lorsqu’on l’opère, mais aussi lorsqu’on y assiste en
tant que spectateur, cela nous libère totalement de la
colère, de l’énervement, de l’impuissance, et à la place on trouve
du fun et de l’empuissantement.



### Tout plaquer pour créer

On peut totalement laisser tomber certaines formes de militance
(interpersonnelles) pour se consacrer à créer/œuvrer et cela ne fera pas
de nous un moins «&nbsp;bon&nbsp;» militant. On peut se donner pour but de créer
de meilleurs environnements sociaux (être ce super collègue, ce pote à
qui on peut se confier, ce soutien en qui on a confiance…)&nbsp;;
on peut créer ou aider à créer des trucs et bidules ou
évènements funs&nbsp;; on peut être ce mentor qui apprend tout autant qu’il
soutient&nbsp;; on peut être ce spectateur, ce blogueur, ce journaliste qui
voit et repère ce truc qui change la face du monde si on le fait
connaître. Résister, c’est créer[^69]&nbsp;; et créer c’est changer
le monde.

Je sais qu’il y a une espèce de bataille de chapelle
militante entre ceux qui pensent que seule la confrontation va
amener du changement, et ceux qui veulent incarner/créer le changement
dès à présent, accusant l’un l’autre de pas avoir la bonne stratégie (on
accuse celui qui crée d’être lâche et de ne pas faire front à l’ennemi&nbsp;;
on accuse celui qui se confronte à l’adversaire d’être
violent et de ne pas créer le monde qu’il voudrait voir
apparaître), mais en fait tout ceci se superpose, se croise,
interagit et il n’y a pas à se complexer de ne pas être en confrontation
ou de n’être qu’en confrontation (ou de le reprocher aux autres)&nbsp;: au
final, c’est l’interaction en système (parfois invisible) qui est
productrice de transformation et changement positifs dans la société,
sur le long terme.

### Tout empuissanter, y compris sur la base de conflits

Ça se confronte entre alliés, envers les spectateurs,
dans le mouvement, il y a donc à trouver des façons de gérer les
conflits. Et il y aurait besoin d’un mode qui permette de régler
les problèmes en interne sans qu’il y ait par la suite des
décennies de ressentiments de la part des uns et des autres,
prêts à exploser au moindre faux pas. Il y aurait
aussi besoin d’une façon de régler les conflits qui ne soient pas
autoritaire, car généralement les mouvements militants (excepté
fascistes, d’extrême-droite) n’aiment pas trop la justice punitive. Et
l’idéal serait évidemment que cette résolution de conflits soit
productrice d’empuissantement et d’autodétermination.

Bonne nouvelle, des militants ont déjà bossé dessus et ont déjà
établi des tas de protocoles extrêmement empuissantants
permettant de gérer les conflits efficacement, au bénéfice de toute le
monde, permettant en plus de prévenir d’autres problèmes&nbsp;: c’est la
justice transformatrice. Je vous laisse consulter toutes ces ressources
ici&nbsp;:

- transform harm ([transformharm.org/](https://transformharm.org/)), tout particulièrement les sections «&nbsp;curriculum&nbsp;» de chaque thème (*restorative justice, healing justice*, etc.) sont emplies de programmes, d’outils très intéressants.
- Les outils de [Creative interventions](https://transformharm.org/wp-content/uploads/2019/05/CI-Toolkit-Complete-FINAL-2.pdf).
- Le processus de responsabilisation&nbsp;: un outil absolument génial quelle que soit sa position ou son rôle dans le conflit, qu’on ait été cible d’un comportement qui nous a été pénible ou cause de cette pénibilité, voire témoin du problème, ou auteur de l’offense. Je trouve vraiment que c’est un outil qui permet de décider en toute autodétermination ce qu’on souhaite pour la suite, ce qui pourrait rétablir des liens de confiance, ce qui permettrait de réparer la situation. Vous trouverez les ressources en anglais[ici](https://transformharm.org/download/community-accountability-for-survivors-of-sexual-violence-toolkit-2/?wpdmdl=929&refresh=5f2cab37415aa1596762935) (je l’ai traduit [ici](https://www.hacking-social.com/2021/02/08/jr7-justice-transformatrice-le-processus-de-responsabilisation/) également).


### D’autres ressources

J’ai été loin d’être exhaustive dans cet article, on aurait pu
parler de long en large du conformisme et de son pourquoi,
davantage de la réactance, des dynamiques narcissiques qui peuvent aussi
poser problème dans la militance (il n’y a généralement pas d’argent
à gagner dans ces milieux, mais ça peut attirer des profils en quête de
notoriété, de personnes voulant se démarquer et dont l’attitude
peut s’opposer au travail collectif), et j’aurais pu aussi parler de
ce qui y a au cœur des mécaniques militantes fascistes en parlant de
l’autoritaire, du dominateur, etc. Donc, voici d’autres sources
potentiellement inspiratrices pour un militantisme qui affronterait
l’adversaire et augmenterait la cohésion avec les spectateurs et
alliés, en créant, en gueulant, en se posant, en jouant et j’en passe&nbsp;;
ces ressources ne sont pas exhaustives non plus[^70], il y en a
certainement des milliers d’autres.

Dans l’histoire ou la philosophie, témoignages, stratégies et
paradigmes de désobéissance&nbsp;:

1. Magda et André Trocmé, <cite>Figures de résistance</cite>, texteschoisis par Pierre Boismorand, Cerf, 2008.
2. Semelin Jacques et Mellon Christian, <cite>La non-violence</cite>, PUF, 1994.
3. Semelin Jacques, <cite>Sans armes face à Hitler</cite>, Les arènes, 1998.
4. James Haskins et Rosa Parks, <cite>Mon histoire</cite>, Libertalia 2018.
5. La Boétie, <cite>Discours sur la servitude volontaire</cite>, Bossard, 1922 (première édition, 1578) Voir [Wikipédia](https://fr.wikisource.org/wiki/Discours_de_la_servitude_volontaire/%C3 %89dition_1922).
6. Michel Terestchenko, <cite>Un si fragile vernis d’humanité, Banalité du mal, banalité du bien</cite>, La découverte, 2005.
7. Samuel P. Oliner, Pearl M. Oliner, <cite>The altruistic personnality, rescuers of jews in Nazi Europe</cite>, Macmillan USA, 1988 (on [en a fait un résumé ici](https://www.hacking-social.com/2019/03/25/pa1-la-personnalite-altruiste/).
8. Frédéric Gros, <cite>Désobéir</cite>, Albin Michel, 2017.

Autour du numérique&nbsp;:

1. Edward Snowden, <cite>Mémoires Vives</cite>, Seuil, 2019.
2. Amaëlle Guitton, <cite>Hacker&nbsp;: au cœur de la résistance numérique</cite>, Au diable Vauvert, 2013.
3. Nicolas Danet et Frederic Bardeau, <cite>Anonymous. Pirates informatiques ou altermondialistes numériques&nbsp;?</cite>, FYP Édtions 2011.
4. Steven Levy, <cite>L’éthique des hackers</cite>, Globe, 2013.
5. Pekka Himanen, <cite>L’éthique hacker</cite>, Exils, 2001.

De l’activisme&nbsp;:

1. Srdja Popovic, <cite>Comment faire tomber un dictateur quand on est seul, tout petit, et sans armes</cite>, Payot, 2015.
2. Automone A.F.R.I.K.A. Gruppe, Luther Blisset, Sonja Brünzels, <cite>Manuel de communication-guérilla</cite>, Éditions Zones, disponible [en libre accès](https://www.editions-zones.fr/lyber?manuel-de-communication-guerilla).
3. Andrew Boyd et Dave Ashald Mitchell, <cite>Joyeux bordel, tactiques et principes et théories pour faire la révolution</cite>, Les Liens qui libèrent, 2015 (qui est en fait une partie traduite des tactiques
qu’on trouve à [libre disposition ici](https://www.beautifultrouble.org/toolbox/#/)&nbsp;; le livre en anglais est aussi [disponible librement ici](https://www.creativityandchange.ie/wp-content/uploads/2017/06/beautiful-trouble.pdf), ils en ont même fait un jeu à destinations des militants pour s’aider à
construire des actions, [par ici](https://www.beautifultrouble.org/strategy-cards).

<figure>
    <img src="Pictures/TheYesMen.jpg"
         alt="L’activisme façon Yes men">
    <figcaption>L’activisme façon Yes men. Voir <a href="https://www.dailymotion.com/video/xqihuo">cette vidéo</a> et <a href="https://www.dailymotion.com/video/xqihrr">celle-ci</a>.</figcaption>
</figure>




<!------------ notes CHAP 7  ----------->


[^68]: Toussard (2020).

[^69]: Citation de Stéphane Hessel.

[^70]: Non pas parce que je suis de mauvaise volonté ou que je
    censurerais des ouvrages, mais simplement parce que je suis juste
    une personne qui n’a qu’une poignée d’heures par jour à disposition,
    donc je ne peux pas tout lire, tout étudier. Je précise cela parce
    que j’ai déjà eu des militants me reprochant de ne pas citer untel
    ou untelle.


# Références

Teresa M. Amabile, William DeJong et Mark R. Lepper, « Effects of externally imposed deadlines on subsequent intrinsic motivation », *Journal of Personality and Social Psychology*,  34-1, 1976, p. 92‑98.

Rosemarie Anderson, Sam T. Manoogian et J. Steven Reznick, « The undermining and enhancing of intrinsic motivation in preschool children », *Journal of Personality and Social Psychology*,  34-5, 1976, p. 915‑922.

Susan Anderson et Judith Rodin, « Is Bad News Always Bad?: Cue and Feedback Effects on Intrinsic Motivation », *Journal of Applied Social Psychology*,  19-6, 1989, p. 449‑467.

Avi Assor, Guy Roth et Edward L. Deci, « The emotional costs of parents’ conditional regard: a self-determination theory analysis », *Journal of Personality*,  72-1, 2004, p. 47‑88.

Roy F. Baumeister et Dianne M. Tice, « Self-esteem and responses to success and failure: Subsequent performance and intrinsic motivation », *Journal of Personality*,  53-3, 1985, p. 450‑467.

Beiwen Chen, Maarten Vansteenkiste, Wim Beyers, Liesbet Boone, Edward Deci, Jolene Van der Kaap-Deeder, Bart Duriez, Willy Lens, Lennia Matos, Athanasios Mouratidis, Richard Ryan, Kennon Sheldon, Bart Soenens, Stijn Van Petegem et Joke Verstuyf, « Basic psychological need satisfaction, need frustration, and need strength across four cultures », 2015.

Mihaly Csikszentmihalyi, *Applications of Flow in Human Development and Education*, Springer Netherlands, 2014.

Mihaly Csikszentmihalyi, *The Systems Model of Creativity*, Springer Netherlands, 2014.

J. M. Darley et B. Latané, « Bystander intervention in emergencies: diffusion of responsibility », *Journal of Personality and Social Psychology*,  8-4, 1968, p. 377‑383.

E. L. Deci, R. Koestner et R. M. Ryan, « A meta-analytic review of experiments examining the effects of extrinsic rewards on intrinsic motivation », *Psychological Bulletin*,  125-6, 1999, p. 627‑668; discussion 692-700.

Edward Deci, *Intrinsic Motivation*, Springer US, 1975.

Edward L. Deci, Gregory Betley, James Kahle, Linda Abrams et Joseph Porac, « When Trying to Win: Competition and Intrinsic Motivation », *Personality and Social Psychology Bulletin*,  7-1, 1981, p. 79‑83.

Edward L. Deci et Wayne F. Cascio, « Changes in Intrinsic Motivation as a Function of Negative Feedback and Threats », 1972.

Edward L. Deci, Wayne F. Cascio et Judith Krusell, « Cognitive evaluation theory and some comments on the Calder and Staw critique », *Journal of Personality and Social Psychology*,  31-1, 1975, p. 81‑85.

Edward L. Deci et Richard M. Ryan, « A motivational approach to self: Integration in personality », in *Nebraska Symposium on Motivation, 1990:  Perspectives on motivation*, Lincoln, NE, US, University of Nebraska Press, 1991, p. 237‑288.

Edward L. Deci, Allan J. Schwartz, Louise Sheinman et Richard M. Ryan, « An instrument to assess adults’ orientations toward control versus autonomy with children: Reflections on intrinsic motivation and perceived competence », *Journal of Educational Psychology*,  73-5, 1981, p. 642‑650.

Paul Dolan et Matteo M. Galizzi, « Like ripples on a pond: Behavioral spillovers and their implications for research and policy », *Journal of Economic Psychology*,  47, 2015, p. 1‑16.

Michael E. Enzle et Sharon C. Anderson, « Surveillant intentions and intrinsic motivation », *Journal of Personality and Social Psychology*,  64-2, 1993, p. 257‑266.

Antonella Delle Fave, Fausto Massimini et Marta Bassi, *Psychological Selection and Optimal Experience Across Cultures: Social Empowerment through Personal Growth*, Springer Netherlands, 2011.

B. J. Fogg, *Persuasive Technology: Using Computers to Change What We Think and Do*, Amsterdam ; Boston, Morgan Kaufmann Publishers In, 2003.

Jean Hatzfeld, *Une saison de machettes: récits*, Paris, France, Éditions du Seuil, 2003.

Richard Koestner, Nathalie Houlfort, Stephanie Paquet et Christine Knight, « On the Risks of Recycling Because of Guilt: An Examination of the Consequences of Introjection », *Journal of Applied Social Psychology*,  31-12, 2001, p. 2545‑2560.

Richard Koestner, Gaëtan F. Losier, Robert J. Vallerand et Donald Carducci, « Identified and introjected forms of political internalization: Extending self-determination theory », *Journal of Personality and Social Psychology*,  70-5, 1996, p. 1025‑1036.

Richard Koestner, Richard M. Ryan, Frank Bernieri et Kathleen Holt, « Setting limits on children’s behavior: The differential effects of controlling vs. informational styles on intrinsic motivation and creativity », *Journal of Personality*,  52-3, 1984, p. 233‑248.

M. R. Lepper, D. Greene et R. E. Nisbett, « Undermining children’s intrinsic interest with extrinsic reward: A test of the “overjustification” hypothesis », *Journal of Personality and Social Psychology*,  28-1, 1973, p. 129‑137.

Mark R. Lepper et David Greene, « Turning play into work: Effects of adult surveillance and extrinsic rewards on children’s intrinsic motivation », *Journal of Personality and Social Psychology*,  31-3, 1975, p. 479‑486.

Arlen Moller, Guy Roth, Christopher P. Niemiec, Yaniv Kanat-Maymon et Edward L. Deci, « Mediators of the associations between parents’ conditional regard and the quality of their adult-children’s peer relationships ».

Pearl Oliner, Samuel P. Oliner, Lawrence Baron et Lawrence Blum, *Embracing the Other: Philosophical, Psychological, and Historical Perspectives on Altruism*, New York, New York University Press, 1992.

Samuel P. Oliner, *Altruistic Personality: Rescuers Of Jews In Nazi Europe*, Reprint édition., Touchstone, 2002.

Samuel P. Oliner et Pearl M. Oliner, *The Altruistic Personality: Rescuers of Jews in Nazi Europe*, New York : London, Macmillan USA, 1988.

Thane S. Pittman, Margaret E. Davey, Kimberly A. Alafat, Kathryn V. Wetherill et Nancy A. Kramer, « Informational versus Controlling Verbal Rewards », *Personality and Social Psychology Bulletin*,  6-2, 1980, p. 228‑233.

Robert W. Plant et Richard M. Ryan, « Intrinsic motivation and the effects of self-consciousness, self-awareness, and ego-involvement: An investigation of internally controlling styles », *Journal of Personality*,  53-3, 1985, p. 435‑449.

Catherine Gueguen, *Pour une enfance heureuse. Repenser l’éducation à la lumière des dernières découvertes sur le cerveau*, Paris, Pocket, 2015.

Mark J. Reader et Stephen J. Dollinger, « Deadlines, Self-Perceptions, and Intrinsic Motivation », *Personality and Social Psychology Bulletin*,  8-4, 1982, p. 742‑747.

Carl Ransom Rogers, *Liberté pour apprendre ?*, Paris, France, Dunod, 1984.

Carl Ransom Rogers, Eleonore Lily Herbert et Max Préfacier Pagès, *Le développement de la personne*, Paris, France, Dunod, 1996.

Richard M. Ryan et Edward L. Deci, *Self-Determination Theory: Basic Psychological Needs in Motivation, Development, and Wellness*, New York, Guilford Press, 2017.

Richard M. Ryan et Wendy S. Grolnick, « Origins and pawns in the classroom: Self-report and projective assessments of individual differences in children’s perceptions », *Journal of Personality and Social Psychology*,  50-3, 1986, p. 550‑558.

Richard M. Ryan, Scott Rigby et Kristi King, « Two types of religious internalization and their relations to religious orientations and mental health », *Journal of Personality and Social Psychology*,  65-3, 1993, p. 586‑596.

Jacques Semelin, *Sans armes face à Hitler: la résistance civile en Europe*, 1939-1943, Paris, France, Les Arènes, impr. 2013, 2013.

Jacques Semelin, *Purifier et Détruire. Usages politiques des massacres et génocides*, Paris, Le Seuil, 2005.

Jacques Sémelin, *Pour sortir de la violence*, Paris, Editions ouvrières, 1983.

Jacques Semelin, Christian Mellon, *La Non-Violence*, Paris, Presses Universitaires de France - PUF, 1994.

Ervin Staub, *The Psychology of Good and Evil: Why Children, Adults, and Groups Help and Harm Others*, Cambridge, U.K. ; New York, Cambridge University Press, 2003.

Michel Terestchenko, *Un si fragile vernis d’humanité: banalité du mal, banalité du bien*, Paris, France, la Découverte : MAUSS, 2005.

Netta Weinstein, William S. Ryan, Cody R. DeHaan, Andrew K. Przybylski, Nicole Legate et Richard M. Ryan, « Parental autonomy support and discrepancies between implicit and explicit sexual identities: Dynamics of self-acceptance and defense », *Journal of Personality and Social Psychology*,  102-4, 2012, p. 815‑832.


