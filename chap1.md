<!-------------- CHAP 1 Le jeu Militant ---------------->


## Le jeu militant

Pour comprendre les différents problèmes, prenons la
métaphore du jeu (game). Par «&nbsp;game[^15]» j’entends la structure
du jeu militant, c’est-à-dire ses buts, ses règles implicites et
explicites qui définissent les possibilités ou les limites d’action&nbsp;;
cette structure peut être investie par la motivation
singulière du militant, son «&nbsp;play&nbsp;», c’est-à-dire sa façon de jouer
le jeu ou de le subvertir, et ce dans une forme de
légaliberté[^16] (une liberté nouvelle qui s’exerce *dans et
par* les règles singulières du jeu). Le jeu militant est donc une
création de mouvements singuliers qui auparavant
n’existaient pas dans la société.

Et dans ce jeu que je qualifierais de stratégie-gestion, les
militants peuvent rencontrer plusieurs types d’acteurs&nbsp;: les alliés,
les adversaires et les spectateurs.


### Les alliés/soutiens

Les alliés peuvent partager avec les militants des
mêmes buts, des mêmes valeurs liées à ces buts, ou une
même valorisation/dévalorisation de certains comportements. Il y
a une affinité commune quelque part, soit dans le groupe lui-même, soit
à l’extérieur. Ce soutien peut être incarné par une personne comme
par une structure (ou les deux à la fois si la personne
représente une structure), ou encore par un autre «&nbsp;game&nbsp;»
militant. Par exemple, durant l’occupation, les
résistants qui s’attaquaient directement aux structures de l’ennemi
pouvaient coopérer avec les sauveteurs[^17]: s’ils
trouvaient des enfants juifs en danger, ils contactaient des personnes
dont l’activité était de les cacher, ils étaient donc alliés contre la
destructivité nazie. Leurs actions étaient radicalement différentes,
parfois même leurs valeurs n’étaient pas du tout les mêmes, mais ils
visaient tous deux à entraver l’activité de l’adversaire (l’un en
détruisant les moyens de destructivité, l’autre en empêchant de détruire
davantage de personnes).

Les alliés le sont souvent via un but partagé, et cela n’a rien à
voir ni avec des caractéristiques personnelles communes[^18]
(caractère, physique, groupe d’appartenance, croyances,
convictions...) ni avec des interactions positives
personnelles (telles que l’amitié par exemple). On peut être
militant allié de personnes avec qui on ne s’entend pas du tout, comme
être adversaire politique de son meilleur ami, sans que cela se
passe mal au quotidien.

L’allié n’est pas non plus identifiable comme tel. Dans une
manif’ plutôt orientée thème du numérique, je me
rappelle avoir vu un couple de personnes âgées observant la foule d’un
regard sévère se mettre à hurler de façon très autoritaire à la foule de
jeunes&nbsp;: «&nbsp;C’est formidable ce que vous faites&nbsp;!!! Bravo
continuez&nbsp;!!!&nbsp;». Jamais je n’aurais pu imaginer qu’ils soient des
soutiens tant leur comportement semblait exprimer l’inverse, tant on
aurait pu préjuger autre chose d’eux (par exemple,
qu’ils ne s’intéressent pas aux questions numériques
du fait de leur grand âge).

### Les adversaires/l’adversité

Par rapport aux militants, les adversaires ne suivent pas les
mêmes buts, n’ont pas les mêmes valeurs liées à ces buts, ni
la même valorisation/dévalorisation de certains comportements.
L’adversaire ne désigne pas nécessairement un individu, il
peut être une structure (game) considérée par les
militants/engagés comme causant de la souffrance ou des
difficultés&nbsp;: par exemple, un système de *benchmark* au
travail à la Caisse d’Épargne était perçu par ses
opposants comme un adversaire à cause de la souffrance qu’il
engendrait[^19], et cet adversaire était par extension
niché dans les individus qui entretenaient, maintenaient
et défendaient ce système problématique. Dans ce
cas, un directeur avait joué le jeu du *benchmark* avec
zèle, harcelant quotidiennement les employés mis en compétition
constante et il y gagnait à pratiquer ce
*benchmark-game*, car il remportait d’énormes primes, sans
compter la «&nbsp;prestance&nbsp;» du statut. Mais par la suite,
il a subi le même type d’harcèlement, en a fait un
infarctus, ce qui lui a fait prendre conscience de l’horreur
à laquelle il avait participé. Dès lors, [il a lancé l’alerte](https://www.dailymotion.com/video/x2ymwlh).


<figure>
    <img src="Pictures/patronspression.png"
         alt="Reportage&nbsp;: les patrons mettent-ils trop la pression&nbsp;?">
    <figcaption>Reportage&nbsp;: les patrons mettent-ils trop la pression&nbsp;? (<a href="https://www.dailymotion.com/video/x2ymwlh">France 2, Envoyé Spécial</a>, 28 février 2013.</figcaption>
</figure>




L’adversaire, ce n’était pas lui en tant que personne,
l’adversaire se nichait dans sa soumission, dans son
zèle et dans son allégeance à un système déshumanisant&nbsp;:
en tant que personne, il a changé suite à son infarctus,
il a pu lever son aveuglement, prendre conscience de sa soumission
et de ce qu’il avait fait d’horrible.

Tout comme l’allié n’est pas défini par ses caractéristiques
personnelles, l’adversaire ne l’est pas non plus à cause de
caractéristiques personnelles, mais parce qu’il joue à un jeu adverse
dans lequel il est aliéné. Cette aliénation peut être dite
culturelle et s’alimenter par d’apparents gros avantages, comme
un statut très supériorisé par rapport à autrui, de très
fortes primes, des faveurs énormes dans la société… Tout cela
contribue à rendre l’individu aveugle à certaines
souffrances, parce que les regarder en face comporte le risque
d’une prise de conscience qui entraînerait à son tour de la
culpabilité, puis s’ensuivrait l’impératif compliqué de
devoir tout changer. Ces énormes avantages et autres
supériorisations sont aussi un contrôle sur l’individu&nbsp;: c’est la
carotte qui fait avancer l’âne, et prendre conscience de cet état de
pion peut être très pénible surtout quand on s’est senti si
supériorisé pendant longtemps. L’individu expliquera le
fait qu’il ait plus de carottes qu’autrui comme de l’ordre du mérite
personnel, alors que dans de nombreux cas, soit c’est un pur hasard,
soit c’est juste une façon d’entretenir sa pleine soumission.

L’individu soumis à l’adversaire peut donc s’embrumer dans
le déni, refuser de voir les conséquences des actes qu’on lui
ordonne (ou qu’il reproduit par obéissance à des injonctions
implicites de la société), parce que la prise de conscience serait une
claque majeure pour son ego et aurait un fort retentissement
dans l’organisation de sa vie. En d’autres termes, il
persiste dans cette aliénation car abandonner ses attitudes
et comportements aurait un coût bien trop élevé, et lui donnerait le
sentiment de s’être investi et engagé pour rien.

Cependant cette aliénation culturelle peut être levée. Un adversaire
qui a causé de la souffrance un temps peut tenter d’apporter une
réparation, voire aider les militants sans pour autant voler leur parole
ou les dominer encore une fois&nbsp;; dans le reportage sur le
*benchmark*, le harceleur a lancé l’alerte via son témoignage, a
pu donner une information précieuse sur le mode de fonctionnement
destructif de ce système. On voit aussi dans cette affaire que les
syndicats se sont attaqués à la structure, à l’entreprise pour régler le
problème et non à l’individu en particulier (manager,
directeur…) qui serait seul pointé du doigt et dont la simple
démission/licenciement suffirait à tout régler, car ils savaient
que l’ennemi principal était le benchmark. Et, suite à l’action
syndicale, la justice a interdit à la Caisse d’Épargne
d’employer ce mode de management.[^20]

### D’autres caractéristiques de l’adversité

&#9632; Selon les combats militants, l’adversité peut être
difficile à montrer aux autres, et il est donc difficile de faire
comprendre qu’il y a bien un problème. Par exemple, la
surveillance généralisée est rendue le plus invisible possible pour se
perpétuer, voire est dissimulée sous des artifices
*fun* et attractifs[^21]. D’où le fait que les
militants ont souvent besoin de faire un travail d’information
préalable.

&#9632; L’adversité peut être très complexe à montrer, car elle est
souvent systémique. Quand on veut expliquer un problème
(voire simplement le nommer), on arrive très rapidement,
à des choses totalement abstraites, des concepts qui sont
difficiles à représenter. Par exemple, la liberté, l’égalité et la
fraternité sont des valeurs abstraites, et chacun y plaque sa propre
représentation (par exemple pour Sade, la vraie liberté c’était
aussi d’avoir le droit de tuer et de violer sans être
réprimé[^22]) qui s’oppose à d’autres représentations (la
liberté entendue comme une responsabilité, qui inclut donc des limites,
notamment celle de ne pas porter atteinte à autrui puisque ce serait
trahir sa propre responsabilité).

Dès lors, on peut tomber dans un puits sans fond d’explications
très théoriques qui pourra rendre le combat militant élitiste, car il
demande un certain niveau culturel ainsi qu’une veille quasi permanente
des débats en cours afin d’être pleinement saisi. Ce faisant, le
discours militant pourra d’une part paraître difficile d’accès pour le
quidam et, d’autre part, ce haut degré de maîtrise théorique pourra
décourager plus d’un à rejoindre cette militance, pourra même être vécu
par d’autres comme une forme d’écrasement social et culturel, voire
comme une forme de violence symbolique (quand il s’agit de classes
sociales peu ou pas du tout favorisées). De plus, les militants
eux-mêmes pourront perdre pied avec le terrain, se retrouver dans une
tour d’ivoire sans prendre conscience de leur hauteur parfois
condescendante. Au lieu de construire, d’agir, de se confronter aux
adversaires, ils pourront perdre une énergie précieuse à force de
débattre plus que de raison en interne sur des éléments secondaires,
voire de se confondre dans des échanges interminables portant sur le
sexe des anges.

&#9632; L’adversité peut être très complexe à montrer, car
hautement technique. On en revient au point précédent&nbsp;:
si pour être un «&nbsp;bon&nbsp;» militant et comprendre le problème on
doit maîtriser X langages de programmation, savoir bidouiller en
profondeur tel OS, alors il n’y aura pas grand monde qui pourra voir
qu’il y a effectivement un problème, ni même pour s’y
confronter, tant ça demande de compétences et de
savoirs. Pour contrer ça, les mouvements peuvent se centrer sur
des problèmes plus accessibles aux personnes ou faciliter techniquement
la militance.

&#9632; L’adversaire, c’est souvent la soumission à l’autorité ou une
allégeance d’un individu qui n’a pas conscience de son statut de pion.
Au fond, les «&nbsp;vrais&nbsp;» ennemis qui auraient par exemple un plaisir
sadique à voir la souffrance sont plutôt rares. On pourrait donc
dans un mouvement militant s’interroger sur cet aspect, sur
ce pourquoi il y a une soumission à telle
attitude/comportement destructif et non à telle autre
attitude/comportement constructif&nbsp;; cela permettrait d’éviter de
s’attaquer aux personnes elles-mêmes (car ça ne réglera pas
le problème sur le long terme, on en discutera après), permettra
plutôt de transformer leur rapport à l’adversité
qu’ielles entretiennent, de les libérer de cette
allégeance et de cette soumission, ce qui peut, à terme, leur faire
éprouver une gratitude pour la cause militante.

### Le spectateur (ou témoin ou tiers[^23])

C’est l’individu qui ne fait rien d’autre que poursuivre son
train-train quotidien face à une situation qui
éveillerait en principe l’action militante, ou dans laquelle les
adversaires travailleraient à détruire. Il continue sa routine
habituelle et ne fait rien pour ou contre les éléments qui se
jouent dans la situation, qu’ils soient constructifs ou destructifs.
La recherche a montré que plus on est dans une situation où il y a
beaucoup de monde, moins on se sent acteur et moins on sera
par exemple enclin à aider autrui si nécessaire, c’est
[l’effet spectateur/témoin](https://skeptikon.fr/videos/watch/559631c3-70bd-40ca-b26c-83d1bafc404a)[^23b].


<figure>
    <img src="Pictures/effettemoin.jpg"
         alt="Effet du témoin. [XP] Peut-on compter sur un groupe pour nous porter secours&nbsp;?">
    <figcaption>Effet du témoin. [XP] Peut-on compter sur un groupe pour nous porter secours&nbsp;? Sur <a href="https://skeptikon.fr/videos/watch/559631c3-70bd-40ca-b26c-83d1bafc404a">Peertube</a>, sur <a href="https://www.youtube.com/watch?v=fJBPIQ3wwA0">Youtube</a>. Vous pouvez consulter aussi l’expérience elle-même (Darley et Latané, 1968)&nbsp;; plus d’infos aussi sur <a href="https://fr.wikipedia.org/wiki/Effet_du_t%C3%A9moin">Wikipedia</a>.</figcaption>
</figure>


Être spectateur, ce n’est donc pas être insensible aux
événements visiblement destructifs (harcèlement, agression), mais
davantage ne pas savoir s’il faut intervenir ou ne pas savoir comment,
ou encore avoir peur des conséquences si on sort de sa
routine.

En situation militante, cela se complique davantage
parce que l’individu lambda peut ne pas percevoir du tout le
problème. Par exemple, un militant antipub repérera très
vite en quoi tel panneau publicitaire sur la voie publique pose
problème et pourrait vous donner mille arguments contre l’existence
de cette pub. À l’inverse, le spectateur suit sa routine
habituelle, est plongé dans ses pensées, c’est comme si les
pubs n’existaient pas (et c’est exactement l’effet souhaité par les
publicitaires, la pub s’adresse d’abord à
nos processus inconscients[^24]). Le spectateur peut être
convaincu par les arguments des antipubs, peut-être qu’un temps il
parviendra à analyser les panneaux pubs pour ce qu’ils sont,
s’en énervera comme eux. Sa vision sera celle d’un antipub pendant un
temps. Puis le quotidien reviendra lui effacer cette perspective, parce
que nos processus attentionnels sont très limités et qu’on ne peut pas
tout traiter consciemment en permanence. Le militant antipub aux
stratégies constructives le sait, et le soulage en déboulonnant les
pubs, en les hackant, en les subvertissant, il le libère
ainsi de cette pollution mentale et attentionnelle le
temps d’une action. Il y a là à la fois une
confrontation avec l’adversaire ainsi qu’un soutien
au spectateur car son espace visuel est
libéré. Mais tout cela tombe à l’eau si la pub est remplacée
par une injonction ou une attaque faite au spectateur.

Autrement dit, contrairement aux expériences de l’effet spectateur,
la situation à potentiel militant ou dans laquelle un adversaire cause
un problème a ceci de particulier que&nbsp;:

&#9632; Le spectateur peut ne pas être informé ou ne pas pouvoir voir
les problèmes de la situation. Et cela ne vient nullement du fait qu’il
est «&nbsp;con&nbsp;», ses raisons peuvent être tout à fait légitimes,
normales, banales. Je doute par exemple que les clients
de la Caisse d’Épargne aient pu savoir que dans leur
banque il y avait un tel harcèlement systématisé. Pour
perdurer, la destructivité a besoin de se cacher, de
se travestir, voire se parer d’arguments ou de
promesses qui peuvent être très séduisants.

&#9632; Le spectateur ne peut pas conscientiser en permanence tous les
problèmes. Nos systèmes attentionnels sont vraiment très limités, et
porter conscience sur une chose va demander de supprimer de l’attention
sur une autre. Si on conscientise tout en même temps dans les détails,
on ne peut qu’agir contre un seul des problèmes ou sur une seule
chose à faire. Pire, l’hyperconscientisation peut être tellement
massive qu’on en vient à ne plus rien faire tellement ça nous nous
plonge dans un surmenage mental, ce qui est assez ironique. Tout
comme on ne peut pas porter une centaine de kilos toute la journée en
permanence, nos ressources mentales ne peuvent supporter mille
attentions continuellement avec le même zèle.

&#9632;  Le spectateur peut ne pas avoir les capacités d’agir
ou se sentir impuissant quant au problème
conscientisé. Ça peut être le cas lorsqu’on évoque des problèmes d’ordre
géopolitique ou qui nous paraissent bien éloignés. Soit le
spectateur ressent de la détresse et cela peut le plomber parce
qu’il ne peut rien faire, soit il va éviter le sujet parce qu’il sent
déjà qu’il pourrait être plombé par un sentiment
d’impuissance.

&#9632; Le spectateur peut savoir, être conscient, mais sa situation ne
lui permet pas de s’ajuster (d’ailleurs cela vaut aussi pour le
militant)&nbsp;: par exemple, il peut savoir que les produits bio locaux
sont bien meilleurs pour l’écologie, mais sa situation de pauvreté
l’oblige à devoir rationaliser financièrement le budget
nourriture (et donc acheter prioritairement ce qui est le moins
cher) parce que c’est ça ou ne pas pouvoir payer le loyer, ou
devoir sacrifier le chauffage l’hiver, etc.

&#9632; Le spectateur peut savoir que ce problème gêne certains, mais
il ne l’estime pas pour autant comme un problème
prioritaire. Par exemple, il peut s’en foutre de respecter ou non
l’orthographe et les recommandations de l’Académie (contrairement à un
grammar nazi), parce que ce qui compte pour lui c’est de réussir
à communiquer et à se faire comprendre, et ça marche y
compris lorsqu’il y a des fautes.

&#9632; Être un modèle de vertu ne va pas forcément avoir une
influence positive qui sera imitée. Je précise cela
parce que dans les expériences sur l’effet spectateur, si une
personne va aider, alors tous les autres spectateurs vont sortir de leur
passivité et aider à leur tour comme pour l’imiter. Mais
s’il y a cet effet mimétique c’est parce que la
souffrance est visible, que les spectateurs sentent qu’il y a un
mal-être et ont besoin d’un exemple pour savoir que faire ou
tout simplement s’autoriser à agir. Or, dans les situations à
militance, la souffrance/les problèmes peuvent être en premier lieu
totalement invisibles aux yeux du spectateur, qui ne
saura même pas pourquoi vous faites cela. Je me rappelle
un·e militant·e pour le zéro déchet qui rapportait son agacement
car iel s’était fait·e envoyer bouler après
avoir demandé à un traiteur d’utiliser sa propre
boîte pour contenir les aliments plutôt que de suivre la
procédure habituelle d’emballage du
magasin[^25]. Cette attitude n’est pas perçue comme «&nbsp;à
copier&nbsp;» ni par l’employé (qui peut avoir perçu cela davantage comme une
tentative de contrôle injuste de son comportement professionnel)
ni par les autres clients dans la file (qui voient juste un
autre client qui gaspille trop de temps et les fait
attendre).

Cependant, face à l’adversaire, être un modèle vertueux peut
effectivement être utile pour apparaître cohérent dans son
combat&nbsp;: si on négocie avec les pouvoirs publics pour l’arrêt d’une
politique polluante, mieux vaut ne pas arriver en SUV au premier
rendez-vous, c’est un coup à se décrédibiliser totalement et à
ne pas être écouté.

Certaines situations très particulières peuvent aussi
renforcer l’importance d’être un modèle, notamment celles où
n’importe qui peut faire la connexion entre ce comportement vertueux et
une utilité sociale directe. Par exemple, durant la Première
Guerre Mondiale, André Trocmé[^26], alors enfant,
rencontre un soldat allemand qui lui propose à manger. Il refuse
parce qu’on ne mange pas avec l’adversaire et Trocmé est déjà très
patriote à l’époque (un jeu patriote rejetant toute
interaction avec l’ennemi). Le soldat lui explique en toute
sympathie qu’il n’est en rien un ennemi, car il a refusé de
porter des armes, de tuer ou de faire du mal à qui que ce soit, il ne
s’occupe que des communications. Trocmé est fasciné parce qu’il ne
savait pas cela possible, ils continuent de parler, mangent le
pain ensemble. Ce modèle restera gravé à jamais dans sa mémoire, et
Trocmé participera plus tard avec sa femme et tout le village de
Chambon-sur-Lignon à résister, à cacher et sauver entre 3500 et
5000 Juifs de la mort[^27]; son patriotisme a évolué, et
l’adversaire n’est plus vu dans l’individu, mais dans la destructivité
auquel il peut être allégeant. À noter que la résistance de
Chambon-sur-Lignon a été très particulière, car elle s’est faite sans
chefs ni organisation formelle, simplement par une cohésion tacite.

Être un modèle même modeste dans ses actions, mais dont la
prosociabilité de l’engagement est visible, indéniable pour les enfants,
peut inspirer ceux-ci, pourra peut-être leur donner le
courage de savoir quoi faire face une adversité qu’on n’aurait pas
pu imaginer.

En fait, pour résumer ce débat «&nbsp;faut-il être un
modèle de vertu/de pureté devant les spectateurs?&nbsp;».
Je dirais que cela n’a un impact positif que si les bénéfices
prosociaux qu’apporte le comportement sont directement
perceptibles par autrui (via moins de souffrances, moins de
menaces, plus de relations sociales positives, plus de bonheur,
etc.).

### Que va faire le militant avec ce trio&nbsp;?

En toute logique, on peut imaginer que le militant va donc tenter de
renforcer la cohésion et la diversité des alliés, notamment en accordant
de l’attention positive au spectateur (lui donner des informations
utiles, lui faire vivre des prises de conscience qui l’aident aussi,
prendre le temps de chercher à le comprendre pour mieux
répondre à ses besoins, le libérer, etc.). Il va combattre
l’adversité tant au niveau distal, mécanique, systémique que dans la
recherche d’une prise de conscience chez l’adversaire (ce
dernier comprendrait alors que ce sont les
mécaniques adverses en lui qui l’empoisonnent, et
pourrait donc décider de les abandonner, les transformer,
etc.).

On peut déduire de cette logique trois pans d’actions
(potentiellement cumulables/menés de concert) dans le jeu militant, à
savoir&nbsp;: la confrontation, la construction et l’information.

### La confrontation

Le militant se confronte à l’adversaire. Cela peut se faire via des
négociations, du lobbying, des manifestations tout comme du sabotage, du
hacking, via un combat direct. Dans les mouvements non
violents[^28] du passé, cela a pu se manifester par
l’appropriation de droits qui étaient pourtant interdits à
certaines personnes de façon injuste&nbsp;: par exemple, Rosa Parks s’est
assise dans le bus à une place qui lui était interdite par sa couleur
de peau&nbsp;; les militants afro-américains pour les droits civiques sont
allés dans les restaurants, épiceries qui leur étaient interdits,
etc. La réponse/riposte de l’adversaire a été parfois
épouvantablement violente, frappant les militants, les
emprisonnant, les tuant, mais ils ont tenu bon, ont continué à se
confronter, en vivant, en étant là, dignes et laissant l’adversaire
révéler son vrai visage de haine. La confrontation peut avoir des
facettes très variées, allant d’une furtivité totale de ses
membres (par exemple le sabotage, le hacking) jusqu’à une forte
visibilité publique, elle peut accepter des actions illégales
comme prendre appui sur la loi. Il y a vraiment énormément de
possibilités de se confronter à l’adversaire, toutes très
différentes dans leurs stratégies et buts.

### La construction

Le militant construit un objet, un environnement social, des façons
de faire/de s’organiser, etc. Ce qui est à l’opposé ou radicalement
différent de ce que fait l’adversaire, et se pose comme
concurrence au niveau du bien-être (ce qui est construit cause moins
de souffrances, l’humain y trouve plus de bien-être, de bonheur, etc.).
Par exemple, j’ai découvert parmi les hackers des façons de
s’organiser do-ocratique «&nbsp;le pouvoir à celui qui fait&nbsp;», très
différentes de la façon dont on se structure
traditionnellement dans le monde du travail. Là, n’importe qui
pouvait monter une opération avec un pouvoir de décision
propre à son initiative&nbsp;; ou si untel était connu pour
avoir réussi tel aspect technique de l’opération, il était convié aux
opérations impliquant ces mêmes techniques. Par contre,
on pouvait l’envoyer balader s’il venait faire son chef dans une
opération pour laquelle il n’avait rien fait,
qu’importent ses faits d’armes dans tel autre domaine.
Il y avait là des valeurs anti-autoritaires,
anti-hiérarchiques qui étaient vécues concrètement dans le quotidien et
qui pouvaient être perçues dans la façon d’organiser les actions.

Le groupe militant construit et vit dans des modes
d’organisation qui peuvent aussi constituer une confrontation avec
l’adversaire, dans le sens où cette construction révèle à quel point ces
modèles adverses peuvent être périmés ou malsains,
puisqu’il prouve de fait que l’inverse peut fonctionner mieux,
de façon beaucoup plus humaine et plaisante.

### L’information

C’est la révélation aux spectateurs/futurs alliés du problème, par
exemple le lancement d’alerte, le témoignage, le *leak*…&nbsp;;
cela peut aussi se faire au travers d’ateliers/évènements à des
fins d’éducation populaire, ou à travers toutes sortes
de formes d’enseignement. Dans le milieu hacker (comme dans le milieu du
renseignement), on considère l’information comme le pouvoir&nbsp;: on ne peut
faire certaines choses que si on sait certaines choses. C’est ce qui
fait que chez les hackers la libération de l’information
à tous est considérée comme un empuissantement de la
population et une victoire en soi&nbsp;: c’est par exemple ce qu’a fait
Alexandra Elbakyan avec Sci-hub et qui permet à
n’importe quel chercheur ou personne curieuse d’avoir accès à
quasiment toutes les recherches scientifiques, sans avoir à se
ruiner.

À l’inverse, l’adversaire a souvent des pratiques de
rétention ou de déformation d’infos qui lui permettent
d’avoir un contrôle sur les personnes et les situations.

Manipuler l’information (en donner certaines et pas d’autres, mentir
sur les faits, informer certains et pas d’autres, inventer de fausses
informations, etc.) peut aussi permettre de dominer une personne et la
contrôler d’une façon ou d’une autre, que ce soit pour en tirer plus
d’exploitation de force de travail ou obtenir une allégeance&nbsp;: si je
fais croire à telle information fausse, alors j’obtiens de la légitimité
auprès de ceux qui vont y croire, voire une certaine autorité, donc je
peux mieux les exploiter (c’est ce que peuvent
faire les acteurs d’un mouvement sectaire, par exemple la
scientologie).




<figure>
    <img src="Pictures/scientologie.png"
         alt="En scientologie, chaque niveau pour être « clair » coûte plusieurs milliers de dollars, et consiste en des révélations...">
    <figcaption>En scientologie, chaque niveau pour être «&nbsp;clair&nbsp;» coûte plusieurs milliers de dollars, et consiste en des révélations. Les militants anti-sciento ont donc tout simplement révélé celles-ci&nbsp;: à OT3, pour 158 000 dollars, les adeptes apprennent que Xenu, un méchant extraterrestre a envoyé une partie de sa population dans nos volcans, et comme ça ne suffisait pas, il a aussi balancé des bombes H. Mais il restait des bouts d’extraterrestres, les bodythétans. Le clan de Xenu leur ont implanté des images de la future société terrienne&nbsp;: toutes les religions sont pures illusions inventées par la troupe de Xenu afin de tourmenter à jamais ces bodythétans. Et ces bouts d’extraterrestes plein d’illusions se sont mis en grappe en nous, heureusement la scientologie a la solution pour les virer (mais il va falloir payer). Pour 316 000 dollars, vous apprendrez aussi que Ron Hubbard était la réincarnation de Bouddha, que Lucifer était le représentant de la confédération galactique et que Jésus était homosexuel (la scientologie est homophobe). En principe, après avoir lu ça sans pour autant être scientologue, vous devriez être en combustion spontanée. Plus d’infos sur <a href="https://wikileaks.org/wiki/Church_of_Scientology_collected_Operating_Thetan_documents">wikileaks<a>, <a href="https://whyweprotest.net/threads/leak-ot8-hco-pl-7-4-70rd-hco-b-19-3-71.101594/">whyweprotest</a>, et <a href="https://www.hacking-social.com/2015/04/27/gamification-partie-3-un-exemple-dune-gamification-extreme-et-dangereuse-la-scientologie/">hacking-social</a>. (Image provenant du magazine <em>Advance</em> de la scientologie, dans les années 80).</figcaption>
</figure>


Le travail militant sur la libération de l’information n’est
donc pas juste une façon d’informer/d’éduquer les spectateurs ou
de renforcer la cohésion entre alliés. Il s’agit aussi
de hacker les mécaniques de l’adversaire, d’entraver les façons
dont il tire du pouvoir de domination. Ainsi, même le témoignage le plus
simple révélant comment se déroule dans le détail une journée de travail
dans telle entreprise (indépendamment d’une démarche
militante) donne potentiellement du pouvoir d’agir à celui qui le
lira, parce qu’il aura plus d’éléments pour décider de son comportement
face à cette entreprise. Il y a très longtemps je me souviens d’un
employé bossant à Quick qui avait été licencié et menacé de
procès pour avoir simplement raconté ses journées sur
Twitter[^29], tweets qui évoquaient les
manquements à l’hygiène et qui le choquaient à raison (il y avait
déjà eu un ado de 14 ans mort par intoxication[^30]
après avoir mangé dans ce même restaurant)&nbsp;: c’est
extrêmement révélateur à mon sens des mécaniques de domination
car les entreprises ont besoin de garder le contrôle de
l’information pour perpétuer leur contrôle, maintenir le statu quo,
préserver leur levier d’exploitation, de profit.

Cependant, précisons que nous avons la chance de vivre dans un monde où
l’humanité n’a jamais eu autant accès à l’information, mais que la
contrepartie est que nous sommes constamment bombardés de nouvelles
infos, qu’il y a une très forte concurrence sur le marché de
l’attention.

Autrement dit, il y a un autre enjeu qui se lie à cette
problématique de l’information&nbsp;: la question
de l’attention et des façons de la capter. L’information
même la plus empuissantante est en concurrence avec des vidéos d’animaux
mignons, de divertissement en général, des informations plus
émotionnelles et donc plus attractives (forte colère, drama,
peurs), et des stratégies puissantes de communicants qui
jouent sur le grand échiquier de l’attention avec brio pour
décider de ce qui occupera les esprits à tel moment (voir ce reportage
absolument sidérant, je vous le conseille vraiment vivement&nbsp;: *Jeu d’influences - les stratèges de la communication*, [partie 1](https://www.dailymotion.com/video/x1u8i9i), [partie 2](https://www.dailymotion.com/video/x1v5icc).


Libérer l’information ne suffit plus, il y a aussi tout
un art à développer pour la rendre accessible à tous, attractive,
afin que son potentiel empuissantement et son utilité
sociale soit mis en valeur.

### Le jeu militant déconnant

Les milieux militants jouent en général sur ces trois registres en
même temps, la construction permettant à la fois de renforcer la
cohésion entre alliés et spectateurs, voire à séduire l’adversaire qui va
alors laisser tomber sa destructivité&nbsp;; la mission d’information visant
souvent le soin des alliés ou spectateurs et la diminution du pouvoir de
domination de l’adversaire&nbsp;; et la confrontation se faisant contre les
mécaniques adversaires et ses systèmes.


Alors pourquoi la militance déconnante fait-elle l’exact inverse, jouant un
jeu aux règles inverses&nbsp;?

Pourquoi se confronte-t-elle aux alliés et spectateurs (mais pas à
l’adversaire)&nbsp;? Comme les grammar nazis qui se confrontent à
l’ensemble des gens faisant des fautes, mais jamais ne se
confrontent de manière radicale à la langue et ses
problématiques linguistiques qui pourtant sont directement en cause
dans nos erreurs (erreurs qui sont d’ailleurs
souvent très logiques).

Pourquoi la militance déconnante ne construit-elle rien et préfère
s’attaquer à ceux qui construisent&nbsp;?

Pourquoi la militance déconnante ne libère-t-elle pas
l’information pour empuissanter, mais préfère l’utiliser comme une
massue pour corriger les gens&nbsp;? Comme ceux qui
militent au nom d’une pseudo zététique via des échanges dont
la dynamique est identique à celle des grammar nazis, reprochant
à tout va les biais d’untel ou ses arguments mal construits, sans voir
que les biais et la «&nbsp;mal-construction&nbsp;» des arguments n’a rien d’une
faute, et en dit beaucoup plus sur les attentes, les
besoins, les motivations des personnes concernées et que c’était
peut-être cela qu’on pourrait peut-être prioritairement
regarder.

C’est cela que j’appelle du militantisme déconnant&nbsp;: en combattant
les alliés et spectateurs, en ne construisant rien d’utile, en
n’informant que pour taper, il produit un dégoût pour son sujet. Qui
veut se renseigner, soutenir, joindre quelque chose qui est perçu comme
menaçant&nbsp;? Plus grave encore, cette déconnance laisse tout le champ à
l’adversaire de s’étendre, laisse les causes premières de la
destructivité persister, invisibilise parfois les autres
militances non déconnantes.

Autrement dit, les mêmes objectifs que ceux d’un saboteur, d’un
ennemi au mouvement.


<!-------------- Notes CHAP 1 ---------------->



[^15]: Employer ce mot anglais plutôt que le français «&nbsp;jeu&nbsp;» est plus
    pratique (ici ou ailleurs) parce qu’en français (et d’ailleurs dans
    d’autres langues) nous n’avons pour seul mot que «&nbsp;jeu/jouer/jouet&nbsp;»
    dont l’orthographe est laborieuse pour séparer deux réalités
    totalement différentes&nbsp;: le game (jeu) ce serait le plateau de
    Monopoly, les règles, les buts&nbsp;; le play (jeu) c’est l’élan du
    joueur, sa motivation, son énergie à saisir ce jeu qui peut être
    très variable, comme jouer de manière très conformiste et dogmatique
    (suivre toutes les règles au pied de la lettre, atteindre le but),
    de manière hacker (on change les règles pour que ce soit plus fun,
    moins injuste pour les enfants, etc.), tricheur (faire semblant de
    suivre les règles pour gagner plus facilement), etc. Les façons de
    play peuvent changer le game (ou pas si on le joue conformiste). Et
    ça vaut aussi dans l’emploi de métaphores comme «&nbsp;peser dans le
    youtubegame&nbsp;», qui se réfère au respect du game de YouTube dans son
    play conformiste (=augmenter son nombre d’abonnés/de vues, être en
    tendance, respecter les règles de copyright et les pressions
    implicites à ne pas parler de sujets qui fâchent les annonceurs,
    etc.), mais le play sur YouTube pourrait être différent (faire une
    vidéo antiyoutube en cherchant à ne pas fâcher les annonceurs, en
    respectant le copyright, etc.).

[^16]: Terme provenant de Colas Duflot qui définit le jeu comme
    l’invention d’une liberté *dans et par* une légalité, et cette
    liberté singulière d’employer ce concept pour des structures qui ne
    sont pas du jeu, mais dans le champ de la recherche des
    game-designers portant sur la ludification/gamification, on peut
    constater que les différences entre une structure «&nbsp;sérieuse&nbsp;» et
    une structure de jeu ne sont pas si énormes, l’une se confondant avec
    l’autre, parfois volontairement, parfois involontairement. Sources
    (non exhaustives)&nbsp;: Colas Duflot, *[Jouer et philosopher](https://hal.archives-ouvertes.fr/hal-01628001)*, PUF, 1997&nbsp;; Eric Zimmerman et Katie Salen, *Rules of play. Game Design Fundamentals*, MIT Press, 2003.

[^17]: Oliner (1988)

[^18]: Sauf dans le cas de groupes fascistes, ethnocentriques,
    autoritaires, qui filtrent selon la couleur de peau, l’origine
    ethnique, des caractéristiques physiques. Sauf également dans des
    groupes dogmatiques où il pourrait y avoir une exigence à être selon
    une norme extrêmement stricte, tout du moins d’un point de vue
    comportemental.

[^19]: Je prends ici la question du *benchmark* qui a été mis en place à
    la Caisse d’Épargne, et qu’on peut voir décrite notamment dans ce
    numéro d’Envoyé spécial «&nbsp;[Les patrons mettent-ils trop la
    pression&nbsp;?](https://www.dailymotion.com/video/x2ymwlh)&nbsp;», datant du 28 février 2013.

[^20]: Libération, «&nbsp;[La Caisse d’Epargne Rhône-Alpes condamnée pour avoir mis ses employés en concurrence](https://www.liberation.fr/societe/2012/09/05/une-banque-lyonnaise-condamnee-pour-avoir-mis-ses-employes-en-concurrence-entre-eux_844175/)&nbsp;», 05/09/2012.

[^21]: En captologie, B. J. Fogg dans «&nbsp;Persuasive Technology&nbsp;: Using
    Computers to Change What We Think and Do&nbsp;» rapporte qu’une
    application ludique pour enfants questionnait ceux-ci de temps en
    temps sur les habitudes personnelles de leurs parents. Il s’agissait
    d’obtenir des informations très personnelles en vue de profit, et
    tout cela de façon la moins détectable possible. Vu que c’était
    intégré autour d’un jeu *fun*, impossible pour l’enfant de détecter
    le problème.

[^22]: Youness Bousenna, «&nbsp;[Viol et meurtre, la République selon Sade](https://philitt.fr/2016/04/24/viol-et-meurtre-la-republique-selon-sade/)&nbsp;», *Philitt*, 24/04/2016.

[^23]: Je garde prioritairement le terme «&nbsp;spectateur&nbsp;» plutôt que
    «&nbsp;témoin&nbsp;» ou «&nbsp;tiers&nbsp;», parce que d’une part c’est celui qui est
    le plus utilisé dans ma chapelle qui est la psychologie, et parce
    que d’autre part je trouve qu’il connote davantage la passivité face
    à un «&nbsp;spectacle&nbsp;» et pointe bien du doigt le problème. Le terme
    «&nbsp;tiers&nbsp;» (qui renvoie aussi au spectateur) est davantage utilisé
    dans l’analyse des génocides, notamment en histoire pour décrire
    l’inaction des États voisins alors qu’un génocide est imminent ou
    en cours mais qu’il y a passivité face à ce phénomène, voire un
    déni. J’utilise moins le mot témoin, parce qu’on aurait tendance à
    imaginer que celui-ci va un jour témoigner, ce qui est déjà ne plus
    être passif. Le témoignage peut potentiellement aider une victime,
    voire lancer l’alerte. Or, si aucune autorité ne le leur demande,
    les spectateurs n’apporteront pas leur témoignage pour autant, voire
    peuvent témoigner de façon incomplète pour cacher la passivité dont
    ils peuvent avoir honte a posteriori.
	
[^23b]: Vous pouvez consulter aussi l’expérience elle-même (Darley et Latané, 1968)&nbsp;;
    plus d’infos aussi [ici](https://fr.wikipedia.org/wiki/Effet_du_t%C3 %A9moin) (Wikipédia).

[^24]: Quand je parle de processus inconscients, je ne parle pas en
    langage psychanalytique, mais en termes neuro. On parle ici de
    processus *cognitifs* inconscients. Notre cerveau traite
    l’information même si on n’en a pas conscience, il enregistre les
    stimuli, les associations positives/négatives. Les décisions que
    l’on prend sont également d’abord prises de façon inconsciente avant
    d’arriver dans notre conscience. Les cours de Stanislas Dehaene
    expliquent ceci fort bien, vous pouvez les trouver [ici](https://www.college-de-france.fr/site/stanislas-dehaene/course-2008-2009.htm). Je sais qu’il est «&nbsp;cancel&nbsp;» par certains notamment parce qu’il a bossé avec le gouvernement, il n’en reste pas moins que ses cours
    sont très bien foutus, très sérieux et accessibles et n’ont rien de
    néolibéral ou de macroniste, c’est de la psychologie cognitive et de
    la neuropsychologie.

[^25]: Je ne retrouve plus la source, ça date, mais c’était peut-être
    issu de Béa Johnson dans son livre *Zéro déchet* à moins que ça
    ne vienne d’un témoignage masculin sur twitter&nbsp;; d’où également mon
    emploi de «&nbsp;iel&nbsp;».

[^26]: Dans Magda et André Trocmé, *Figures de résistance*, textes
    choisis par Pierre Boismorand, Cerf, 2008.

[^27]: Les chiffres sont difficiles à estimer.

[^28]: À ne pas confondre avec la notion de pacifisme&nbsp;: les
    mouvements non-violents peuvent détruire également, saboter,
    «&nbsp;s’attaquer à&nbsp;», ou se défendre légitimement...
    Autrement dit, ils peuvent être des «&nbsp;fouineurs à chapeau gris&nbsp;».
    C’est simplement qu’ils refusent de détruire les personnes. Même
    Gandhi, non-violent, disait à ses militants qu’il était OK
    de tuer l’adversaire si celui-ci s’apprêtait à le tuer.
    Il y avait des ateliers de défense physique (et mentale) chez les
    militants non-violents de Martin Luther King. Contrairement aux
    moqueries que je vois passer sur la toile et ailleurs, les
    non-violents étaient badass, n’avaient rien de
    tenants d’un pacifisme moralisateur et lâche se
    refusant à toute prise de risque, à toute confrontation.

[^29]: Le Courrier Picard, «&nbsp;[Le déluge s’est abattu sur l’équipier Quick après l’épisode Twitter](https://www.courrier-picard.fr/art/region/le-deluge-s-est-abattu-sur-l-equipier-quick-ia195b0n154213)&nbsp;», 08/08/2013.
    et aussi [ici](http://www.slate.fr/story/66845/equipier-quick-twitter-plainte-justice).

[^30]: Le Parisien, «&nbsp;[Quick d’Avignon&nbsp;: Benjamin, 14 ans, est bien mort intoxiqué](https://www.leparisien.fr/faits-divers/quick-d-avignon-benjamin-14-ans-est-bien-mort-intoxique-18-02-2011-1321649.php)&nbsp;», 18/02/2011.


