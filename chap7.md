<!------------ CHAP 7 D’autres voies pour trouver d’autres façons de faire ----------->



## D’autres voies pour trouver d’autres façons de faire

Je me suis beaucoup appuyée sur la théorie de l’autodétermination
pour analyser et extirper des solutions alternatives au militantisme
déconnant, mais c’est juste une perspective parmi d’autres qui
pourraient être tout aussi bonnes&nbsp;; ce n’est pas «&nbsp;la&nbsp;» chose à faire ni
«&nbsp;la&nbsp;» perspective qu’il faudrait avoir, bien au contraire. Je l’ai
choisie juste parce que je la trouve à la fois suffisamment précise pour
entrer dans le détail, et suffisamment libertaire pour que chacun
puisse réfléchir *à partir* de lui-même et non
selon ses «&nbsp;règles&nbsp;». Bref, ce n’est pas une théorie qui ordonne, mais
qui laisse le maximum de possibilités et essaye de donner des pistes
d’extension à celles-ci, c’est pour cela que j’aime la
partager. Mais tout est bon pour trouver d’autres sources
d’inspiration.

### Rétro-ingénierie du kiff

On pourrait trouver quantité de solutions, d’alternatives, de façons
de faire, en analysant dans le détail tout ce qu’on adore, tout ce qui
nous a motivés, dans ce qu’on a trouvé de merveilleux et de
mémorable. Il s’agit de faire de la rétro-ingénierie du kiff
pour tenter de le reproduire un jour au travers de nos activités. Un peu
comme le travail de recherche des game-designers lorsqu’ils
cherchent les mécaniques qu’ils voudront reproduire dans leur
jeu&nbsp;: les conseils de la dev du dimanche sont excellents à ce
sujet, et perso je pense qu’on peut les transposer tout à fait à des
domaines qui ne sont pas de l’ordre du jeu, si on cherche à
créer quelque chose qui générera une expérience motivante&nbsp;:

<figure>
    <img src="Pictures/journalbordEP02.jpg"
         alt="Journal de bord EP 02 - Coucou, tu veux voir mon pitch&nbsp;?">
    <figcaption>Journal de bord EP 02 - Coucou, tu veux voir mon pitch&nbsp;? Voir sur <a href="https://www.youtube.com/watch?v=RIvclun0utQ">Youtube</a></figcaption>
</figure>


J’ai aussi énormément aimé les méthodes et les façons de penser que
j’ai trouvées dans «&nbsp;l’art du game design&nbsp;» de Jesse Shell,
«&nbsp;Rules of play&nbsp;» de Katie Salen, et globalement le champ du
game-design me fascine parce qu’il nous apprend comment construire une
structure -- un jeu -- qui va motiver au maximum autrui, lui
faire vivre des expériences mémorables ou des moments sociaux uniques en
leur genre.

### Rétro-ingénierie de l’adversaire et bidouillage

L’adversaire, surtout si sa création domine, a réussi un truc. Le
problème qu’on peut avoir avec lui c’est que sa création génère
de la souffrance, de l’injustice, et/ou sert uniquement des intérêts
personnels. Cependant, sa manière de faire a eu une
puissance d’influence qu’on peut décortiquer et qu’on peut transformer
de façon beaucoup plus profitable. Par exemple, bien que je sois assez
anti-pub, je sais que lire les publicitaires à travers leurs manuels
a été très utile&nbsp;: par exemple, si je veux retenir quelque chose
par cœur, j’emploierais les méthodes qu’ils
utilisent pour que le consommateur mémorise un message. On peut
faire des lectures, des analyses de l’adversaire et déjà commencer à
hacker et transformer ses méthodes. Par exemple, je suis très critique
du *nudge* (manipulation des comportements via
le design de l’environnement), d’autant que le
*nudge* est souvent utilisée sous une idéologie
néo-libérale, mais je le trouve aussi fun et super
intéressant&nbsp;; récemment un doctorant m’a montré son étude[^68]
où il avait hacké le *nudge* pour en supprimer
l’aspect manipulateur et lui substituer
à la place de l’autodétermination. Les résultats ont bien
été là, c’était jubilatoire. Tout est transformable, hackable et la
bonne nouvelle c’est que ce mécanisme de hack et de transformation est
ultra fun à vivre lorsqu’on l’opère, mais aussi lorsqu’on y assiste en
tant que spectateur, cela nous libère totalement de la
colère, de l’énervement, de l’impuissance, et à la place on trouve
du fun et de l’empuissantement.



### Tout plaquer pour créer

On peut totalement laisser tomber certaines formes de militance
(interpersonnelles) pour se consacrer à créer/œuvrer et cela ne fera pas
de nous un moins «&nbsp;bon&nbsp;» militant. On peut se donner pour but de créer
de meilleurs environnements sociaux (être ce super collègue, ce pote à
qui on peut se confier, ce soutien en qui on a confiance…)&nbsp;;
on peut créer ou aider à créer des trucs et bidules ou
évènements funs&nbsp;; on peut être ce mentor qui apprend tout autant qu’il
soutient&nbsp;; on peut être ce spectateur, ce blogueur, ce journaliste qui
voit et repère ce truc qui change la face du monde si on le fait
connaître. Résister, c’est créer[^69]&nbsp;; et créer c’est changer
le monde.

Je sais qu’il y a une espèce de bataille de chapelle
militante entre ceux qui pensent que seule la confrontation va
amener du changement, et ceux qui veulent incarner/créer le changement
dès à présent, accusant l’un l’autre de pas avoir la bonne stratégie (on
accuse celui qui crée d’être lâche et de ne pas faire front à l’ennemi&nbsp;;
on accuse celui qui se confronte à l’adversaire d’être
violent et de ne pas créer le monde qu’il voudrait voir
apparaître), mais en fait tout ceci se superpose, se croise,
interagit et il n’y a pas à se complexer de ne pas être en confrontation
ou de n’être qu’en confrontation (ou de le reprocher aux autres)&nbsp;: au
final, c’est l’interaction en système (parfois invisible) qui est
productrice de transformation et changement positifs dans la société,
sur le long terme.

### Tout empuissanter, y compris sur la base de conflits

Ça se confronte entre alliés, envers les spectateurs,
dans le mouvement, il y a donc à trouver des façons de gérer les
conflits. Et il y aurait besoin d’un mode qui permette de régler
les problèmes en interne sans qu’il y ait par la suite des
décennies de ressentiments de la part des uns et des autres,
prêts à exploser au moindre faux pas. Il y aurait
aussi besoin d’une façon de régler les conflits qui ne soit pas
autoritaire, car généralement les mouvements militants (excepté
fascistes, d’extrême-droite) n’aiment pas trop la justice punitive. Et
l’idéal serait évidemment que cette résolution de conflits soit
productrice d’empuissantement et d’autodétermination.

Bonne nouvelle, des militants ont déjà bossé dessus et ont déjà
établi des tas de protocoles extrêmement empuissantants
permettant de gérer les conflits efficacement, au bénéfice de toute le
monde, permettant en plus de prévenir d’autres problèmes&nbsp;: c’est la
justice transformatrice. Je vous laisse consulter toutes ces ressources
ici&nbsp;:

- transform harm ([transformharm.org/](https://transformharm.org/)), tout particulièrement les sections «&nbsp;curriculum&nbsp;» de chaque thème (*restorative justice, healing justice*, etc.) sont emplies de programmes, d’outils très intéressants.
- Les outils de [Creative interventions](https://transformharm.org/wp-content/uploads/2019/05/CI-Toolkit-Complete-FINAL-2.pdf).
- Le processus de responsabilisation&nbsp;: un outil absolument génial quelle que soit sa position ou son rôle dans le conflit, qu’on ait été cible d’un comportement qui nous a été pénible ou cause de cette pénibilité, voire témoin du problème, ou auteur de l’offense. Je trouve vraiment que c’est un outil qui permet de décider en toute autodétermination ce qu’on souhaite pour la suite, ce qui pourrait rétablir des liens de confiance, ce qui permettrait de réparer la situation. Vous trouverez les ressources en anglais[ici](https://transformharm.org/download/community-accountability-for-survivors-of-sexual-violence-toolkit-2/?wpdmdl=929&refresh=5f2cab37415aa1596762935) (je l’ai traduit [ici](https://www.hacking-social.com/2021/02/08/jr7-justice-transformatrice-le-processus-de-responsabilisation/) également).


### D’autres ressources

J’ai été loin d’être exhaustive dans cet article, on aurait pu
parler de long en large du conformisme et de son pourquoi,
davantage de la réactance, des dynamiques narcissiques qui peuvent aussi
poser problème dans la militance (il n’y a généralement pas d’argent
à gagner dans ces milieux, mais ça peut attirer des profils en quête de
notoriété, de personnes voulant se démarquer et dont l’attitude
peut s’opposer au travail collectif), et j’aurais pu aussi parler de
ce qui y a au cœur des mécaniques militantes fascistes en parlant de
l’autoritaire, du dominateur, etc. Donc, voici d’autres sources
potentiellement inspiratrices pour un militantisme qui affronterait
l’adversaire et augmenterait la cohésion avec les spectateurs et
alliés, en créant, en gueulant, en se posant, en jouant et j’en passe&nbsp;;
ces ressources ne sont pas exhaustives non plus[^70], il y en a
certainement des milliers d’autres.

Dans l’histoire ou la philosophie, témoignages, stratégies et
paradigmes de désobéissance&nbsp;:

1. Magda et André Trocmé, <cite>Figures de résistance</cite>, texteschoisis par Pierre Boismorand, Cerf, 2008.
2. Semelin Jacques et Mellon Christian, <cite>La non-violence</cite>, PUF, 1994.
3. Semelin Jacques, <cite>Sans armes face à Hitler</cite>, Les arènes, 1998.
4. James Haskins et Rosa Parks, <cite>Mon histoire</cite>, Libertalia 2018.
5. La Boétie, <cite>Discours sur la servitude volontaire</cite>, Bossard, 1922 (première édition, 1578) Voir [Wikipédia](https://fr.wikisource.org/wiki/Discours_de_la_servitude_volontaire/%C3 %89dition_1922).
6. Michel Terestchenko, <cite>Un si fragile vernis d’humanité, Banalité du mal, banalité du bien</cite>, La découverte, 2005.
7. Samuel P. Oliner, Pearl M. Oliner, <cite>The altruistic personnality, rescuers of jews in Nazi Europe</cite>, Macmillan USA, 1988 (on [en a fait un résumé ici](https://www.hacking-social.com/2019/03/25/pa1-la-personnalite-altruiste/).
8. Frédéric Gros, <cite>Désobéir</cite>, Albin Michel, 2017.

Autour du numérique&nbsp;:

1. Edward Snowden, <cite>Mémoires Vives</cite>, Seuil, 2019.
2. Amaëlle Guitton, <cite>Hacker&nbsp;: au cœur de la résistance numérique</cite>, Au diable Vauvert, 2013.
3. Nicolas Danet et Frederic Bardeau, <cite>Anonymous. Pirates informatiques ou altermondialistes numériques&nbsp;?</cite>, FYP Édtions 2011.
4. Steven Levy, <cite>L’éthique des hackers</cite>, Globe, 2013.
5. Pekka Himanen, <cite>L’éthique hacker</cite>, Exils, 2001.

De l’activisme&nbsp;:

1. Srdja Popovic, <cite>Comment faire tomber un dictateur quand on est seul, tout petit, et sans armes</cite>, Payot, 2015.
2. Automone A.F.R.I.K.A. Gruppe, Luther Blisset, Sonja Brünzels, <cite>Manuel de communication-guérilla</cite>, Éditions Zones, disponible [en libre accès](https://www.editions-zones.fr/lyber?manuel-de-communication-guerilla).
3. Andrew Boyd et Dave Ashald Mitchell, <cite>Joyeux bordel, tactiques et principes et théories pour faire la révolution</cite>, Les Liens qui libèrent, 2015 (qui est en fait une partie traduite des tactiques
qu’on trouve à [libre disposition ici](https://www.beautifultrouble.org/toolbox/#/)&nbsp;; le livre en anglais est aussi [disponible librement ici](https://www.creativityandchange.ie/wp-content/uploads/2017/06/beautiful-trouble.pdf), ils en ont même fait un jeu à destinations des militants pour s’aider à
construire des actions, [par ici](https://www.beautifultrouble.org/strategy-cards).

<figure>
    <img src="Pictures/TheYesMen.jpg"
         alt="L’activisme façon Yes men">
    <figcaption>L’activisme façon Yes men. Voir <a href="https://www.dailymotion.com/video/xqihuo">cette vidéo</a> et <a href="https://www.dailymotion.com/video/xqihrr">celle-ci</a>.</figcaption>
</figure>




<!------------ notes CHAP 7  ----------->


[^68]: Toussard (2020).

[^69]: Citation de Stéphane Hessel.

[^70]: Non pas parce que je suis de mauvaise volonté ou que je
    censurerais des ouvrages, mais simplement parce que je suis juste
    une personne qui n’a qu’une poignée d’heures par jour à disposition,
    donc je ne peux pas tout lire, tout étudier. Je précise cela parce
    que j’ai déjà eu des militants me reprochant de ne pas citer untel
    ou untelle.

