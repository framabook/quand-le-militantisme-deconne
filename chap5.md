<!------------ CHAP 5 D’autres causes du militantisme déconnant ----------->



## D’autres causes du militantisme déconnant

### Le surmenage

Quand on est surmené, on essaye de régler les problèmes au plus vite
pour en traiter d’autres plus urgents, donc il est totalement logique
qu’on en vienne à être plus sec dans notre ton, qu’on ait plus tendance
à l’injonction pour obtenir de l’autre un comportement immédiat afin
qu’il cesse de nous solliciter. Le problème ce n’est ni nous, ni l’autre
qui sollicite ou fait un truc pour lequel on va l’injonctiver en
réaction, mais bien la situation de surmenage. Or, c’est extrêmement
courant en militance, parce que les mouvements n’ont pas souvent les
moyens de gérer tout ce qu’il y a à gérer, parce que la militance mène à
affronter des situations particulièrement surmenantes, stressantes,
parfois oppressantes et violentes. Et même lorsque la situation
surmenante est loin derrière, il y a toujours cette menace qu’elle
revienne sous peu, d’autant qu’elle laisse souvent des traces. En
conséquence, notre cerveau maintient ce mode «&nbsp;sous tension&nbsp;» par
prévention, parce que cela s’est avéré être une manière efficace de
gérer le moment tendu.

Autrement dit, dans ce cas de figure ce n’est ni la faute du militant,
ni de l’allié qui faute ou qui aurait un comportement qui va générer une
critique, mais bien un problème situationnel qui demande des solutions
organisationnelles. La situation d’urgence, de surmenage peut être
inévitable, en ce cas, l’idéal est d’avoir un mode de fonctionnement
préétabli pour ces situations particulières, et d’autres modes de
fonctionnement pour les autres situations. Ce n’est pas forcément
incohérent d’avoir un mode plus «&nbsp;hiérarchique&nbsp;» dans une situation de
forte confrontation avec l’adversaire, avec des règles plus serrées,
parce que la violence ou les risques peuvent obliger à cela, et parfois
le rôle donné à chacun dans un groupe peut avoir des effets
protecteurs&nbsp;; parmi les hackers, j’ai pu voir à l’œuvre à la fois un
mode quasi-militaire lors d’opérations risquées impliquant beaucoup de
monde, avec des instructions très strictes qui ne laissaient pas de
place à de l’initiative personnelle, parce que c’était à la fois le moyen de
mener à bien l’opération et de protéger tout le monde de
risques très concrets. Mais dès que l’opération était terminée,
l’autogestion sans chef, do-ocrate (le pouvoir à celui qui fait/initie
un projet), anti-autoritaire, reprenait le dessus pour fomenter de
nouvelles opérations. Il s’agit de pouvoir switcher, être flexible dans
l’organisation et dans les modes d’agir afin de coller aux besoins
particuliers de la situation, et ne pas rester en mode «&nbsp;menaces&nbsp;»
lorsque celles-ci ne sont pas présentes.

Quoi qu’il en soit, le surmenage et les dérives que cela entraîne ne
peuvent être résolus que par des modes d’organisation qui sont pensés en
fonction des situations. Cela n’est pas un problème qui peut être résolu
en se focalisant sur un individu «&nbsp;fautif&nbsp;».

### Le manque d’information

Pour reprendre l’exemple de «&nbsp;vous connaissez PeerTube&nbsp;?&nbsp;» qu’on a eu
des centaines de fois, c’était saoulant non pas parce que les gens
l’étaient, mais parce qu’il leur manquait l’information que nous étions
déjà partisans de PeerTube, que nous avions déjà nos vidéos sur des
instances, que des dizaines d’individus avant lui n’arrêtaient pas de
nous le dire, et qu’ils ne devinaient pas eux-mêmes qu’il leur
manquait ces informations. Et si nous l’avions répété sans cesse, nous
aurions été nous-mêmes saoulants, c’est pourquoi nous ne l’avons pas
fait. J’ai vu aussi le même genre de problème chez des individus
participant à des formes de *cancel culture*[^59] -- malgré eux&nbsp;: ils se
permettaient une certaine agressivité se pensant seuls dans les
commentaires à avoir ce ton et ne se rendant pas compte qu’ils
contribuaient à rejoindre une meute qui attaquait déjà de toutes parts
sur le même ton.

Avant de conseiller, ordonner, critiquer, s’énerver contre quelqu’un ou
un groupe, on pourrait tenter de s’informer au préalable des positions
de la personne qu’on cible, en regardant ce qu’elle a pu déjà répondre
par le passé à ce sujet, si elle a parlé de ses positions quelque part,
si elle n’a pas déjà fait ce qu’on voudrait qu’elle fasse, etc. Parfois,
cela suffira à combler le manque d’informations et il n’y aura pas
besoin d’interpeller la personne (par exemple, on pourra voir qu’elle
connaît déjà PeerTube ou qu’elle a déjà exprimé son choix pour/contre en
public).

Il s’agirait avant toute interaction de partir du principe qu’on ne sait
pas d’emblée les positions des personnes, leur savoir ou leur ignorance
d’un sujet, mais d’enquêter avant.

Cela peut fonctionner en situation où l’on initie l’interaction avec un
autre sur le net, comme dans une situation où l’on est attaqué par un
autre. Même si on repère que l’autre veut par exemple nous humilier ou
nous écraser, on peut partir du principe que ce n’est peut-être pas ça,
et tout simplement poser des questions pour bien comprendre sa
position[^60]. Par exemple «&nbsp;*Vous me dites que d’avoir mis le mot
«&nbsp;bonheur&nbsp;» dans ce titre est odieux et insupportable, quel est l’élément
associé à bonheur qui vous parait odieux&nbsp;?*&nbsp;» et on cherche à comprendre
ce qui a éveillé le sentiment négatif chez l’autre, on enquête sans
jugement ni défensivité. Cela peut lever pas mal de malentendus et
pacifier l’échange.

Sur Internet, le manque d’informations c’est aussi l’absence de langage
non verbal (absence du ton de la voix, des mimiques de visage, des
gestes du corps, etc.). Ainsi, on a tous un déficit d’informations
parfois énorme sur l’état émotionnel dans lequel a été posté un message
et dans quelle visée. Et encore une fois, on oublie totalement qu’il
nous manque quantité d’informations pour interpréter ce message parce
qu’IRL, lorsqu’on est neurotypique, on a l’habitude d’avoir toutes ces
informations automatiquement sans qu’on en ait conscience. Sur la toile,
on va alors avoir le même réflexe et interpréter le message
automatiquement, en voyant une offense dans une ironie, en voyant de
l’ironie dans un message pourtant sérieux, etc. Pour pallier ce manque
d’informations non verbales, on va se concentrer sur d’autres indices
tels que la ponctuation, y plaquant un sens qui n’est pourtant pas celui
du locuteur. D’autant que l’usage et la connotation des ponctuations
varient selon des facteurs socio-culturels, tels que l’âge de la
personne&nbsp;: les boomers pourront avoir tendance par exemple à terminer
tous leurs tweets d’un point, selon l’usage «&nbsp;académique&nbsp;» qu’ils ont
profondément intériorisé, sans exclamation ni smiley[^61], ce qui pourra
donner l’impression, selon le propos tenu, à un ton brutal, voire un
mode passif-agressif, alors qu’il s’agissait parfois tout simplement
d’une volonté de soigner son écriture, sans froisser son interlocuteur.
Même chose pour l’usage des points de suspension dans un message, qui
pourra être utilisé différemment et suggérer de multiples
interprétations contradictoires… On se focalise sur ces petits
détails, car on cherche une substitution à ce langage non-verbal qui
nous manque cruellement. S’ensuivent donc quantité de malentendus de
toutes parts.

Là encore, on peut prévenir la situation en étant très explicite
lorsqu’on s’exprime, avec tout ce qu’on a disposition (smiley,
formulation de politesse, soin aux styles de la phrase, mots, expression
explicite de son émotion/son état/ses buts, etc.).

Ou encore lorsqu’on est l’interlocuteur, demander des précisions sur le
message, poser des questions jusqu’à être sûr de bien comprendre, avant
de juger son but. Ça peut paraître long dit comme ça, mais en fait poser
une question ce n’est parfois qu’une seule phrase. Et parfois la réponse
suffit à se faire une idée.


<figure>
    <img src="Pictures/Ambassadeur.jpg"
         alt="Abassadeur">
    <figcaption>«&nbsp;Ambassadeur&nbsp;: Honte et fierté mélangées. Nos ennemis nous ont appelés «&nbsp;tanks vivants&nbsp;». Ainsi que par des noms moins flatteurs.&nbsp;» On peut même s’amuser à utiliser la méthode Elcor (dans les jeux Mass Effect, les Elcors sont des êtres qui ne peuvent partager une communication non-verbale avec les autres espèces, ni même faire transparaître leurs émotions dans leurs voix&nbsp;; pour pallier ce manque, ils commencent systématiquement leur propos par un mot qui donnera la bonne teinte émotionnelle à leur discours). D’autres exemples <a href="https://www.youtube.com/watch?v=8eJt1GBon7o">ici</a>.</figcaption>
</figure>

### La réaction à la notoriété bizarre du Net&nbsp;: les relations parasociales

C’est un terme qui a été formulé en 1956 par Horton et Wohl pour décrire
les relations unilatérales d’un·e artiste avec son public&nbsp;: les
spectateurs peuvent se sentir comme amis avec ceux-ci, donc croire tout
connaître de lui, alors qu’en fait non. Aujourd’hui, ce type de
relations est encore plus répandu parce qu’on peut tous être cet
«&nbsp;artiste&nbsp;» qui envoie ou partage du contenu avec une communauté qui le
suit.

D’une part, la personne qui une petite ou grande notoriété sur le Net ne
sait rien de vous et ne peut rien déduire de votre comportement habituel
(par exemple, elle ne peut pas savoir que lorsque vous employez des
injures, c’est du second degré ou une marque d’amitié&nbsp;; elle ne sait pas
que vous êtes peu versé dans les formules de politesse mais néanmoins
cordial), elle peut donc difficilement interpréter des remarques qui
seraient à double sens, encore plus sans avoir accès à votre langage non
verbal pour comprendre. Le militant déconnant peut croire que cette
personne à notoriété va parfaitement le comprendre, qu’il est sympa
d’office, qu’importe le style du message, parce que lui, il la connaît
bien mais oublie qu’elle, elle ne le connaît pas du tout. Et là peuvent
se créer de très forts malentendus.

D’autre part, en tant que spectateur, bien qu’on ait ce sentiment de
familiarité avec la personne à notoriété, on ne la connaît pas du tout&nbsp;:
on ne peut pas savoir si elle est en dépression ou si elle traverse une
phase difficile, elle peut très bien partager quelque chose de sombre
tout en étant dans une situation joyeuse dans son quotidien, tout comme
partager de la joie en broyant du noir. Là encore, avant d’entamer une
démarche qui risque potentiellement d’être dure à digérer pour l’autre,
on peut poser des questions, «&nbsp;tâter le terrain&nbsp;» pour savoir si c’est
le bon moment ou non de parler de telle chose&nbsp;; on peut aussi se
rappeler qu’on ne connaît la personne qu’à travers son
travail/œuvre/partage, pas sa vie tout entière qui peut être
radicalement différente. Même des vlogs réguliers qui pourtant
renseignent sur la vie de la personne sont sélectifs, ne sont qu’un
aperçu de sa vie, ce qu’elle accepte de montrer. Tout comme on ne peut
déduire le bien-être d’un vendeur de sandwichs à la qualité dudit
sandwich (qui peut par exemple avoir été cuisiné sous une pression
énorme), on ne peut pleinement déduire l’état d’esprit d’un partageur à
son seul partage. Pour connaître un peu le milieu, je dirais que lorsque
vous vous adressez un partageur/créateur sur le Net, il est probable
qu’il est en dépression, en burn-out ou surmené, qu’importe la vivacité
dont il peut faire preuve dans ses œuvres. Il serait plus prudent
d’éviter de partir du principe qu’il peut encaisser toutes les
récriminations.

L’autre aspect de cette relation parasociale, c’est que parfois, les
spectateurs confondent ces petites célébrités du Net avec les célébrités
classiques&nbsp;: c’est-à-dire qu’ils partent du principe que la notoriété
est accompagnée d’un statut supérieur (plus de pouvoir, plus de
possibilités, plus d’argent, plus de moyens, plus d’influence, etc.),
donc qu’elles auraient en quelque sorte pour devoir d’utiliser ce
trop-plein de privilèges qu’il leur serait offert, notamment pour vanter
ou exercer une pureté militante. Or, même des gens qui ont une forte
audience sur le Net peuvent n’avoir aucun privilège matériel par rapport
au spectateur moyen, peuvent toujours être salarié smicard, au chômage,
voire dans des situations de grande pauvreté, de sérieuses difficultés. Et
vous n’en saurez généralement rien.

Cependant je comprends, ça peut être trompeur qu’une petite célébrité
sur le Net en galère au quotidien puisse avoir le même nombre de
followers[^62] qu’une petite célébrité de la télévision qui elle, peut
avoir des moyens plus importants, le soutien d’une structure, des
relations qui la mettent à l’abri, etc. Bref, la notoriété du Net doit
être déconnectée dans nos représentations des privilèges, car la
notoriété sur la toile n’est pas synonyme d’avantages matériels ou
sociaux[^63].

### La suspicion d’infiltrés/d’ennemis

L’infiltration dans un groupe militant est malheureusement une pratique
existante, d’autant plus sur le Net où il est souvent facile de
rejoindre le Discord d’un autre groupe militant pour glaner des
informations ou pour troller en interne (ce que l’on peut retrouver par
exemple dans des groupes politiques fortement engagés, notamment entre
fascistes et anti-fascistes). La suspicion d’infiltrés (ou la présence
effective de ceux-ci) peut nous faire nous méfier des alliés,
des spectateurs, et nous mettre en mode paranoïa. C’est un cercle vicieux
terrible, et j’avoue que je n’ai pas la solution contre cela d’autant
que je l’ai malheureusement déjà vécu dans certains mouvements (présence
réelle d’infiltrés professionnels, confirmée par des leaks découverts
plus tard et publiés dans certains médias). L’idée serait peut-être de
se concentrer davantage sur les actions qui sont proposées, de les évaluer
au regard du mouvement et des buts de celui-ci, ce qui
permettrait d’éviter des catastrophes. Les infiltrés ou individus
malveillants auront tendance à diviser, créer des conflits internes,
proposer de s’attaquer aux alliés et spectateurs, chercher à obtenir des
postes à pouvoir de décisions, épuiser les éléments les plus doués,
proposer des actions honteuses/inefficaces qui ne permettent pas de se
confronter à l’adversaire. Donc, ce n’est pas tant qu’il faudrait le
traquer pour le virer, mais davantage prendre soin des alliés, des
spectateurs car c’est une politique plus puissamment établie&nbsp;: ces
projets saboteurs ne seront alors pas suivis parce qu’ils apparaîtront
incohérents, inadaptés.

La suspicion qu’il y ait des infiltrés ou qu’untel ait des projets
malveillants ou potentiellement destructeurs pour le groupe (par exemple,
un membre qu’on pense vouloir nuire au mouvement suite à un conflit mal
résolu en interne, ce qui arrive assez fréquemment&nbsp;: tout militant
d’expérience aura sans doute en mémoire l’exemple d’un ancien camarade
qui, sous l’effet du ressentiment, a pu se mettre à saper activement un
mouvement ou à vouloir nuire à ses membres) peut également n’être qu’une
simple suspicion qui s’avérera plus tard infondée, et ça serait dommage
que l’activité militante soit détournée juste parce qu’on est en mode
méfiance et qu’on a peur des menaces internes. Cependant, là aussi, je
pense qu’on peut tenter d’éviter les problèmes en se concentrant sur les
actions au cœur du mouvement, celles qui sont les plus concrètes et les plus
cohérentes.

<!------------ notes CHAP 5 ----------->



[^59]: «&nbsp;Cancel culture&nbsp;»&nbsp;: «&nbsp;pratique qui consiste à dénoncer des
    individus (ou structures) dans le but de les ostraciser&nbsp;». Plus
    d’infos sur [Wikipédia](https://fr.wikipedia.org/wiki/Cancel_culture), ou sur [Neonmag](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html).

[^60]: Ici, je me base sur les pratiques et méthodes de Carl Rogers,
    psychologue humaniste qui visait l’empuissantement et
    l’autodétermination des personnes, tant dans des contextes
    thérapeutiques, de groupes aux buts divers (académique, religieux,
    politique à visée de résolution de conflits, etc.). Ces écrits sont
    particulièrement accessibles, y compris pour les personnes non
    formées à la psychologie, notamment ses ouvrages *Liberté pour apprendre*, *Le développement de la personne*.

[^61]: Si vous êtes acolyte des illustres de l’académie française, vous
    devez dire «&nbsp;binette&nbsp;» ou «&nbsp;frimousse&nbsp;» pour désigner [un smiley](https://www.academie-francaise.fr/smiley).
    

[^62]: Follower = «&nbsp;acolyte des illustres&nbsp;» si votre allégeance va à
    l’Académie française, quoique je pense qu’elle se fout un peu de la
    gueule des personnes utilisant Internet, voire de la population tout
    court, quand on voit qu’elle a rejeté l’usage commun du masculin
    pour «&nbsp;covid&nbsp;» à la grande joie des Grammar Nazis qui auront une
    occasion supplémentaire de corriger leurs interlocuteurs.
    Voir [l’explication de l’académie sur cette traduction](https://www.academie-francaise.fr/followers)&nbsp;; on pourrait dire «&nbsp;abonnés&nbsp;» mais il me semble que cela reste trop associé à l’image de
    quelqu’un qui a acheté un abonnement pour accéder à un contenu. Le
    terme «&nbsp;adepte&nbsp;» est utilisé aussi par bing, mais là encore il me
    semble que cela nous renvoie à une image erronée du follower (qui
    n’est pas forcément partisan du contenu suivi, encore moins fidèle à
    lui comme il le serait d’une religion).

[^63]: Une étude sur les vulgarisateurs le montre bien&nbsp;: «&nbsp;Frontiers. French Science Communication on YouTube&nbsp;: A Survey of Individual and Institutional Communicators and Their Channel Characteristics    Communication&nbsp;», [frontiersin.org](https://www.frontiersin.org/articles/10.3389/fcomm.2021.612667/full)&nbsp;;
    ou en vidéo&nbsp;: [Analyse des vulgarisateurs scientifiques
    sur Youtube](https://www.youtube.com/watch?v=T6UzuDaPvqA)&nbsp;; ou dans ce
    thread&nbsp;: «&nbsp;On a analysé plus
    de 600 chaînes et 70 000 vidéos de vulgarisation scientifique en
    français, et complété cette analyse par un sondage auprès de 180
    youtubeurs. Nos résultats (avec \@SciTania \@MasselotPierre
    \@tofu89) viennent d’être publiés dans Frontiers in
    communication&nbsp;», [Stéphane Debove sur Twitter](https://twitter.com/stdebove/status/1394322535244869636)&nbsp;; par exemple seul 12 % des vulgarisateurs (sur 600 chaînes
    françaises) gagnent plus de 1000 euros par mois, 44 % ne gagnent
    rien du tout.

