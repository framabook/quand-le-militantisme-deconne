<!-------------- INTRO Quand le militantisme déconne ---------------->

# Quand le militantisme déconne&nbsp;: injonctions, pureté militante, attaques...



Si vous cherchez un article à charge contre le militantisme en
général, je me dois de vous prévenir d’emblée, ce ne sera pas le
cas ici&nbsp;: j’ai été militante à trois reprises, dans des
milieux radicalement différents (en syndicat, parmi des hackers
*grey hat*[^1], parmi des youtubeurs) et ces
trois expériences ont été mémorables à bien des titres. J’ai appris
énormément auprès des autres militant·es, dans
l’action, même dans les moments les plus pénibles,
comme ces moments de confrontation avec
«&nbsp;l’adversaire&nbsp;» (celui qui représentait/défendait le
maintien des problèmes structurels pour lesquels
on luttait). J’ai eu des opportunités de faire des choses que je
ne pensais jamais pouvoir faire dans ma vie. Cela m’a
prouvé que même si l’on est officiellement «&nbsp;sans
pouvoir&nbsp;», en fait si, on peut choper un pouvoir d’agir, pour
transformer les choses, et ensemble ça peut marcher, avoir des
effets conséquents. Jamais je ne regretterais d’avoir participé
à tout ça, connu de telles expériences, même avec
tous les aspects négatifs qu’elles ont pu avoir,
que ce soit à travers les pressions, les déceptions, les
difficultés, la violence et la paranoïa, les faux pas&nbsp;: parce
qu’on était là, ensemble, et on sortait de l’impuissance, on créait
quelque chose qui transformait un peu les choses, qui comptait, qui
était juste.

J’ai aussi une énorme sympathie pour les milieux militants que je
n’ai pas expérimentés. Par exemple, je n’ai jamais
milité directement pour le libre, mais j’ai toujours eu plaisir
à découvrir les nouveautés libres, à les tester, à les
adopter parfois. Et ça vaut pour tout un tas de mouvements très
variés.

Pourquoi cette sympathie et gratitude générale pour les
militants&nbsp;? Très sincèrement parce que leurs actions
répondent parfois à mes besoins (psychologiques ou
non) et ont une résonance particulièrement
empuissantante que j’aime ressentir, que ce soit
grâce à l’astuce d’une chimiste écolo pour fabriquer
ses produits cools, les crises de rire
devant le Pap 40, l’appréciation esthétique qu’offre le travail
souvent engagé de Banksy, le réseau social libre qui me
laisse plus de caractères que Twitter, la
sororité féministe qui m’a permis d’éviter mes foutus biais
d’internalité, l’accès à l’info rendu possible grâce
par des _grey hat_ qui littéralement me permet de travailler
pleinement au quotidien (Merci Aaron Swartz,
Alexandra Elbakyan, Edward Snowden, et tous les
inconnu·es qui bidouillent dans l’ombre à libérer
l’info), etc. Et si tout cela répond à mes besoins,
résonne, m’augmente, aide à me développer, m’offre des solutions ou
m’ouvre l’esprit à d’autres solutions, cela doit avoir cet effet
bénéfique chez d’autres. Et c’est effectivement le cas, quand on
étudie la militance sous une perspective historique, à
travers les décennies.

<figure>
    <img src="Pictures/pap40.png"
         alt="Le pap’40 en action">
    <figcaption>Le pap’40 en action (<a href="https://www.dailymotion.com/video/x1l4bqm">sur dailymotion</a>)&nbsp;; sa <a href="https://www.youtube.com/user/EgliseTSC">chaîne Youtube</a></figcaption>
</figure>


Là, le bénéfice est à un autre niveau que je qualifierais de
totalement épique&nbsp;: les changements sociaux proviennent toujours
d’abord de collectifs qui militent pour faire avancer
les choses.

On découvre que cette puissance de l’action militante
résonne à travers les siècles, on peut la sentir lorsqu’on
étudie l’histoire des Afro-américains et la conquête de
leurs droits, l’histoire des résistances sans armes durant la
Seconde Guerre Mondiale, l’histoire du
féminisme, l’histoire LGBT, l’histoire des
hackers et bien d’autres mouvements&nbsp;!

Tout, de la petite info à la petite innovation, du mouvement massif
à l’action solitaire la plus risquée, nous offre un
espoir de dingue&nbsp;: on peut changer le monde en mieux, on peut le
faire ici et maintenant, on peut avoir ce courage, et ce
qu’importe la taille de la destructivité contre laquelle
on lutte, que ce soit contre un régime autoritaire,
contre une situation de génocide, d’esclavage, de
manipulation, de crise, d’oppression, de dangers… On peut
tenter quelque chose pour que cette résistance
fonctionne au mieux, même si on n’a rien.
Et on peut le faire avec une arme de compassion massive qui
irradiera pour des générations entières. Franchement, c’est à pleurer de
joie et de soulagement prosocial que de lire les histoires des
sauveteurs durant la Seconde Guerre Mondiale,
c’est ouf la force de construction que transmettent les écrits de
Rosa Parks, Martin Luther King, et tant
d’autres.

J’ai tellement de gratitude pour toutes ces personnes, pour
quantité de mouvements, c’est pourquoi je tente d’en parler quand je
le peux, sous l’angle des sciences humaines&nbsp;: je suis
partageuse de contenus sur le Net à travers
Hacking-social/Horizon-gull depuis plus de 7
ans avec Gull, d’une façon engagée. On vulgarise les
sciences humaines d’une façon volontairement
non-neutre, c’est-à-dire en prenant
parti contre tout ce qui peut détruire, oppresser, exploiter,
manipuler les gens, et en partageant tout ce qui
pourrait aider à viser plus d’autodétermination.

L’œuvre des militants, des activistes et autres
engagés fait donc partie de ma ligne éditoriale
depuis la création du site HS.

Mes angles éditoriaux se sont transformés au fil du
temps, d’une part pour une raison assez positive qui
est une addiction de plus en plus prononcée pour fouiner dans la
littérature scientifique, et d’autre part parce que j’ai commencé à
rechigner à parler des combats militants
actuels, quand bien même je les soutenais, et parce que j’avais plein de
choses positives à dire.

Progressivement, j’ai opté pour des compromis, comme parler
de militance uniquement si celle-ci avait pu
être étudiée au travers de recherches scientifiques
en sciences humaines et sociales. C’est ce que j’ai pu faire d’ailleurs
en toute tranquillité avec la justice transformatrice,
qui aborde en partie le travail de militants
abolitionnistes du système pénal, et qui a été bien
accueilli sans doute parce que ce n’était pas un
sujet ni d’actu, ni français. Ça n’a hurlé que
très peu, et seulement sur un réseau social pour lequel je
m’attendais à ce genre de réaction épidermique.

Je me rends compte aussi que plus les années
ont filé, moins j’ai repartagé de contenus divers sur les réseaux
sociaux (tout confondus), moins j’ai fait de posts sur ceux-ci,
moins j’ai osé m’exprimer, poser des questions, faire des
remarques, etc. Actuellement, je constate que je ne partage que
des infos liées à nos thèmes habituels (alors
que je croise tout un tas de contenus que je pourrais
partager). Parfois, je n’informe même pas de
notre activité sur *certains* réseaux sociaux parce que
je n’ai pas l’énergie/la patience de gérer certaines
réactions&nbsp;: l’énervement à cause d’une
image/ de l’entête pour des raisons qui n’ont rien à
voir avec l’article&nbsp;; le mépris parce que j’ai
posté ça aussi sur une plateforme impure
liée aux GAFAM[^2]&nbsp;; la colère parce
que le sujet principal de l’article ne portait pas
précisément sur celui que certains auraient voulu&nbsp;;
le dégoût parce qu’il y a un anglicisme/un
néologisme&nbsp;; les injonctions à mettre des
TW[^3] ou autres balises alors que j’ai consacré des
paragraphes complets à faire des avertissements dans
l’article&nbsp;; la condescendance pour la recherche/la
statistique/l’expert·e citée, car elle a été pointée du doigt
un jour par un tribunal
pseudo-scientifique/pseudo-zététicien/pseudo-sceptique
(que je prenne en compte pendant X pages de critiques
scientifiques et que je les discutent ne compte pas, j’aurais
dû ne pas en parler tout simplement, car y faire
référence c’est déjà trop)&nbsp;; le rejet par un
tribunal grammar-nazi-académique parce que j’ai mis mes
sources bibliographiques à la fin de l’article et non
dans le corps de texte, etc.



Et dans ma tête, il y a ainsi une liste noire
qui grandit au fur et à mesure, de sujets à ne pas ou ne plus
aborder sur le Net, quand bien même je les estime
avec amour ou qu’ils comptent à mes yeux, que je
sens que ça pourrait être utile à autrui, parce que je sais que cela ne
sera pas entendu de la sorte, et que, avouons-le, je n’ai pas le courage
d’encaisser ce qui va s’ensuivre. J’ai autocensuré notamment
des problématiques militantes pour lesquelles je suis directement
concernée, parce que lisant comment cela se passait pour
d’autres sur les réseaux sociaux, en découvrant la
pureté requise pour pouvoir en parler, et la
méfiance qu’on aurait d’emblée à mon égard de part mes
«&nbsp;endogroupes&nbsp;»[^4] si je les révélais, je
sais d’expérience que les attaques
toucheraient trop à ma personne et
que, sous le feu de l’émotion, je serais capable de
m’auto-annuler, c’est-à-dire
détruire mon existence sur le Net, et mon
travail avec. Le fait d’avoir vu tant de collègues détruits
de la sorte, alors qu’ils avaient un propos
doux, juste, socialement tellement utile,
créatif, me confirme malheureusement que cette liste
noire de sujets, je dois la maintenir pour l’instant si je
veux continuer à faire ce que je fais.



«&nbsp;Encore un boomer qui crie "on peut
plus rien dire gnagnagnaa" parce que des militants
soulignent ses erreurs et qu’il ne veut
pas les assumer&nbsp;!!&nbsp;» pourriez-vous me dire&nbsp;; je
comprends tout à fait qu’on puisse avoir ce réflexe quand quelqu’un se
plaint du militantisme-correcteur du Net. Mais cette
liste noire qu’est la mienne ne veut pas dire que
j’estime que je ne peux plus rien dire. Elle veut surtout dire que
j’ai dû développer au fil du temps des stratégies et des
hacks plus ou moins sournois non pas pour pouvoir
m’exprimer sans me prendre des reproches plein la tête, non
pas pour convaincre[^5], mais pour ne pas être
dégoûtée de mes propres engagements à
cause de quelques militants qui partagent
ces mêmes engagements. Pour le dire plus
simplement&nbsp;: j’ai peur du jugement des alliés.

Je n’ai pas d’angoisses
particulières vis-à-vis des individus que je
sais «&nbsp;adversaires&nbsp;» de ce dont je vais parler. Par
exemple, je suis assez détendue sur le fait de parler de
l’autoritarisme comme quelque chose de problématique et
j’attends parfois avec impatience les défenseurs
de l’autoritarisme pour discuter avec eux. Comme je ne cherche
pas à convaincre, je n’ai absolument pas de problème à ce
qu’ils rejettent le contenu avec véhémence, ce qui
est d’ailleurs plutôt cohérent de leur part. Je
peux même être admirative quand ils arrivent à
exprimer pleinement, sans complexes, ce qui les gêne, parce qu’ils
s’affichent sincèrement avec leurs valeurs, quand bien
même elles sont parfois horribles (au hasard, en appeler à
tuer les 3/4 de la population ou envoyer en enfer les gens
comme moi). Et dans cette confrontation sincère, je
récolte des informations précieuses pour de
futurs contenus (les adversaires véhéments sont au fond
d’excellents contributeurs *involontaires*).

Par contre, c’est vraiment dur de se prendre de la haine de la part
d’individus qui, je le constate après discussion, ont les
mêmes valeurs que celles diffusées dans le contenu
qu’ils blâment, ont les mêmes engagements, les mêmes
objectifs. Ils me crient dessus parce que je n’ai
pas parlé de ceci ou de cela, que j’ai utilisé un mot qui ne
leur plaît pas, un en-tête qui aurait dû d’une
manière ou d’une autre contenir tout l’article, parce que je
n’ai pas utilisé un mot ou cité une référence qui leur
paraissait indispensable, etc. Bref, ces reproches
et cette colère de la part d’alliés, de camarades,
de confrères, j’ai eu beaucoup de mal à les digérer et à les
comprendre, d’autant plus qu’ils sont advenus pour des
sujets auxquels je ne m’attendais pas
le moins du monde.

Un jour j’ai parlé par exemple d’une
expérience d’éducation alternative[^6]. Je me suis
centrée sur ce qui s’y faisait et était
particulièrement cool dans le développement de l’autonomie et de
l’empuissantement de l’enfant, au top vis-à-vis
de ce qu’on sait en psycho- et neuro-. Puis j’ai
fait ce que j’estime à présent une erreur, j’ai repris
toutes les critiques disponibles à l’encontre de
cette expérience afin de les debunker. C’était du
débunkage ultra simple, puisque les 3/4 des critiques
portaient sur des éléments qui n’étaient même pas présents
dans cette expérience ou encore s’attaquaient
personnellement à la personne qui l’avait menée sous forme de procès
d’intention, or ces caractéristiques n’avaient strictement aucun
lien avec l’expérience elle-même. Plus important à mes
yeux, les critiques vantaient parallèlement un mode
d’éducation autoritaire, très conservateur (globalement, on
revenait 100/150 ans en arrière) basé sur le
contrôle&nbsp;; ils voyaient le plein
développement de l’autonomie chez l’enfant comme une
menace. Scientifiquement[^7], on sait pourtant que
le contrôle autoritaire sape la motivation et le potentiel
des enfants&nbsp;; le seul «&nbsp;atout&nbsp;» de l’éducation autoritaire est de
transformer la personne en pion, contrôlable
extérieurement par des autorités, et intérieurement
par des normes pressantes.

Les critiques n’ont pas fait gaffe à ça, et se
sont plutôt ralliés à ces autoritaires pour tenter
de me convaincre à quel point la personne ayant mené
l’expérience devait être annulée pour de
soi-disant accointances avec le
néolibéralisme (ce qui est totalement faux quand on
analyse le travail dans les faits), ignorant totalement les
détails de l’expérience et ses apports. Et ce type
d’accusations venait d’individus qui, très souvent,
était des militants actifs menant des expériences éducatives quasi
similaires à celles que cette expérience vantait…
C’était ouf, et même des années après publication de ce
contenu je reçois encore des messages pour tenter de me
convaincre que cette personne est mauvaise, donc que
l’expérience l’est, et que je devrais annuler mon avis
positif.

Tout ceci a été vraiment saoulant, d’autant plus
que naïvement je pensais qu’on pouvait espérer une sorte de
cohésion contre les modes éducatifs
autoritaires/contrôlants, ce qui est le point commun
à quasi toutes les expériences d’éducation
alternative. Mais non. Ça a été l’article le plus
«&nbsp;polémique&nbsp;» du site, je n’en reviens toujours
pas.

Depuis ce jour, je n’ai plus parlé d’éducation alternative, même si
pourtant d’autres militants m’avaient proposé de façon
stratégiquement plus intéressante de faire découvrir
leur école et leur mode de fonctionnement. Je suis passée à
des sujets plus _safe_, l’un de mes pare-feux étant
désormais de parler d’études, expériences, actions se déroulant
hors de notre pays et n’étant pas d’actualité, ou très
indirectement.

Aussi, je ne debunke plus rien (du moins, pas de cette
façon officiellement affichée), parce que cela n’a strictement
aucune résonance constructive[^8], et j’ai l’impression que
certains se servent de ce qui est alors estimé comme «&nbsp;vrai&nbsp;» pour
se permettre d’attaquer (de manière
disproportionnée) ceux qui sont dans le «&nbsp;faux&nbsp;», ce qui est une dynamique qui
ne m’intéresse pas du tout d’alimenter.

À la place, je bidouille pour trouver des sujets à écho et
je les tricote d’une façon à ce que, quand même, ils fassent
résonance avec ce que nous vivons ici et maintenant. D’un
côté, cela aura eu le grand avantage de me pousser à chercher des sujets
inédits et à développer une forme de créativité plus hackeuse
que je n’aurais peut-être pas eu sans cette saoulance.

Bref, tout ça pour dire que je ne m’autocensure pas par peur
des «&nbsp;ennemis&nbsp;», mais davantage par crainte de la
punition des alliés et acteurs de cette cause
commune que nous défendons. Je constate que d’autres
créateurs de contenus partagent cette même
crainte de l’endogroupe davantage que de leur ennemi&nbsp;:

<figure>
    <img src="Pictures/cancelling.png"
         alt="L’annulation (le canceling) - ContraPoint">
    <figcaption>L’annulation (le canceling) - ContraPoint. Sur Youtube sous-titré en français (<a href="https://www.youtube.com/watch?v=OjMPJVmXxV8">ici</a>) et sur Peertube, sous-titré en anglais et en espagnol (<a href="https://peertube.parleur.net/videos/watch/2c55681b-c3cb-4261-a7a0-42d05da5111f">ici</a>)</figcaption>
</figure>



Et ça vaut malheureusement aussi pour le libre&nbsp;: j’ai toujours
autant de respect et de sympathie pour le libre, parce que j’ai la
chance de connaître des libristes fortement prosociaux que
j’adore, et que j’utilise au quotidien du libre -- ça répond à
mes besoins de compétence et de sécurité numérique --, et,
ayant adopté depuis longtemps une certaine éthique hacker,
j’adhère au discours libriste qui fait partie de la grande famille
hacker.

Mais la campagne de certains militants pour
PeerTube a été, malgré tout, extrêmement
saoûlante.

Au départ, Gull et moi-même étions enthousiastes
pour promouvoir et publier sur PeerTube, on s’est
vite renseignés, on a été hébergés sur une instance dès que
cela a été possible. Parfois, il y avait des *down* sur
l’instance en question, alors selon l’état de la mise en
ligne, je partageais ou non le lien des vidéos via
PeerTube. Qu’importe notre présence ou
absence ponctuelle, on nous a reproché de ne pas être sur
PeerTube, on nous a fait de longs messages
condescendants pour nous expliquer pourquoi il fallait être sur
PeerTube et pourquoi il fallait
arrêter d’être sur YouTube, on nous a
engueulés parce que l’instance était *down* et
que de fait telle vidéo ne pouvait ponctuellement être vue
que sur Youtube. On ne pouvait pas annoncer
une vidéo avec joie sans que celle-ci soit
instantanément rabattue par un commentaire reprochant
notre manque d’éthique à mettre un lien Youtube et
non PeerTube (alors qu’au début on partageait
prioritairement le lien PeerTube) ou encore rappeler
une énième fois ce qu’était PeerTube comme si nous
l’ignorions et en quoi nous devions moralement y être. On
faisait au mieux, on était déjà convaincus par PeerTube,
on se démenait pour trouver des alternatives, même sur
YouTube on avait pris le parti dès le départ de démonétiser tout
notre contenu, mais visiblement ce n’était pas encore assez
parfait.



<figure>
    <img src="Pictures/messageframablog.png"
         alt="Un excellent article de framablog à ce sujet et pourquoi la saoulance n’est pas la solution pour promouvoir PeerTube">
    <figcaption><a href="https://framablog.org/2020/10/29/message-aux-youtubeurs-youtubeuses-et-surtout-a-celles-et-ceux-qui-aiment-leurs-contenus/">Un excellent article de framablog</a> à ce sujet et pourquoi la saoulance n’est pas la solution pour promouvoir PeerTube</figcaption>
</figure>


J’étais également très enthousiaste à notre
arrivée sur Mastodon, j’y postais même des trucs que
je ne postais pas sur les autres réseaux, j’envisageais de faire
là-bas un compte perso. Mais, à part
quelques personnes que je connaissais et qui
étaient bien sympas, progressivement les retours que
j’avais sur un article, un dossier, un bouquin, un live, ne
concernaient plus que notre faute à ne pas être des libristes
avant toute chose, il y avait suspicion que ce qu’on
utilisait était éthiquement incorrect (par exemple, la plateforme lulu
pour publier mes livres -- c’est sacrément ironique car
justement je l’avais choisie exclusivement pour éviter la vague
de reproches que des collègues peuvent avoir lorsqu’ils publient via
Amazon...).

Et d’autres personnes ont pu rencontrer ce même type
d’expérience&nbsp;:



<figure>
    <img src="Pictures/mcgodwin.png"
         alt="Messages sur Twitter">
    <figcaption>Messages sur Twitter (<a href="https://twitter.com/mcgodwinpaccard/status/1392055800076582912">source</a>)</figcaption>
</figure>







C’est terrible, mais d’une plateforme pour laquelle j’étais
enthousiaste, j’en suis venue à avoir la même politique de communication
que celle que j’avais sur Facebook (plateforme
que je déteste depuis des années),
c’est-à-dire&nbsp;: un minimum de publications, pas
d’interactions même quand il y a des remarques, écriture du
message la plus épurée possible par anticipation des
quiproquos, etc. Et si on me fait des corrections
légitimes sur la forme, mais sur un ton condescendant,
je les prends en compte à ma sauce (je n’obéis pas,
je transforme d’une nouvelle façon), et je ne fais aucun
retour. Je ne fais pas ça contre ce type de critiques, mais
davantage pour rester concentrée sur mon travail et ne pas être
plongée dans un énième débat où potentiellement
je dois me justifier pendant des heures d’un choix incompris, débat qui
est totalement infécond pour toutes les parties, et
où j’ai l’impression d’être la pire des merdes tant je dois
m’inférioriser pour calmer l’individu et lui offrir la
«&nbsp;supériorité&nbsp;» morale/intellectuelle qui l’apaise.


J’ai pris des exemples libristes, mais si cela peut vous rassurer,
j’ai le même vécu pénible avec des communautés
zététiques/sceptiques qui ont été hautement moralisatrices
et condescendantes, nous reprochant un manque de
détails dans le report des données scientifiques ou
suspectant que nous partagions une étude «&nbsp;biaisée&nbsp;» parce qu’ils
n’avaient pas compris un résultat (alors qu’il suffisait d’aller
simplement jeter un coup œil rapide à la source que nous mettions à
disposition). Ça a eu un impact sur notre activité et
notre motivation, nous amenant à un perfectionnisme
infructueux qui consistait à donner encore plus de détails
mais qui étaient pourtant inutiles à préciser, voire
nuisibles à une vulgarisation limpide[^9]. Plus grave, j’ai vu
des phénomènes dans des communautés zététiques/sceptiques qu’on
a décidé de quitter très rapidement tant le
climat devenait malsain&nbsp;: des activités marrantes selon eux
étaient de se faire des soirées foutage de gueule d’un créateur de la
même communauté zététique, faire des débats sans fin inféconds sur
l’usage d’un mot, et surtout ne jamais créer ensemble,
coopérer, s’attaquer aux problèmes de fond, aux «&nbsp;véritables&nbsp;»
adversaire~~s~~ qui ne sont pas tant des individus, mais
des systèmes, des structures, des normes, etc. Mais que les
zététiciens/sceptiques se rassurent, j’ai aussi vu ces
mêmes mécaniques chez des membres de syndicat (par exemple,
Gull s’est vu une fois reprocher d’aller simplement
discuter avec des membres d’un autre syndicat…), chez des
militants antipub (où Gull s’est vu corrigé non sans une
pointe de mépris parce qu’il avait osé citer une marque pour la
dénoncer car pour ce critique il était
interdit de citer une marque…), etc.



Là encore, ce n’est pas qu’un souci qui nous arriverait par
manque de bol, une fois de plus Contrapoints
explique à merveille ces mécaniques qui arrivent potentiellement dans
n’importe quel groupe réuni autour de n’importe quel sujet&nbsp;:



<figure>
    <img src="Pictures/cringe.png"
         alt="Le Cringe - ContraPoint">
    <figcaption>Le Cringe - ContraPoint. Sur Youtube, sous-titré <a href="https://www.youtube.com/watch?v=vRBsaJPkt2Q">en français</a>&nbsp;; et sur Peertube, <a href="https://peertube.parleur.net/videos/watch/551dcf2f-2aad-4c60-856a-e9a40d87d0f6">en anglais</a>.</figcaption>
</figure>



Le fait que je considère la culpabilisation, le jugement, le
mépris, la condescendance, l’attaque, l’injonction, l’appel à la
pureté, comme du militantisme déconnant (à entendre comme
dysfonctionnel)[^10] n’est évidemment pas étranger au fait que
je sois fragile, état que j’assume pleinement, parfois même
avec fierté&nbsp;: oui je craque en privé devant ce militant pour
l’Histoire qui, dans un mail de 3 pages, argumente sur
le fait que je suis médiocre, inutile et tellement inférieure aux hommes
présents à cette table ronde publique car
j’ai osé utiliser le mot «&nbsp;facho&nbsp;», usage qui
selon lui prouvait que je n’y connaissais
strictement rien parce que X et Y (imaginez un cours
d’histoire sur la Seconde Guerre
Mondiale). J’ai répondu en partie en assumant
l’un de mes défauts (être nulle à l’oral) et en envoyant les
liens vers mon bouquin en libre accès sur le
fascisme qui justifie en plus de 300 pages pourquoi je garde ce mot
et pourquoi il est pertinent pour décrire certains contenus (il
ne m’a pas répondu en retour).

J’assume aussi comme mon «&nbsp;problème&nbsp;» la
déprime que je peux avoir quand j’ai pour
premier commentaire une engueulade sur l’utilisation d’un anglicisme
dans un article qui a nécessité des heures de
recherches et d’écriture.

J’assume comme mon «&nbsp;problème&nbsp;» mon surmenage et
la lâcheté que je me permets à ne plus avoir
à me justifier ni répondre aux reproches hors-sujet.

J’ai ma sensibilité, mes faiblesses, mon temps et des capacités
limitées, je fais avec, je ne me plains pas de
souffrir de ces attitudes militantes qui me sont
reloues, et je sais à peu près comment
gérer ça et transformer ça[^11]. Je ne me sens pas victime
de quoi que ce soit, je ne suis pas à plaindre.
Au final, j’estime que nous sommes très chanceux d’avoir une
communauté si soutenante, de ne pas avoir été harcelés ou
annulés massivement comme d’autres acteurs du
Net.

Ce qui m’enrage dans un premier temps avec ces histoires de
militance déconnante, c’est que cela a détruit l’œuvre de
collègues très talentueux dont j’aurais voulu découvrir encore
de nouvelles œuvres, qui avaient un magnifique potentiel
d’influences positives,
autodéterminatrices, émancipatrices.
Mais merde, mais c’était tellement, tellement injuste de
s’en être pris à eux, pour un mot, une tournure de phrase. De
les avoir détruits pour ça, pas tant avec une seule
remarque, mais bien avec cette répétition incessante de
chipotage à chaque nouveau contenu. Tellement inapproprié de les
voir en adversaire alors qu’il n’y avait pas meilleur allié.

À force de voir cette histoire de déconnance se répéter
inlassablement partout dans tous les domaines, je m’inquiète pour
les visées fécondes de ces mouvements. J’en viens à
avoir peur que ces militants puristes dépensent une forte
énergie pour des ennemis qui n’en sont pas, qui ne représentent
pas une menace, qui au contraire sont de
potentiels alliés qui feraient avancer leur cause. J’ai
peur que cette division, voire hiérarchisation, entre
personnes potentiellement alliées ne serve finalement qu’à
donner davantage de pouvoir à leurs
adversaires. Peur que toute cette énergie gaspillée, au
fond, ne profite qu’à l’avancée de ce qu’on dénonce
pourtant tous et que les acteurs des oppressions, des
exploitations, des dominations, des manipulations,
aient le champ libre pour développer pleinement leur œuvre
destructrice, puisque les personnes pouvant les en
empêcher sont plus occupées à s’entre-taper dessus pour ou
contre le point médian, pour ou contre l’anglicisme, pour ou contre
l’usage de Facebook, et j’en passe.

Au-delà de ces peurs, je me désole aussi que tant
d’opportunités de collaboration, de construction commune, de
coopération, d’entraide passent alors à la trappe au profit du
militantisme déconnant qui ne fait que corriger, sanctionner
et contrôler. Parce que toutes les réussites que
j’ai pu voir étaient fondées sur une entraide, une
cohésion et une convergence entre militants, entre des groupes
différents, avec des acteurs qui, de base, ne
suivaient même pas le combat, n’avaient pas
les mêmes buts. Les réussites les plus belles que j’ai pu
avoir la chance d’apprécier regroupaient à la fois des groupes
militants assez radicaux (méthodes hautement destructives mais sans
violence sur les personnes), des groupes militants faisant du lobbying
auprès du monde distal[^12] (politiques, structures de
pouvoir), spectateurs lambda (soutien et sympathie pour la cause), et
résultat, une loi injuste ne passait pas, et tout le monde était en
sympathie avec tout le monde, ce n’était que joie.


<figure>
    <img src="Pictures/stopactaoctopus.jpg"
         alt="Par exemple, l’action conjointe de la Quadrature du Net, du Parti Pirate (notamment allemand), Anonymous (DoS), de la mobilisation citoyenne (manifestations dans toute l’Europe), et peut-être d’autres acteurs que j’oublie, a permis de stopper ACTA.">
    <figcaption>Par exemple, l’action conjointe de la Quadrature du Net, du Parti Pirate (notamment allemand), Anonymous (DoS), de la mobilisation citoyenne (manifestations dans toute l’Europe), et peut-être d’autres acteurs que j’oublie, a permis de stopper ACTA. Plus d’infos <a href="https://www.laquadrature.net/2012/07/04/acta-victoire-totale-pour-les-citoyens-et-la-democratie/">sur laquadrature.org</a>. Source image&nbsp;: <a href="https://commons.wikimedia.org/wiki/File&nbsp;:Stop_ACTA_Octupus.jpg">Wikimedia</a>.</figcaption>
</figure>


C’est pourquoi dans cet article on va tenter de comprendre pourquoi
on peut avoir tendance à attaquer, injonctiver et pourquoi
ça peut être déconnant vis-à-vis des buts d’un mouvement, et
ce qu’on pourrait faire à la place de potentiellement
plus efficace.



## Avertissements sur ce contenu



Vous l’aurez sans doute compris, mais je précise pour éviter tout
malentendu&nbsp;:



&#9733; *Cet article porte sur le militantisme déconnant, ou pureté militante, et non sur le militantisme tout court*. Nous nous considérons
nous-mêmes comme des personnes engagées à travers notre activité
sur Hacking-social/Horizon-gull, et
engagées contre l’autoritarisme sous toutes ses formes, et donc par
définition, militantes.

&#9733; *Je ne catégorise pas tout le militantisme libriste comme uniquement associé à des pratiques déconnantes*, pas du tout, d’autant
que nous écrivons sur le Framablog, ce qui serait un comble.
Idem, si vous avez des affinités avec un groupe dont je
décris la pratique déconnante. N’endossez pas la
responsabilité ou la culpabilité des pratiques déconnantes si vous
ne les avez jamais faites mais que vous partagez le même
groupe que ceux qui y ont recours. On ne peut pas être
responsable du comportement des autres individus en permanence,
ce serait un fardeau trop lourd à porter et impossible à gérer.
La responsabilité incombe à celui qui fait l’acte, sans omettre,
bien sûr, les raisons extérieures qui ont poussé celui-ci à
faire l’acte (évitons d’internaliser ces problématiques aux seuls
individus, nous verrons que c’est plus complexe que
cela).

&#9733; *On parlera de militantisme interpersonnel, c’est-à-dire entre personnes ne représentant pas une autorité quelconque*. Par exemple, untel corrige unetelle sur un de
ses mots sur Internet/lors d’un repas de
famille/lorsqu’elle se confie/lorsqu’elle présente une
création/etc.&nbsp;; untel critique untel car il a osé parler
de fromages sans mettre de trigger warning[^13], etc.
J’exclus de cette catégorie les rapports de pouvoirs entre
personnes, par exemple une négociation entre un syndicat
et un DRH autour d’un conflit, le débat public entre un
individu fasciste et une personne au combat antifasciste, etc.
Les deux derniers cas désignent des
porte-paroles d’un groupe et la situation n’a
rien du rapport interpersonnel habituel, il est hautement
stratégique et préparé en amont pour les deux parties en
confrontation.

&#9733; On ne parlera pas du militantisme d’extrême-droite, fasciste ou
lié à des poussées de haines sur une personne ciblée (par exemple les
raids de harcèlement). Parce que -- et c’est terrible à dire -- il est
cohérent avec l’autoritarisme de droite composé
d’une valorisation de l’*agressivité autoritaire*, de la
*soumission autoritaire* et du *conventionnalisme*[^14]. Si
c’est cohérent au vu de leur combat, alors ce n’est pas
déconnant. Ils sont convaincus du bienfait
d’attaquer et de soumettre une personne, donc il est logique
de s’attendre à ce qu’ils attaquent une personne, qu’ils
l’injonctivent, la poussent à rentrer dans des
normes conventionnelles ou à fuir la sphère publique.
Cependant, si cela vous intéresse&nbsp;:
- voici un excellent article qui montre leur méthode militante&nbsp;: *Un militant repenti balance les secrets de l’ultra-droite*, ([*Midi Libre*, 2012](https://www.midilibre.fr/2012/10/08/un-militant-repenti-balance-les-secrets-de-l-ultra-droite,574771.php))&nbsp;;
- On a parlé d’autoritarisme aussi dans [un dossier](http://www.hacking-social.com/2017/01/16/f1-espece-de-facho-etudes-sur-la-personnalite-autoritaire/)&nbsp;;
- et [cet article](https://www.hacking-social.com/2019/09/02/mcq-le-potentiel-fasciste-lautoritaire-et-le-dominateur/), le résumé des recherches qui ont suivi&nbsp;;
- Et dans [cette vidéo](https://skeptikon.fr/videos/watch/3308307a-fbff-4b6d-9740-1a5d3f30a0be) qui est le début d’une série.

<figure>
    <img src="Pictures/lesautoritaires.png"
         alt="Les autoritaires, partie 1">
    <figcaption>Les autoritaires, partie 1 (sur <a href="https://skeptikon.fr/videos/watch/3308307a-fbff-4b6d-9740-1a5d3f30a0be">skeptikon.fr</a>, sur <a href="https://www.youtube.com/watch?v=2__Dd_KXuuU&t=1275s">Youtube</a>, sur <a href="https://vimeo.com/496438064">Vimeo</a>)</figcaption>
</figure>



&#9733;  Pour cet article, il serait incohérent que je
m’autocensure par peur de la pureté militante, ainsi y aura-t-il des
néologismes, des anglicismes, des points médians et parfois une absence
de points médians, des liens YouTube ou pointant vers
des réseaux sociaux, structures non-libres. Cependant, je
justifierai parfois certains de mes choix «&nbsp;impurs&nbsp;»
en note de bas de page quand j’estime que cela est nécessaire. J’y
mettrai aussi les références. Désolée pour l’énervement
futur que cela pourrait vous causer.


<!-------------- Notes INTRO ---------------->



[^1]: J’ai gardé le terme anglais parce qu’on croise rarement des
    personnes se disant «&nbsp;fouineuse à chapeau gris&nbsp;» comme le
    recommanderait l’Académie française. Les hackers *grey hat* désignent les hackers faisaient des actions illégales pour des buts prosociaux/activistes, à la différence des *black hat* qui vont faire des actions illégales pour leur seul profit personnel par exemple.

[^2]: GAFAM = Google, Apple, Facebook, Amazon et Microsoft.

[^3]: TW = Trigger Warning = avertissements.

[^4]: Endogroupe = groupe d’appartenance. On peut en choisir certains
    (supporter telle équipe et pas telle autre, être dans une
    communauté de passionnés pour telle discipline, etc.) et d’autres
    sont dus au pur hasard comme la nationalité à la naissance, le
    genre, la génération dans laquelle on appartient, la couleur de
    peau, etc.

[^5]: Ça, c’est volontairement hors de mes buts, non pas que
    je sois défaitiste, mais parce que je ne vois pas l’intérêt de
    convaincre qui que ce soit&nbsp;: imaginons qu’untel
    serait par exemple convaincu que la justice
    réparatrice (un thème que j’aborde) c’est bien. Ok,
    super. Et ensuite&nbsp;? Qu’est-ce que ça change&nbsp;? Strictement rien. Au
    fond, je ne veux rien d’autre que ce que
    souhaiterait un blogueur tech&nbsp;: il partage un logiciel
    qu’il trouve cool et il est content si ça sert à d’autres qui vont
    l’utiliser pour d’autres besoins. Il peut même être super
    content de découvrir que ce logiciel qu’il a partagé a été hacké
    pour en faire un autre truc plus performant qui sert d’autres buts,
    parce qu’au fond, c’est la tech et son développement qui le font
    kiffer. Et c’est pas grave si les autres n’aiment pas, ne
    testent pas. Au fond, lui comme moi, on veut juste
    partager, c’est tout.

[^6]: Je n’ai ni envie de donner le lien ni même de référence au sujet
    de cet écrit&nbsp;; si le sujet vous intéresse intrinsèquement, vous le
    trouverez facilement.

[^7]: Deci et coll. (1999)&nbsp;; Deci et Ryan (2017)&nbsp;; Csiszentmihalyi (2014,
    2015)&nbsp;; Della Fave, Massimini, Bassi (2011)&nbsp;; Gueguen C. (2014,
    2018) références non exhaustives.

[^8]: Je parle de mes articles, je ne critique pas les débunkeurs en
    général. Il y a certains débunkeurs sur PeerTubeYouTube qui ont du
    talent, je ne doute pas qu’ils arrivent eux à avoir un écho positif.

[^9]: Gull a d’ailleurs constaté que cette surenchère à préciser
    toujours plus de détails en vidéo, sous la demande parfois pesante
    de certains septiques, contribuaient à dissoudre le discours
    principal, à ennuyer de nombreux *viewers* se plaignant
    désormais d’un contenu «&nbsp;trop académique&nbsp;».

[^10]: J’emploie le mot «&nbsp;dysfonctionnel&nbsp;» pour qualifier les
    comportements militants tels que l’injonction, l’attaque, etc., car
    la fonction de la militance est, entre autres, de combattre un
    problème et non d’être perçue comme un problème par ceux que les
    militants peuvent chercher à convaincre. En cela, ça
    dysfonctionne, là où un militantisme «&nbsp;fonctionnel&nbsp;» opterait pour des
    comportements et actions qui résolvent un problème, s’attaquerait à
    la destructivité et non aux potentielles forces de construction.

[^11]: Attention ces dernières phrases sont fortement imprégnées de mes
    biais d’internalité (ignorer les causes extérieures, se
    responsabiliser pour des problèmes extérieurs), je ne conseille pas
    du tout de reproduire la même dynamique que la mienne, potentiellement
    sapante. Si je les ai écrites, c’est parce que c’est ce que je
    ressens et que ce serait mentir que d’en enlever les biais
    d’internalité qui ont structuré ces sentiments.

[^12]: En psycho- sociale, notamment dans le champ de la théorie de
    l’autodétermination, on emploie le terme d’environnement distal pour
    décrire des environnements qui sont «&nbsp;distants&nbsp;» des individus (mais
    pas moins influents), tel que les champs politique, économique,
    culturel&nbsp;; il se distingue de l’environnement social proximal
    (famille, travail, et tout environnement social qui fait le
    quotidien la personne).

[^13]: Exemple tiré d’un témoignage réel, disponible sur [neonmag.fr](https://www.neonmag.fr/purete-militante-culture-du-callout-quand-les-activistes-sentre-dechirent-569283.html).

[^14]: Ce sont les caractéristiques de l’Autoritarisme de droite (RWA)
    telles que définies par la recherche en psychologie sociale et
    politique de ces dernières décennies, notamment par Altemeyer. Si
    une personne se disant de gauche a pour attitude principale
    l’agressivité autoritaire, la soumission et le conventionnalisme,
    alors il serait plus cohérent pour lui de se revendiquer d’un
    autoritarisme de droite, qui serait plus raccord avec ses attentes et
    valeurs. Plus d’infos sur [hacking-social.com](https://www.hacking-social.com/2019/09/02/mcq-le-potentiel-fasciste-lautoritaire-et-le-dominateur/).


