Teresa M. Amabile, William DeJong et Mark R. Lepper, « Effects of externally imposed deadlines on subsequent intrinsic motivation », *Journal of Personality and Social Psychology*,  34-1, 1976, p. 92‑98.

Rosemarie Anderson, Sam T. Manoogian et J. Steven Reznick, « The undermining and enhancing of intrinsic motivation in preschool children », *Journal of Personality and Social Psychology*,  34-5, 1976, p. 915‑922.

Susan Anderson et Judith Rodin, « Is Bad News Always Bad?: Cue and Feedback Effects on Intrinsic Motivation », *Journal of Applied Social Psychology*,  19-6, 1989, p. 449‑467.

Avi Assor, Guy Roth et Edward L. Deci, « The emotional costs of parents’ conditional regard: a self-determination theory analysis », *Journal of Personality*,  72-1, 2004, p. 47‑88.

Roy F. Baumeister et Dianne M. Tice, « Self-esteem and responses to success and failure: Subsequent performance and intrinsic motivation », *Journal of Personality*,  53-3, 1985, p. 450‑467.

Beiwen Chen, Maarten Vansteenkiste, Wim Beyers, Liesbet Boone, Edward Deci, Jolene Van der Kaap-Deeder, Bart Duriez, Willy Lens, Lennia Matos, Athanasios Mouratidis, Richard Ryan, Kennon Sheldon, Bart Soenens, Stijn Van Petegem et Joke Verstuyf, « Basic psychological need satisfaction, need frustration, and need strength across four cultures », 2015.

Mihaly Csikszentmihalyi, *Applications of Flow in Human Development and Education*, Springer Netherlands, 2014.

Mihaly Csikszentmihalyi, *The Systems Model of Creativity*, Springer Netherlands, 2014.

J. M. Darley et B. Latané, « Bystander intervention in emergencies: diffusion of responsibility », *Journal of Personality and Social Psychology*,  8-4, 1968, p. 377‑383.

E. L. Deci, R. Koestner et R. M. Ryan, « A meta-analytic review of experiments examining the effects of extrinsic rewards on intrinsic motivation », *Psychological Bulletin*,  125-6, 1999, p. 627‑668; discussion 692-700.

Edward Deci, *Intrinsic Motivation*, Springer US, 1975.

Edward L. Deci, Gregory Betley, James Kahle, Linda Abrams et Joseph Porac, « When Trying to Win: Competition and Intrinsic Motivation », *Personality and Social Psychology Bulletin*,  7-1, 1981, p. 79‑83.

Edward L. Deci et Wayne F. Cascio, « Changes in Intrinsic Motivation as a Function of Negative Feedback and Threats », 1972.

Edward L. Deci, Wayne F. Cascio et Judith Krusell, « Cognitive evaluation theory and some comments on the Calder and Staw critique », *Journal of Personality and Social Psychology*,  31-1, 1975, p. 81‑85.

Edward L. Deci et Richard M. Ryan, « A motivational approach to self: Integration in personality », in *Nebraska Symposium on Motivation, 1990:  Perspectives on motivation*, Lincoln, NE, US, University of Nebraska Press, 1991, p. 237‑288.

Edward L. Deci, Allan J. Schwartz, Louise Sheinman et Richard M. Ryan, « An instrument to assess adults’ orientations toward control versus autonomy with children: Reflections on intrinsic motivation and perceived competence », *Journal of Educational Psychology*,  73-5, 1981, p. 642‑650.

Paul Dolan et Matteo M. Galizzi, « Like ripples on a pond: Behavioral spillovers and their implications for research and policy », *Journal of Economic Psychology*,  47, 2015, p. 1‑16.

Michael E. Enzle et Sharon C. Anderson, « Surveillant intentions and intrinsic motivation », *Journal of Personality and Social Psychology*,  64-2, 1993, p. 257‑266.

Antonella Delle Fave, Fausto Massimini et Marta Bassi, *Psychological Selection and Optimal Experience Across Cultures: Social Empowerment through Personal Growth*, Springer Netherlands, 2011.

B. J. Fogg, *Persuasive Technology: Using Computers to Change What We Think and Do*, Amsterdam ; Boston, Morgan Kaufmann Publishers In, 2003.

Jean Hatzfeld, *Une saison de machettes: récits*, Paris, France, Éditions du Seuil, 2003.

Richard Koestner, Nathalie Houlfort, Stephanie Paquet et Christine Knight, « On the Risks of Recycling Because of Guilt: An Examination of the Consequences of Introjection », *Journal of Applied Social Psychology*,  31-12, 2001, p. 2545‑2560.

Richard Koestner, Gaëtan F. Losier, Robert J. Vallerand et Donald Carducci, « Identified and introjected forms of political internalization: Extending self-determination theory », *Journal of Personality and Social Psychology*,  70-5, 1996, p. 1025‑1036.

Richard Koestner, Richard M. Ryan, Frank Bernieri et Kathleen Holt, « Setting limits on children’s behavior: The differential effects of controlling vs. informational styles on intrinsic motivation and creativity », *Journal of Personality*,  52-3, 1984, p. 233‑248.

M. R. Lepper, D. Greene et R. E. Nisbett, « Undermining children’s intrinsic interest with extrinsic reward: A test of the “overjustification” hypothesis », *Journal of Personality and Social Psychology*,  28-1, 1973, p. 129‑137.

Mark R. Lepper et David Greene, « Turning play into work: Effects of adult surveillance and extrinsic rewards on children’s intrinsic motivation », *Journal of Personality and Social Psychology*,  31-3, 1975, p. 479‑486.

Arlen Moller, Guy Roth, Christopher P. Niemiec, Yaniv Kanat-Maymon et Edward L. Deci, « Mediators of the associations between parents’ conditional regard and the quality of their adult-children’s peer relationships ».

Pearl Oliner, Samuel P. Oliner, Lawrence Baron et Lawrence Blum, *Embracing the Other: Philosophical, Psychological, and Historical Perspectives on Altruism*, New York, New York University Press, 1992.

Samuel P. Oliner, *Altruistic Personality: Rescuers Of Jews In Nazi Europe*, Reprint édition., Touchstone, 2002.

Samuel P. Oliner et Pearl M. Oliner, *The Altruistic Personality: Rescuers of Jews in Nazi Europe*, New York : London, Macmillan USA, 1988.

Thane S. Pittman, Margaret E. Davey, Kimberly A. Alafat, Kathryn V. Wetherill et Nancy A. Kramer, « Informational versus Controlling Verbal Rewards », *Personality and Social Psychology Bulletin*,  6-2, 1980, p. 228‑233.

Robert W. Plant et Richard M. Ryan, « Intrinsic motivation and the effects of self-consciousness, self-awareness, and ego-involvement: An investigation of internally controlling styles », *Journal of Personality*,  53-3, 1985, p. 435‑449.

Catherine Gueguen, *Pour une enfance heureuse. Repenser l’éducation à la lumière des dernières découvertes sur le cerveau*, Paris, Pocket, 2015.

Mark J. Reader et Stephen J. Dollinger, « Deadlines, Self-Perceptions, and Intrinsic Motivation », *Personality and Social Psychology Bulletin*,  8-4, 1982, p. 742‑747.

Carl Ransom Rogers, *Liberté pour apprendre ?*, Paris, France, Dunod, 1984.

Carl Ransom Rogers, Eleonore Lily Herbert et Max Préfacier Pagès, *Le développement de la personne*, Paris, France, Dunod, 1996.

Richard M. Ryan et Edward L. Deci, *Self-Determination Theory: Basic Psychological Needs in Motivation, Development, and Wellness*, New York, Guilford Press, 2017.

Richard M. Ryan et Wendy S. Grolnick, « Origins and pawns in the classroom: Self-report and projective assessments of individual differences in children’s perceptions », *Journal of Personality and Social Psychology*,  50-3, 1986, p. 550‑558.

Richard M. Ryan, Scott Rigby et Kristi King, « Two types of religious internalization and their relations to religious orientations and mental health », *Journal of Personality and Social Psychology*,  65-3, 1993, p. 586‑596.

Jacques Semelin, *Sans armes face à Hitler: la résistance civile en Europe*, 1939-1943, Paris, France, Les Arènes, impr. 2013, 2013.

Jacques Semelin, *Purifier et Détruire. Usages politiques des massacres et génocides*, Paris, Le Seuil, 2005.

Jacques Sémelin, *Pour sortir de la violence*, Paris, Editions ouvrières, 1983.

Jacques Semelin, Christian Mellon, *La Non-Violence*, Paris, Presses Universitaires de France - PUF, 1994.

Ervin Staub, *The Psychology of Good and Evil: Why Children, Adults, and Groups Help and Harm Others*, Cambridge, U.K. ; New York, Cambridge University Press, 2003.

Michel Terestchenko, *Un si fragile vernis d’humanité: banalité du mal, banalité du bien*, Paris, France, la Découverte : MAUSS, 2005.

Netta Weinstein, William S. Ryan, Cody R. DeHaan, Andrew K. Przybylski, Nicole Legate et Richard M. Ryan, « Parental autonomy support and discrepancies between implicit and explicit sexual identities: Dynamics of self-acceptance and defense », *Journal of Personality and Social Psychology*,  102-4, 2012, p. 815‑832.

